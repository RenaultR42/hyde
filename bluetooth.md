# Bluetooth profiles

Hyde support several Bluetooth profiles:

* Bluetooth Low Energy (BLE) for some additional services
* AVRCP to control audio stream: play, pause, next / previous, etc.
* A2DP to stream audio from phone /device to Hyde

# GATT services

With BLE profile, some custom services are provided over GATT.

## GATT device generic

Service UUID: 0x1800

### Characteristics

#### Device name

* UUID: 0x2A00
* Permissions: read
* Value: "ArtCore Hyde" (in ASCII)

### Device type

* UUID: 0x2A01
* Permissions: read

## Service tuner

Service UUID: 17A0D16D9E5C438095E8EB9315ABAE01

### Characteristics

#### Power management

* UUID: 17A0D16E9E5C438095E8EB9315ABAE01
* Permissions: read, write and notify
* Values: <status>
	* <status>
		* 0 = Power off
		* 1 = Power on

Read to get the current status, write to start or stop the device.

#### Radio mode

* UUID: 17A0D16F9E5C438095E8EB9315ABAE01
* Permissions: read and notify
* Values: <mode>
	* <mode>
		* 0 = unknown
		* 1 = DAB
		* 2 = FM
		* 4 = STANDBY

Read to get the current mode.
The mode can be changed when input source is changed or power management changed also.

## Service radio

Service UUID: 75371D8BB5D348C48B7A24859DEABA32

### Characteristics

#### Station name

* UUID: 75371D8CB5D348C48B7A24859DEABA32
* Permissions: read and notify
* Values: <length> <name>
	* <length>: integer from 0 to 17: station name length
	* <name>: in ASCII

Read to get the current station name.

#### Frequence radio

* UUID: 75371D8DB5D348C48B7A24859DEABA32
* Permissions: read, write and notify
* Values: <radio_type> <frequency>
	* <radio_type>:
		* 0 = DAB
		* 1 = FM
	* <frequency>:
		* DAB = index
		* FM = (frequency - 87.5Mhz) / 50 KHz

Read to get the current radio type and current station frequency or index.
Write to select another index or frequency for a specific radio type.

#### Store preset

* UUID: 75371D8EB5D348C48B7A24859DEABA32
* Permissions: read, write and notify
* Values (write): <radio_type> <preset> <frequency>
	* <radio_type>:
		* 0 = DAB
		* 1 = FM
	* <preset>: integer from 0 to 5
	* <frequency>:
		* DAB = index
		* FM = (frequency - 87.5Mhz) / 50 KHz
* Values (read): <radio_type> <preset_frequency 1> <preset_frequency 2> <preset_frequency 3> <preset_frequency 4> <preset_frequency 5> <preset_frequency 6>
	* <radio_type>:
		* 0 = DAB
		* 1 = FM
	* <frequency>:
		* DAB = index
		* FM = (frequency - 87.5Mhz) / 50 KHz

Read to get the current radio type presets.
Write to assign a frequency or index to a preset for a radio mode.

#### Select preset

* UUID: 75371D8FB5D348C48B7A24859DEABA32
* Permissions: write
* Values: <preset> <frequence>
	* <preset>: integer from 0 to 5
	* <frequence>:
		* DAB = index
		* FM = (frequency - 87.5Mhz) / 50 KHz

Write to switch to the bookmarked index or frequency for the current radio mode.

## Service DAB

Service UUID: 6996BD4906A34DF5A021ACA68AE457ED

### Characteristics

#### Auto scan

* UUID: 6996BD4A06A34DF5A021ACA68AE457ED
* Permissions: write
* Values: <value>
	* <value> whatever

Trigger a new DAB scan to refresh list of stations.

#### Number of services

* UUID: 6996BD4B06A34DF5A021ACA68AE457ED
* Permissions: read and notify
* Values: <nb>
	* <nb> integer from 0 to 99

Read the number of available stations over DAB.
This number is not changed between two auto DAB scan.

#### DAB service index information

* UUID: 6996BD4C06A34DF5A021ACA68AE457ED
* Permissions: read, write and notify
* Values (read): <index> <freq_ensemble> <station_name_len> <station_name>
	* <index> integer from 0 to 99
	* <freq_ensemble> integer from 0 to 40 or 0xFF if no frequency ensemble
	* <station_name_len> integer from 0 to 16 which is the length of the station name
	* <station_name> is the name of the station name of this index
* Values (write): <index>
	* <index> integer from 0 to 100

Write the DAB index used to get info for the next read operation.
Index is an integer from 0 to _Number of services - 1_.

Read value to get info about the DAB index.
Frequency ensemble:
	* 0 = " 5A 174.928MHz "
	* 1 = " 5B 176.640MHz "
	* 2 = " 5C 178.352MHz "
	* 3 = " 5D 180.064MHz "
	* 4 = " 6A 181.936MHz "
	* 5 = " 6B 183.648MHz "
	* 6 = " 6C 185.360MHz "
	* 7 = " 6D 187.072MHz "
	* 8 = " 7A 188.928MHz "
	* 9 = " 7B 190.640MHz "
	* 10 = " 7C 192.352MHz "
	* 11 = " 7D 194.064MHz "
	* 12 = " 8A 195.936MHz "
	* 13 = " 8B 197.648MHz "
	* 14 = " 8C 199.360MHz "
	* 15 = " 8D 201.072MHz "
	* 16 = " 9A 202.928MHz "
	* 17 = " 9B 204.640MHz "
	* 18 = " 9C 206.352MHz "
	* 19 = " 9D 208.064MHz "
	* 20 = " 10A 209.936MHz "
	* 21 = " 10N 210.096MHz "
	* 22 = " 10B 211.648MHz "
	* 23 = " 10C 213.360MHz "
	* 24 = " 10D 215.072MHz "
	* 25 = " 11A 216.928MHz "
	* 26 = " 11N 217.088MHz "
	* 27 = " 11B 218.640MHz "
	* 28 = " 11C 220.352MHz "
	* 29 = " 11D 222.064MHz "
	* 30 = " 12A 223.936MHz "
	* 31 = " 12N 224.096MHz "
	* 32 = " 12B 225.648MHz "
	* 33 = " 12C 227.360MHz "
	* 34 = " 12D 229.072MHz "
	* 35 = " 13A 230.784MHz "
	* 36 = " 13B 232.496MHz "
	* 37 = " 13C 234.208MHz "
	* 38 = " 13D 235.776MHz "
	* 39 = " 13E 237.488MHz "
	* 40 = " 13F 239.200MHz "

## Service Audio

Service UUID: 2CB1FAD1AC5F4E6C9E456156DA47E4F4

### Characteristics

#### Audio input

* UUID: 2CB1FAD2AC5F4E6C9E456156DA47E4F4
* Permissions: read, write and notify
* Values: <input_source>
	* <input_source>:
		* 1 = Bluetooth
		* 2 = DAB tuner
		* 3 = FM tuner
		* 4 = Analog input
		* 5 = Digital input

Write to switch the audio input switch.
Read to know the current audio input.

#### Volume

* UUID: 2CB1FAD3AC5F4E6C9E456156DA47E4F4
* Permissions: read, write and notify
* Values: <volume>
	* <volume>: integer from 0 to 255

Write to change current volume level. If the volume exceed the maximum value, the maximum value is applied.
Read to know the current volume level.

Default volume: 5
Default maximum volume: 31
