#ifndef H_BLUETOOTH
#define H_BLUETOOTH

#include <stdbool.h>

#include "types.h"
#include "config.h"
#include "uart.h"
#include "audio.h"

#define MAC_ADDRESS_SIZE	12

#if 0
#define DISABLE_TWS 1
#endif

enum pairing_status {
	PAIR_NONE	= 0,
	PAIR_OK	= 1,
	PAIR_ERROR	= 2,
	OPEN_OK	= 3,
	CLOSE_OK	= 4,
};

void reset_bc128(config_data_t *config);
void init_bc128(config_data_t *config);
void save_bc128_config(void);

bool has_bluetooth_player(void);
enum pairing_status get_bt_pairing_status(void);
void set_bt_pairing_status(enum pairing_status status);

void set_bluetooth_power(enum power_state mode);
void control_bt(enum audio_state mode);
void update_bt_name(char *name, config_data_t *config);
void parse_cmd_bluetooth(serial_comm_t *msg, config_data_t *config);
byte old_send_bc128(char *string);
byte send_bc128(char *format, ...);
void setup_dsp(void);
void process_bluetooth_queue(config_data_t *config);

void select_bluetooth_as_source(config_data_t *config);
void set_bluetooth_volume(byte volume);

#endif
