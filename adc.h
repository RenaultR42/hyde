/*----------------------------------------------------------------------------
 * Name:    ADC.h
 * Purpose: MCB1700 low level ADC definitions
 * Note(s):
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2009-2013 Keil - An ARM Company. All rights reserved.
 *----------------------------------------------------------------------------*/

#ifndef __ADC_H
#define __ADC_H

#define ADC_VALUE_MAX	0x3ff	/* 22V */
#define ADC_VALUE_NRM	0x33d	/* 18V */
#define ADC_VALUE_TRE	0x297	/* 14.4V */
#define ADC_VALUE_MIN	0x28c	/* 14V */

/* PWR-MON PIO0_11 (AD0) 15V input  -> 3.26V : 1011 ADC val */
#define POWER_MONIT_LEVEL	910	/* 90% */

enum adc_channels {
	ADC_CHANNEL_0 = 0x1,
	ADC_CHANNEL_1 = 0x2,
	ADC_CHANNEL_2 = 0x4,
	ADC_CHANNEL_3 = 0x8,
	ADC_CHANNEL_4 = 0x10,
	ADC_CHANNEL_5 = 0x20,
	ADC_CHANNEL_6 = 0x40,
	ADC_CHANNEL_7 = 0x80,
};

void adc_init(enum adc_channels channels);

#endif
