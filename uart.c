#include <string.h>
#include "LPC11xx.h"
#include "uart.h"
#include "main.h"
#include "platform_hyde.h"
#include "gpio.h"
#include "platform_gpio_hyde.h"

static bool lock_serial = false;
static serial_comm_t uart_msg;
static byte cmd_data[SERIAL_RX_BUFFERSIZE] = { '\0' };

/* IER register */
#define UART_IER_RECEIVE_BYTE_INT_SHIFT	0
#define UART_IER_RECEIVE_BYTE_INT_ENABLE	(1 << UART_IER_RECEIVE_BYTE_INT_SHIFT)
#define UART_IER_TRANSMIT_BYTE_INT_SHIFT	1
#define UART_IER_TRANSMIT_BYTE_INT_DISABLE	(0 << UART_IER_TRANSMIT_BYTE_INT_SHIFT)

/* FCR register */
#define UART_FCR_ENABLE_FIFO_SHIFT	0
#define UART_FCR_ENABLE_FIFO		(1 << UART_FCR_ENABLE_FIFO_SHIFT)
#define UART_FCR_RX_FIFO_RESET_SHIFT	1
#define UART_FCR_RX_FIFO_RESET		(1 << UART_FCR_RX_FIFO_RESET_SHIFT)
#define UART_FCR_TX_FIFO_RESET_SHIFT	2
#define UART_FCR_TX_FIFO_RESET		(1 << UART_FCR_TX_FIFO_RESET_SHIFT)

/* MCR register */
#define UART_MCR_ENABLE_RTS_SHIFT	6
#define UART_MCR_ENABLE_RTS		(1 << UART_MCR_ENABLE_RTS_SHIFT)
#define UART_MCR_ENABLE_CTS_SHIFT	7
#define UART_MCR_ENABLE_CTS		(1 << UART_MCR_ENABLE_CTS_SHIFT)

/* LCR register */
#define UART_LCR_WORD_SIZE_SHIFT	0
#define UART_LCR_WORD_SIZE_8_BITS	(0x3 << UART_LCR_WORD_SIZE_SHIFT)
#define UART_LCR_STOP_BIT_SHIFT	2
#define UART_LCR_STOP_1_BIT		(0 << UART_LCR_STOP_BIT_SHIFT)
#define UART_LCR_PARITY_SHIFT		3
#define UART_LCR_PARITY_ODD		(0 << UART_LCR_PARITY_SHIFT)
#define UART_LCR_DIVISOR_LATCH_SHIFT		7
#define UART_LCR_DIVISOR_LATCH_DISABLED	(0 << UART_LCR_DIVISOR_LATCH_SHIFT)
#define UART_LCR_DIVISOR_LATCH_ENABLED		(1 << UART_LCR_DIVISOR_LATCH_SHIFT)

/* LSR register */
#define UART_LSR_RECEIVER_HAS_DATA	0x01
#define UART_LSR_TRANSMITTER_EMPTY	0x20

void uart_init(uint32_t baudrate)
{
	uint32_t fdiv;
	volatile uint32_t reg_val;

	NVIC_DisableIRQ(UART_IRQn);

	enable_sys_clock(SYS_CLOCK_GPIO, true);
	enable_sys_clock(SYS_CLOCK_IOCON, true);

	set_port_pinmux(PORT_IO_UART_RX, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_UART_RX_FUNC));
	set_port_pinmux(PORT_IO_UART_TX, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_UART_TX_FUNC));
	set_port_pinmux(PORT_IO_UART_CTS, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_UART_CTS_FUNC));
	set_port_pinmux(PORT_IO_UART_RTS, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_UART_RTS_FUNC));

	enable_sys_clock(SYS_CLOCK_UART, true);
	LPC_SYSCON->UARTCLKDIV = 0x1;	/* Divided by 1 */

	LPC_UART->LCR = UART_LCR_WORD_SIZE_8_BITS | UART_LCR_STOP_1_BIT | UART_LCR_PARITY_ODD | UART_LCR_DIVISOR_LATCH_ENABLED;
	fdiv = ((SystemCoreClock / LPC_SYSCON->UARTCLKDIV) / 16) / baudrate;	/* Baud rate */
	LPC_UART->DLM = fdiv / 256;
	LPC_UART->DLL = fdiv % 256;
	LPC_UART->FDR = 0x10;	/* Default */
	LPC_UART->LCR = UART_LCR_WORD_SIZE_8_BITS | UART_LCR_STOP_1_BIT | UART_LCR_PARITY_ODD | UART_LCR_DIVISOR_LATCH_DISABLED;

	LPC_UART->FCR = UART_FCR_ENABLE_FIFO | UART_FCR_RX_FIFO_RESET | UART_FCR_TX_FIFO_RESET;
	LPC_UART->IER = UART_IER_TRANSMIT_BYTE_INT_DISABLE | UART_IER_RECEIVE_BYTE_INT_ENABLE;
	LPC_UART->MCR = UART_MCR_ENABLE_CTS | UART_MCR_ENABLE_RTS;

	/* Read to clear the line status. */
	reg_val = LPC_UART->LSR;

	/* Ensure a clean start, no data in either TX or RX FIFO. */
	while ((LPC_UART->LSR & (LSR_THRE | LSR_TEMT)) != (LSR_THRE | LSR_TEMT))
		;

	while (LPC_UART->LSR & LSR_RDR) {
		reg_val = LPC_UART->RBR;	/* Dump data from RX FIFO */
	}

	/* Enable the UART Interrupt */
	NVIC_EnableIRQ(UART_IRQn);

	uart_msg.cmd_read = true;
	uart_msg.rx_byte_cnt = 0;
}

/* Send char to serial port */
int sendchar(int c)
{
	while (!(LPC_UART->LSR & UART_LSR_TRANSMITTER_EMPTY))
		;

	LPC_UART->THR = c;
	return c;
}

/* Read char (blocking) */
int getkey(void)
{
	while (!(LPC_UART->LSR & UART_LSR_RECEIVER_HAS_DATA))
		;

	return LPC_UART->RBR;
}

void UART_IRQHandler(void)
{
	byte rxbyte = LPC_UART->RBR;

	if (rxbyte == '\r' || uart_msg.rx_byte_cnt >= (SERIAL_RX_BUFFERSIZE - 1)) {
		cmd_data[uart_msg.rx_byte_cnt] = '\0';
		uart_msg.cmd_read = false;
		strcpy((char *) uart_msg.cmd_data_buf, (char *) cmd_data);
		uart_msg.rx_byte_cnt = 0;
	} else if (uart_msg.rx_byte_cnt < (SERIAL_RX_BUFFERSIZE - 1)) {
		cmd_data[uart_msg.rx_byte_cnt] = rxbyte;
		uart_msg.rx_byte_cnt++;
	}
}

bool new_uart_message_is_available(void)
{
	return !uart_msg.cmd_read;
}

serial_comm_t *get_uart_message(void)
{
	return &uart_msg;
}

bool uart_is_busy(void)
{
	return lock_serial;
}

void use_uart(bool is_used)
{
	lock_serial = is_used;
}
