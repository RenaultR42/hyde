#include "platform_hyde.h"
#include "platform_gpio_hyde.h"
#include "gpio.h"
#include "wdt.h"
#include "adc.h"
#include "rfdet.h"
#include "uart.h"
#include "utils.h"
#include "i2c.h"

void enable_sys_clock(enum sys_clock clk, bool enable)
{
	if (enable) {
		LPC_SYSCON->SYSAHBCLKCTRL |= (1UL << clk);
	} else {
		LPC_SYSCON->SYSAHBCLKCTRL &= ~(1UL << clk);
	}
}

void gpio_init(void)
{
	enable_sys_clock(SYS_CLOCK_GPIO, true);
	enable_sys_clock(SYS_CLOCK_IOCON, true);

	set_gpio_direction(GPIO_BANK_ISP, GPIO_PIN_ISP, GPIO_INPUT);
	set_gpio_direction(GPIO_BANK_EXTERN_TRIGGER_ON, GPIO_PIN_EXTERN_TRIGGER_ON, GPIO_INPUT);
	set_gpio_direction(GPIO_BANK_EXTERN_TRIGGER_OFF, GPIO_PIN_EXTERN_TRIGGER_OFF, GPIO_INPUT);
	set_gpio_direction(GPIO_BANK_SETUP_BUTTON, GPIO_PIN_SETUP_BUTTON, GPIO_INPUT);
	set_gpio_direction(GPIO_BANK_TUNER_IRQ, GPIO_PIN_TUNER_IRQ, GPIO_INPUT);
	set_gpio_direction(GPIO_BANK_FAULT_Z, GPIO_PIN_FAULT_Z, GPIO_INPUT);
	set_gpio_direction(GPIO_BANK_AMPLIFIER_ENABLE, GPIO_PIN_AMPLIFIER_ENABLE, GPIO_OUTPUT);

	set_gpio_direction(GPIO_BANK_AMPLIFIER_MUTE, GPIO_PIN_AMPLIFIER_MUTE, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_LDO_ENABLE, GPIO_PIN_LDO_ENABLE, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_TUNER_RESET, GPIO_PIN_TUNER_RESET, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_BLUETOOTH_RESET, GPIO_PIN_BLUETOOTH_RESET, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_UART_RX, GPIO_PIN_UART_RX, GPIO_INPUT);
	set_gpio_direction(GPIO_BANK_UART_TX, GPIO_PIN_UART_TX, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_UART_RTS, GPIO_PIN_UART_RTS, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_UART_CTS, GPIO_PIN_UART_CTS, GPIO_INPUT);
	set_gpio_direction(GPIO_BANK_MONO_ENABLE, GPIO_PIN_MONO_ENABLE, GPIO_OUTPUT);

	set_gpio_direction(GPIO_BANK_RS232_TX, GPIO_PIN_RS232_TX, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_RS232_RX, GPIO_PIN_RS232_RX, GPIO_INPUT);

	set_gpio_direction(GPIO_BANK_AUX_ENABLE, GPIO_PIN_AUX_ENABLE, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_LED, GPIO_PIN_LED, GPIO_OUTPUT);
	set_gpio_direction(GPIO_BANK_AMPLIFIER_ENABLE, GPIO_PIN_AMPLIFIER_ENABLE, GPIO_OUTPUT);

	set_port_pinmux(PORT_IO_ISP, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_UP | IO_PORT_FUNC(PORT_IO_ISP_FUNC));
	set_port_pinmux(PORT_IO_EXTERN_TRIGGER_ON, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_EXTERN_TRIGGER_ON_FUNC));
	set_port_pinmux(PORT_IO_EXTERN_TRIGGER_OFF, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_EXTERN_TRIGGER_OFF_FUNC));
	set_port_pinmux(PORT_IO_SETUP_BUTTON, IO_PORT_HYSTERISIS | IO_PORT_FUNC(PORT_IO_SETUP_BUTTON_FUNC));
	set_port_pinmux(PORT_IO_TUNER_IRQ, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_UP | IO_PORT_FUNC(PORT_IO_TUNER_IRQ_FUNC));
	set_port_pinmux(PORT_IO_FAULT_Z, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_FAULT_Z_FUNC));
	set_port_pinmux(PORT_IO_AMPLIFIER_ENABLE, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_AMPLIFIER_ENABLE_FUNC));

	set_port_pinmux(PORT_IO_AMPLIFIER_MUTE, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_DOWN | IO_PORT_FUNC(PORT_IO_AMPLIFIER_MUTE_FUNC));
	set_port_pinmux(PORT_IO_LDO_ENABLE, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_DOWN | IO_PORT_FUNC(PORT_IO_LDO_ENABLE_FUNC));
	set_port_pinmux(PORT_IO_TUNER_RESET, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_UP | IO_PORT_FUNC(PORT_IO_TUNER_RESET_FUNC));
	set_port_pinmux(PORT_IO_BLUETOOTH_RESET, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_BLUETOOTH_RESET_FUNC));
	set_port_pinmux(PORT_IO_MONO_ENABLE, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_DOWN | IO_PORT_FUNC(PORT_IO_MONO_ENABLE_FUNC));

	set_port_pinmux(PORT_IO_RS232_TX, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_DOWN | IO_PORT_FUNC(PORT_IO_RS232_TX_FUNC));
	set_port_pinmux(PORT_IO_RS232_RX, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_DOWN | IO_PORT_FUNC(PORT_IO_RS232_RX_FUNC));

	set_port_pinmux(PORT_IO_AUX_ENABLE, IO_PORT_DIGITAL | IO_PORT_MODE_PULL_DOWN | IO_PORT_FUNC(PORT_IO_AUX_ENABLE_FUNC));
	set_port_pinmux(PORT_IO_LED, IO_PORT_PSEUDO_OPEN_DRAIN_MODE | IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_LED_FUNC));
	set_port_pinmux(PORT_IO_AMPLIFIER_ENABLE, IO_PORT_DIGITAL | IO_PORT_FUNC(PORT_IO_AMPLIFIER_ENABLE_FUNC));

	LPC_IOCON->SCK_LOC = 0x00000000;
}

void hardware_init(void)
{
	gpio_init();

	enable_amplifier(false);
	set_tuner_power(false);
	enable_ldo(false);

#ifndef NO_ADC
	adc_init(ADC_CHANNEL_0);
#endif

	/* IRQ generator (~10ms) */
	SysTick_Config(SystemCoreClock / 100);

	I2C_Init();
	uart_init(UART_MELODY_DEVICE_BAUDRATE);
	RF_Init();

	SystemCoreClockUpdate();

#ifndef NO_WDT
	WDTInit(WDTCLK_SRC_MAIN_CLK);
#endif

#ifndef ARTDEBUG
	rs232_switch(RS232_SWITCH_MODE_BLUETOOTH);
#endif
}

void enable_amplifier(bool enable)
{
	set_gpio(GPIO_BANK_AMPLIFIER_ENABLE, GPIO_PIN_AMPLIFIER_ENABLE, enable);
}

void mute_amplifier(bool mute)
{
	set_gpio(GPIO_BANK_AMPLIFIER_MUTE, GPIO_PIN_AMPLIFIER_MUTE, mute);
}

void enable_led(bool enable)
{
	/* Active low */
	set_gpio(GPIO_BANK_LED, GPIO_PIN_LED, !enable);
}

void toggle_led(void)
{
	toggle_gpio(GPIO_BANK_LED, GPIO_PIN_LED);
}

void enable_ldo(bool enable)
{
	set_gpio(GPIO_BANK_LDO_ENABLE, GPIO_PIN_LDO_ENABLE, enable);
}

void enable_bluetooth(bool enable)
{
	set_gpio(GPIO_BANK_BLUETOOTH_RESET, GPIO_PIN_BLUETOOTH_RESET, enable);
}

void enable_tuner(bool enable)
{
	set_gpio(GPIO_BANK_TUNER_RESET, GPIO_PIN_TUNER_RESET, enable);
}

bool tuner_is_enabled(void)
{
	return get_gpio(GPIO_BANK_TUNER_RESET, GPIO_PIN_TUNER_RESET);
}

int audio_switch(enum audio_switch_mode mode)
{
	bool mono = false;

	if (mode == AUDIO_SWITCH_MODE_MONO)
		mono = true;

	set_gpio(GPIO_BANK_MONO_ENABLE, GPIO_PIN_MONO_ENABLE, mono);
	return mono ? 1 : 0;
}

enum audio_switch_mode audio_switch_state(void)
{
	enum audio_switch_mode mode = AUDIO_SWITCH_MODE_STEREO;

	if (get_gpio(GPIO_BANK_MONO_ENABLE, GPIO_PIN_MONO_ENABLE))
		mode = AUDIO_SWITCH_MODE_MONO;

	return mode;
}

int analog_switch(enum analog_switch_mode mode)
{
	bool aux = false;

	if (mode == ANALOG_SWITCH_MODE_AUX)
		aux = true;

	set_gpio(GPIO_BANK_AUX_ENABLE, GPIO_PIN_AUX_ENABLE, aux);
	return aux ? 1 : 0;
}

int rs232_switch(enum rs232_switch_mode mode)
{
	bool controller = false;

	if (mode == RS232_SWITCH_MODE_CONTROLLER)
		controller = true;

	set_gpio(GPIO_BANK_RS232_TX, GPIO_PIN_RS232_TX, controller);
	return controller ? 0 : 1;
}

enum rs232_switch_mode rs232_switch_state(void)
{
	enum rs232_switch_mode mode = RS232_SWITCH_MODE_BLUETOOTH;

	if (get_gpio(GPIO_BANK_RS232_TX, GPIO_PIN_RS232_TX))
		mode = RS232_SWITCH_MODE_CONTROLLER;
	
	return mode;
}

bool setup_button_status(void)
{
	return get_gpio(GPIO_BANK_SETUP_BUTTON, GPIO_PIN_SETUP_BUTTON);
}

bool trigger_status_on(void)
{
	/* Active low */
	return !get_gpio(GPIO_BANK_EXTERN_TRIGGER_ON, GPIO_PIN_EXTERN_TRIGGER_ON);
}

bool trigger_status_off(void)
{
	/* Active low */
	return !get_gpio(GPIO_BANK_EXTERN_TRIGGER_OFF, GPIO_PIN_EXTERN_TRIGGER_OFF);
}

bool has_tuner_irq(void)
{
	return get_gpio(GPIO_BANK_TUNER_IRQ, GPIO_PIN_TUNER_IRQ);
}

bool fault_z_status(void)
{
	/* Active low */
	return !get_gpio(GPIO_BANK_FAULT_Z, GPIO_PIN_FAULT_Z);
}

void HardFault_Handler(void)
{
	const unsigned int BLINKING_LED_PERIOD = 5;
	struct timer blinking;

	init_timer(&blinking);
	start_timer(&blinking, BLINKING_LED_PERIOD);

	while (true) {
		blinking_led(&blinking);
		delay(1);
	}
}

void blinking_led(struct timer *time)
{
	if (timer_has_fired(time)) {
		reset_timer(time);
		toggle_led();
	}
}
