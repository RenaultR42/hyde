#ifndef H_PRESET
#define H_PRESET

#include "config.h"

void init_preset(void);
void select_radio_preset(enum preset_keys presetnr, enum radio_source source, config_data_t *config);
void store_preset(enum preset_keys key, enum audio_source audio_src, config_data_t *config);
void select_preset(enum preset_keys key, enum audio_source audio_src, config_data_t *config);

#endif
