#ifndef H_PLATFORM_POWER
#define H_PLATFORM_POWER

#include <stdbool.h>

#include "constants.h"
#include "config.h"

void check_external_trigger_power(config_data_t *config);
void set_power_mode(enum power_state mode, enum eco_mode_status eco_mode, enum audio_source source);
void set_power(enum power_state mode, config_data_t *config);

#endif
