#ifndef H_TIMER
#define H_TIMER

#include <stdbool.h>

enum timer_state {
	TIMER_STATE_ENABLED = 0,
	TIMER_STATE_DISABLED,
};

struct timer {
	unsigned int start_time;
	unsigned int timeout;
	enum timer_state state;
};

unsigned int get_monotonic_clock(void);
void init_timer(struct timer *t);
void start_timer(struct timer *t, unsigned int delay);
void reset_timer(struct timer *t);
void disable_timer(struct timer *t);
bool timer_has_fired(struct timer *t);
bool timer_is_enabled(struct timer *t);

#endif
