#ifndef MAIN_H
#define MAIN_H

#include "types.h"
#include "config.h"

#define TIMEOUT_BT_RESET	100	/* 1 sec */
#define PROTECTION_TIMEOUT	100	/* 1 sec */

void setup_handler(config_data_t *config, enum setup_status status);

#endif
