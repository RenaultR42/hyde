#ifndef H_AUDIO_ROUTING
#define H_AUDIO_ROUTING

#include "config.h"

void update_spp(byte spp, config_data_t *config);
void set_route(enum audio_source source);
void set_source(enum audio_source sourceid, config_data_t *config);
void select_source(enum audio_source source, config_data_t *config);

void init_radio_source(enum audio_source source);
void change_radio_mode(enum radio_source source, config_data_t *config);

#endif
