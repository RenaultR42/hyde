#include <stdio.h>

#include "audio.h"
#include "utils.h"
#include "bluetooth.h"

static enum audio_state state = STOP;

void set_audio_state(enum audio_state new_state)
{
	state = new_state;
}

enum audio_state get_audio_state(void)
{
	return state;
}

void reset_tone(config_data_t *config)
{
	art_debug("EQ Flat");

	config->boost = FREQ_BOOST_NONE;

	send_bc128("SET MM=ON ON 0 OFF OFF OFF OFF OFF");
	configuration_changed();
}

void set_tone(enum freq_boost tone, config_data_t *config)
{
	switch (tone) {
	case FREQ_BOOST_BASS:
		send_bc128("SET MM=ON ON 2 ON OFF OFF OFF OFF");
		break;
	case FREQ_BOOST_MID:
		send_bc128("SET MM=ON ON 5 OFF OFF OFF OFF OFF");
		break;
	case FREQ_BOOST_TREB:
		send_bc128("SET MM=ON ON 3 OFF OFF OFF OFF OFF");
		break;
	default:
		reset_tone(config);
		break;
	}

	set_boost(config, tone);
	configuration_changed();
}

enum audio_source get_audio_source(int route)
{
	enum audio_source source;

	switch (route) {
	case 1:
		source = SRC_BLUETOOTH;
		break;
	case 2:
		source = SRC_DABTUNER;
		break;
	case 3:
		source = SRC_FMTUNER;
		break;
	case 4:
		source = SRC_ANALOG;
		break;
	case 5:
		source = SRC_DIGITAL;
		break;
	default:
		source = SRC_FMTUNER;
		break;
	}

	return source;
}

void set_balance(byte balance)
{
	if (balance == DEFAULT_BALANCE) {
		send_bc128("SET BALANCE=%i %i", DEFAULT_BALANCE, DEFAULT_BALANCE);
	} else if (balance < DEFAULT_BALANCE) {	/* Left louder than right */
		send_bc128("SET BALANCE=%i %i", DEFAULT_BALANCE, balance);
	} else if (balance > DEFAULT_BALANCE) {	/* Right louder than left */
		send_bc128("SET BALANCE=%i %i", DEFAULT_BALANCE * 2 - balance, DEFAULT_BALANCE);
	}
}

void balance_left_right(enum balance_button direction, config_data_t *config)
{
	if (direction == BAL_LEFT) {
		if (config->balance > 9) {
			config->balance -= 10;
			set_balance(config->balance);
			configuration_changed();
		} else {
			do_beep(NOTE_SIXTEENTH, TONE_D, config);
		}
	} else if (direction == BAL_RIGHT) {
		if (config->balance < 191) {
			config->balance += 10;
			set_balance(config->balance);
			configuration_changed();
		} else {
			do_beep(NOTE_SIXTEENTH, TONE_D, config);
		}
	}
}

#ifdef DEBUG_GAIN
void in_out_gain(enum direction_button dir)
{
	static byte gain_in = 0;
	static byte gain_out = 0;

	if (dir == DIR_DOWN) {
		gain_in++;
		if (gain_in > 22)
			gain_in = 0;
	} else if (dir == DIR_UP) {
		gain_out++;
		if (gain_out > 15)
			gain_out = 0;
	}

	send_bc128("SET AUDIO_ANALOG %i %i 0 OFF", (int) gain_in, (int) gain_out);
}
#endif

void restore_gain(enum audio_source source)
{
	switch (source) {
	case SRC_FMTUNER:
	case SRC_DABTUNER:
		send_bc128("SET AUDIO_ANALOG=6 15 0 OFF");
		break;
	case SRC_ANALOG:
		send_bc128("SET AUDIO_ANALOG=9 15 0 OFF");
		break;
	default:
		send_bc128("SET AUDIO_ANALOG=9 13 0 OFF");
		break;
	}
}

void volume_up_down(enum direction_button dir, config_data_t *config)
{
	static byte lock_beep = VOL_NOT_MINMAX;

	if (dir == DIR_DOWN) {
		if (config->current_volume > MAX_VOLUME_ATTN) {
			config->current_volume--;
			set_volume(config->current_volume, config->max_volume, config->current_source);
			lock_beep = VOL_NOT_MINMAX;
			configuration_changed();
		} else {
			if (lock_beep != VOL_LOWER_LIMIT) {
				send_bc128("TONE V 16 N D7 L 16");
				delay(10);
				lock_beep = VOL_LOWER_LIMIT;
			}
		}
	} else if (dir == DIR_UP) {
		if (config->current_volume <= config->max_volume)
		{
			config->current_volume++;
			set_volume(config->current_volume, config->max_volume, config->current_source);
			lock_beep = VOL_NOT_MINMAX;
			configuration_changed();
		} else {
			if (lock_beep != VOL_UPPER_LIMIT) {
				send_bc128("TONE V %i N D7 L 16", get_volume_tone(config));
				delay(10);
				art_debug("UpDown: Max vol reached: %d", config->max_volume);
				lock_beep = VOL_UPPER_LIMIT;
			}
		}
	}
}

void set_volume(byte val, byte vol_max, enum audio_source source)
{
	/* TODO: Must use current link id in volume command */
	if (val > vol_max) {
		val = vol_max;
		art_debug("Max vol tresh reached %d", vol_max);
	}

	if ((source == SRC_BLUETOOTH) && has_bluetooth_player()) {
		set_bluetooth_volume(val);
	} else {
		send_bc128("VOLUME 1 %x", val);
	}

	delay(1);
}

void do_beep(int duration, char tone, config_data_t *config)
{
	byte beepvol = get_volume_tone(config);

	if (duration == 0)
		return;

	send_bc128("TONE V %i N %c7 L %i D 20", beepvol, tone, duration);
}

void double_beep(int duration, char tone_1, char tone_2, config_data_t *config)
{
	byte beepvol1, beepvol2;

	beepvol1 = get_volume_tone(config);
	beepvol2 = beepvol1 >> 1;

	if (duration == 0) {
		return;
	}

	send_bc128("TONE V %i N %c7 L %i D 20 N R7 L 16 V %i N %c7 L %i", beepvol1, tone_1, duration, beepvol2, tone_2, duration);
}
