#ifndef H_PLATFORM_HYDE
#define H_PLATFORM_HYDE

#include <stdbool.h>

#include "platform_gpio_hyde.h"
#include "timer.h"

#define UART_MELODY_DEVICE_BAUDRATE	9600

enum analog_switch_mode {
	ANALOG_SWITCH_MODE_AUX = 0,
	ANALOG_SWITCH_MODE_Q8,
};

enum rs232_switch_mode {
	RS232_SWITCH_MODE_CONTROLLER = 0,
	RS232_SWITCH_MODE_BLUETOOTH,
};

enum audio_switch_mode {
	AUDIO_SWITCH_MODE_MONO = 0,
	AUDIO_SWITCH_MODE_STEREO,
};

enum sys_clock {
	SYS_CLOCK_SYS = 0,
	SYS_CLOCK_ROM = 1,
	SYS_CLOCK_RAM = 2,
	SYS_CLOCK_FLASH_REG = 3,
	SYS_CLOCK_FLASH_ARRAY = 4,
	SYS_CLOCK_I2C = 5,
	SYS_CLOCK_GPIO = 6,
	SYS_CLOCK_CT16B0 = 7,
	SYS_CLOCK_CT16B1 = 8,
	SYS_CLOCK_CT32B0 = 9,
	SYS_CLOCK_CT32B1 = 10,
	SYS_CLOCK_SSP0 = 11,
	SYS_CLOCK_UART = 12,
	SYS_CLOCK_ADC = 13,
	SYS_CLOCK_WDT = 15,
	SYS_CLOCK_IOCON = 16,
	SYS_CLOCK_SSP1 = 18,
};

void enable_sys_clock(enum sys_clock clk, bool enable);
void hardware_init(void);
void gpio_init(void);

void enable_amplifier(bool enable);
void mute_amplifier(bool mute);
void enable_led(bool enable);
void toggle_led(void);
void enable_ldo(bool enable);
void enable_bluetooth(bool enable);
void enable_tuner(bool enable);
bool tuner_is_enabled(void);
int audio_switch(enum audio_switch_mode mode);
enum audio_switch_mode audio_switch_state(void);
int analog_switch(enum analog_switch_mode mode);
int rs232_switch(enum rs232_switch_mode mode);
enum rs232_switch_mode rs232_switch_state(void);

bool setup_button_status(void);
bool trigger_status_on(void);
bool trigger_status_off(void);
bool has_tuner_irq(void);
bool fault_z_status(void);

void blinking_led(struct timer *time);
void HardFault_Handler(void);

#endif
