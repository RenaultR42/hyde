#include "preset.h"
#include "utils.h"

static enum preset_keys current_radio_preset[RADIO_SOURCE_MAX]; /* Indexes */

void init_preset(void)
{
	for (byte i = 0; i < RADIO_SOURCE_MAX; i++)
		current_radio_preset[i] = FREE;
}

void select_radio_preset(enum preset_keys presetnr, enum radio_source source, config_data_t *config)
{
	art_debug("Select radio preset %d", presetnr);

	if (config->radio_presets[source][presetnr] != RADIO_FREE) {
		SetRadioChannel(source, config->radio_presets[source][presetnr]);

		current_radio_preset[source] = presetnr;
		config->current_radio_index[source] = config->radio_presets[source][presetnr];
		configuration_changed();
	}
}

void store_preset(enum preset_keys key, enum audio_source audio_src, config_data_t *config)
{
	enum radio_source source = audio_src == SRC_FMTUNER ? RADIO_SOURCE_FM : RADIO_SOURCE_DAB;

	switch (audio_src) {
	case SRC_FMTUNER:
	case SRC_DABTUNER:
		double_beep(NOTE_SIXTEENTH, TONE_E, TONE_G, config);

		store_radio_preset(source, key, config);
		current_radio_preset[source] = key;
		break;
	default:
		break;
	}
}

void select_preset(enum preset_keys key, enum audio_source audio_src, config_data_t *config)
{
	enum radio_source source = audio_src == SRC_FMTUNER ? RADIO_SOURCE_FM : RADIO_SOURCE_DAB;

	switch (audio_src) {
	case SRC_FMTUNER:
	case SRC_DABTUNER:
		if (current_radio_preset[source] != key) {
			select_radio_preset(key, source, config);
		}
		break;
	default:
		break;
	}
}
