#ifndef H_CONFIG
#define H_CONFIG

#include <stdbool.h>

#include "constants.h"
#include "rfdet.h"

#define BLUETOOTH_NAME_CUSTOM_MAX_SIZE	3

/* For rf preset */
#define FREE				0xFF
#define RADIO_FREE			0xFFFF

#define FLASH_CHECK_VAL_INVALID	0xFFFF
#define FLASH_CHECK_VAL_VALID		0xAA55

enum preset_keys {
	PRESET_1 = 0,
	PRESET_2 = 1,
	PRESET_3 = 2,
	PRESET_4 = 3,
	PRESET_5 = 4,
	PRESET_6 = 5,
};

enum audio_source {
	SRC_BLUETOOTH	= 1,
	SRC_DABTUNER	= 2,
	SRC_FMTUNER	= 3,
	SRC_ANALOG	= 4,
	SRC_DIGITAL	= 5,
	SRC_MAX,
};

enum freq_boost {
	FREQ_BOOST_NONE	= 0,
	FREQ_BOOST_BASS	= 1,
	FREQ_BOOST_MID		= 2,
	FREQ_BOOST_TREB	= 3,
};


typedef struct {
	word check_val;
	enum power_state power_state;
	enum audio_source current_source;

	word current_radio_index[RADIO_SOURCE_MAX];

	byte enable_mono;
	enum freq_boost boost;
	byte current_volume;
	byte balance;

	word rf_remote[NUM_RF_REMOTES];

	/* For FM (frequency ? 87.5Mhz) / 50 KHz (0...410)
	 * DAB: service index
	 */
	word radio_presets[RADIO_SOURCE_MAX][NUM_PRESETS];

	byte dab_scanned;
	byte max_volume;

	enum eco_mode_status eco_mode;

	char btname[BLUETOOTH_NAME_CUSTOM_MAX_SIZE + 1];
} config_data_t;

byte get_volume_tone(config_data_t *config);
void apply_config(config_data_t *config);
void copy_config(config_data_t *config, config_data_t *copy);
void auto_save_configuration(config_data_t *config);
void configuration_changed(void);
void load_config(config_data_t *config);
void store_config(config_data_t *config);
void set_boost(config_data_t *config, enum freq_boost boost);
bool remote_is_registered(config_data_t *config, word remote_addr);
void save_remote(config_data_t *config, word remote_addr);
void do_full_reset(config_data_t *config);

#endif
