#ifndef H_PLATFORM_GPIO_HYDE
#define H_PLATFORM_GPIO_HYDE

#define GPIO_BANK_ISP		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_ISP		1
#define PORT_IO_ISP		&LPC_IOCON->PIO0_1
#define PORT_IO_ISP_FUNC	IO_PORT_0_1_PIO0_1

#define GPIO_BANK_EXTERN_TRIGGER_ON		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_EXTERN_TRIGGER_ON		2
#define PORT_IO_EXTERN_TRIGGER_ON		&LPC_IOCON->PIO0_2
#define PORT_IO_EXTERN_TRIGGER_ON_FUNC		IO_PORT_0_2_PIO0_2

#define GPIO_BANK_EXTERN_TRIGGER_OFF		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_EXTERN_TRIGGER_OFF		3
#define PORT_IO_EXTERN_TRIGGER_OFF		&LPC_IOCON->PIO0_3
#define PORT_IO_EXTERN_TRIGGER_OFF_FUNC	IO_PORT_0_3_PIO0_3

#define GPIO_BANK_I2C_SCL	((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_I2C_SCL	4
#define PORT_IO_I2C_SCL	&LPC_IOCON->PIO0_4
#define PORT_IO_I2C_SCL_FUNC	IO_PORT_0_4_I2C_SCL

#define GPIO_BANK_I2C_SDA		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_I2C_SDA		5
#define PORT_IO_I2C_SDA		&LPC_IOCON->PIO0_5
#define PORT_IO_I2C_SDA_FUNC		IO_PORT_0_5_I2C_SDA

#define GPIO_BANK_SETUP_BUTTON			((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_SETUP_BUTTON			6
#define PORT_IO_SETUP_BUTTON			&LPC_IOCON->PIO0_6
#define PORT_IO_SETUP_BUTTON_FUNC		IO_PORT_0_6_PIO0_6

#define GPIO_BANK_TUNER_IRQ		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_TUNER_IRQ		8
#define PORT_IO_TUNER_IRQ		&LPC_IOCON->PIO0_8
#define PORT_IO_TUNER_IRQ_FUNC		IO_PORT_0_8_PIO0_8

#define GPIO_BANK_FAULT_Z		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_FAULT_Z		9
#define PORT_IO_FAULT_Z		&LPC_IOCON->PIO0_9
#define PORT_IO_FAULT_Z_FUNC		IO_PORT_0_9_PIO0_9

#define GPIO_BANK_AMPLIFIER_ENABLE		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_AMPLIFIER_ENABLE		10
#define PORT_IO_AMPLIFIER_ENABLE		&LPC_IOCON->SWCLK_PIO0_10
#define PORT_IO_AMPLIFIER_ENABLE_FUNC		IO_PORT_0_10_PIO0_10

#define GPIO_BANK_ADC		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_ADC		11
#define PORT_IO_ADC		&LPC_IOCON->R_PIO0_11
#define PORT_IO_ADC_FUNC	IO_PORT_0_11_ADC

#define GPIO_BANK_AMPLIFIER_MUTE		((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_AMPLIFIER_MUTE		1
#define PORT_IO_AMPLIFIER_MUTE			&LPC_IOCON->R_PIO1_1
#define PORT_IO_AMPLIFIER_MUTE_FUNC		IO_PORT_1_1_PIO1_1

#define GPIO_BANK_LDO_ENABLE		((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_LDO_ENABLE		2
#define PORT_IO_LDO_ENABLE		&LPC_IOCON->R_PIO1_2
#define PORT_IO_LDO_ENABLE_FUNC	IO_PORT_1_2_PIO1_2

#define GPIO_BANK_TUNER_RESET		((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_TUNER_RESET		3
#define PORT_IO_TUNER_RESET		&LPC_IOCON->SWDIO_PIO1_3
#define PORT_IO_TUNER_RESET_FUNC	IO_PORT_1_3_PIO1_3

#define GPIO_BANK_BLUETOOTH_RESET	((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_BLUETOOTH_RESET	4
#define PORT_IO_BLUETOOTH_RESET	&LPC_IOCON->PIO1_4
#define PORT_IO_BLUETOOTH_RESET_FUNC	IO_PORT_1_4_PIO1_4

#define GPIO_BANK_MONO_ENABLE		((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_MONO_ENABLE		9
#define PORT_IO_MONO_ENABLE		&LPC_IOCON->PIO1_9
#define PORT_IO_MONO_ENABLE_FUNC	IO_PORT_1_9_PIO1_9

#define GPIO_BANK_RS232_TX		((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_RS232_TX		11
#define PORT_IO_RS232_TX		&LPC_IOCON->PIO1_11
#define PORT_IO_RS232_TX_FUNC		IO_PORT_1_11_PIO1_11

#define GPIO_BANK_RS232_RX		((GPIO *)GPIO_BASE_2)
#define GPIO_PIN_RS232_RX		0
#define PORT_IO_RS232_RX		&LPC_IOCON->PIO2_0
#define PORT_IO_RS232_RX_FUNC		IO_PORT_2_0_PIO2_0

#define GPIO_BANK_AUX_ENABLE		((GPIO *)GPIO_BASE_3)
#define GPIO_PIN_AUX_ENABLE		2
#define PORT_IO_AUX_ENABLE		&LPC_IOCON->PIO3_2
#define PORT_IO_AUX_ENABLE_FUNC	IO_PORT_3_2_PIO3_2

#define GPIO_BANK_LED		((GPIO *)GPIO_BASE_3)
#define GPIO_PIN_LED		4
#define PORT_IO_LED		&LPC_IOCON->PIO3_4
#define PORT_IO_LED_FUNC	IO_PORT_3_4_PIO3_4

#define GPIO_BANK_UART_RX		((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_UART_RX		6
#define PORT_IO_UART_RX		&LPC_IOCON->PIO1_6
#define PORT_IO_UART_RX_FUNC		IO_PORT_1_6_UART_RX

#define GPIO_BANK_UART_TX		((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_UART_TX		7
#define PORT_IO_UART_TX		&LPC_IOCON->PIO1_7
#define PORT_IO_UART_TX_FUNC		IO_PORT_1_7_UART_TX

#define GPIO_BANK_UART_RTS		((GPIO *)GPIO_BASE_1)
#define GPIO_PIN_UART_RTS		5
#define PORT_IO_UART_RTS		&LPC_IOCON->PIO1_5
#define PORT_IO_UART_RTS_FUNC		IO_PORT_1_5_UART_RTS

#define GPIO_BANK_UART_CTS		((GPIO *)GPIO_BASE_0)
#define GPIO_PIN_UART_CTS		7
#define PORT_IO_UART_CTS		&LPC_IOCON->PIO0_7
#define PORT_IO_UART_CTS_FUNC		IO_PORT_0_7_UART_CTS

#endif
