#include <string.h>
#include "LPC11xx.h"
#include "iap.h"

/*                                                              FLASH   RAM
 * LPC1111                                                      8 kB
 * LPC1112                                                      16 kB
 * LPC1113/201/202/203                                          24 kB    2 kB
 * LPC1113/301/302/303/323/333                                  24 kB    2 kB

 * LPC11D14/201/202/203                                         32 kB    4 kB
 * LPC11D14/301/302/303/323/333                                 32 kB    4 kB
 * LPC1114/201/202/203                                          32 kB    4 kB
 * LPC1114/301/302/303                                          32 kB    4 kB
 * LPC1114/323                                                  48 kB    8 kB
 * LPC1114/333                                                  56 kB    8 kB
 * LPC1115                                                      64 kB    8 kB
 * LPC11C12/301                                                 16 kB    8 kB
 * LPC11C14/301                                                 32 kB    8 kB
 * LPC11C22/301                                                 16 kB    8 kB
 * LPC11C24/301                                                 32 kB    8 kB
 */

/* Sector number Sectorsize                                     [kB]    Address range
 * LPC1110 LPC1111 LPC1112 LPC1113 LPC1114                      (4kB)           (8kB)
 * LPC11C12/L                                                   (24 kB)
 * LPC11C14/L
 * PC11C22 PC11C24                                              (16 kB)         (32 kB)
*/

#define FLASH_BLOCKSIZE	0x0100
#define FLASH_SECTORSIZE	0x1000

/* Sector                size    start               -end
 * 0                     4 kB    0x0000 0000 -0x0000 0FFF
 * 1                     4 kB    0x0000 1000 -0x0000 1FFF
 * 2                     4 kB    0x0000 2000 -0x0000 2FFF
 * 3                     4 kB    0x0000 3000 -0x0000 3FFF
 * 4                     4 kB    0x0000 4000 -0x0000 4FFF
 * 5                     4 kB    0x0000 5000 -0x0000 5FFF
 * 6                     4 Kb    0x0000 6000 -0x0000 6FFF
 * 7                     4 Kb    0x0000 7000 -0x0000 7FFF
 */
enum iap_sectors {
	SECTOR5	= 5,
	SECTOR6	= 6,
	SECTOR7	= 7,
};

/* Which sector will be used for storage? */
#define SECTOR 				SECTOR6
#define BLOCK_NUM				0

#define SECTOR5_ADDR  0x00005000
#define SECTOR6_ADDR  0x00006000
#define SECTOR7_ADDR  0x00007000

#if SECTOR == SECTOR5
#define SECTOR_ADDR  SECTOR5_ADDR
#elif SECTOR==SECTOR6
#define SECTOR_ADDR  SECTOR6_ADDR
#elif SECTOR==SECTOR7
#define SECTOR_ADDR  SECTOR7_ADDR
#endif

#define IAP_LOCATION 0x1fff1ff1
typedef void (*IAP)(unsigned int[], unsigned int[]);

/* IAP function */
static IAP mIAPEntry = (IAP) IAP_LOCATION;

/* Allocate memory for non-volatile memory so it isn't used by the linker for something else */
#if 0
static unsigned char mSectorMemory[FLASH_SECTORSIZE] __attribute__((__section(".ARM.__at_0x6000")));
#else
static unsigned char *mSectorMemory = (unsigned char *)SECTOR_ADDR;
#endif

enum iap_status {
	CMD_SUCCESS 				= 0,	//Command is executed successfully.
	INVALID_COMMAND 			= 1,	//Invalid command.
	SRC_ADDR_ERROR 			= 2,	//Source address is not on a word boundary.
	DST_ADDR_ERROR 			= 3,	//Destination address is not on a correct boundary.
	SRC_ADDR_NOT_MAPPED 			= 4,	//Source address is not mapped in the memory map. Count value is taken in to consideration where applicable.
	DST_ADDR_NOT_MAPPED 			= 5,	//Destination address is not mapped in the memory map. Count value is taken in to consideration where applicable.
	COUNT_ERROR 				= 6,	//Byte count is not multiple of 4 or is not a permitted value.
	INVALID_SECTOR 			= 7,	//Sector number is invalid.
	SECTOR_NOT_BLANK 			= 8,	//Sector is not blank.
	SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION = 9,	//Command to prepare sector for write operation was not executed.
	COMPARE_ERROR 				= 10,	//Source and destination data is not same.
	BUSY 					= 11,	//Flash programming hardware interface is busy.
};

/* IAP Command CODEs */
enum iap_cmd {
	PREPARE_SECTOR		= 50,
	RAM_TO_FLASH		= 51,
	ERASE_SECTOR		= 52,
	BLANK_CHECK		= 53,
	READ_PART_ID		= 54,
	READ_BOOTCODEVERSION	= 55,
	COMPARE		= 56,
	REINVOKE_ISP		= 57,
	READ_UID		= 58,
	ERASE_PAGE		= 59,	/* Not supported in LPC11C24 */
};

/* CPU clock in kHz */
#define CPU_CLK		48000
#define CMD_SIZE	5

static void send_cmd(unsigned int *command, unsigned int *result)
{
	__disable_irq();
	mIAPEntry(command, result);
	__enable_irq();
}

/**************************************************************************
DOES:    Erases a sector
RETURNS: TRUE for success, FALSE for error
**************************************************************************/
bool erase_sector(void)
{
	unsigned int command[CMD_SIZE], result[CMD_SIZE];

	/* Prepare */
	command[0] = PREPARE_SECTOR;
	command[1] = SECTOR;
	command[2] = SECTOR;

	send_cmd(command, result);

	if (result[0] != CMD_SUCCESS)
		return false;

	/* Erase sector */
	command[0] = ERASE_SECTOR;
	command[1] = SECTOR;
	command[2] = SECTOR;
	command[3] = CPU_CLK;

	send_cmd(command, result);

	if (result[0] != CMD_SUCCESS)
		return false;

	return true;
}

bool read_block(int readcnt, unsigned char *buffer)
{
	unsigned short octet;

	for (octet = 0; octet < readcnt; octet++)
		buffer[octet] = mSectorMemory[octet];

	return true;
}

/**************************************************************************
DOES:    Writes a buffer of max 256 bytes into a specific sector at block 0 offset
RETURNS: TRUE for success, FALSE for error
**************************************************************************/
bool write_block(int writecnt, unsigned char *data)
{
	unsigned short octet;
	unsigned char buffer[FLASH_BLOCKSIZE];
	unsigned int command[CMD_SIZE], result[CMD_SIZE];

	/* Set up buffer to contain 0xFF */
	for (octet = 0; octet < FLASH_BLOCKSIZE; octet++)
		buffer[octet] = 0xFF;

	for (octet = 0; octet < writecnt; octet++)
		buffer[octet] = data[octet];

	/* Prepare sector */
	command[0] = PREPARE_SECTOR;
	command[1] = SECTOR;
	command[2] = SECTOR;

	send_cmd(command, result);

	if (result[0] != CMD_SUCCESS)
		return false;

	/* Write to sector */
	command[0] = RAM_TO_FLASH;
	command[1] = (unsigned int) (SECTOR_ADDR + (BLOCK_NUM * FLASH_BLOCKSIZE));
	command[2] = (unsigned int) buffer;
	command[3] = FLASH_BLOCKSIZE;
	command[4] = CPU_CLK;

	send_cmd(command, result);

	if (result[0] != CMD_SUCCESS)
		return false;

	/* Verify */
	command[0] = COMPARE;
	command[1] = (unsigned int) (SECTOR_ADDR + (BLOCK_NUM * FLASH_BLOCKSIZE));
	command[2] = (unsigned int) (buffer);
	command[3] = FLASH_BLOCKSIZE;

	send_cmd(command, result);

	if (result[0] != CMD_SUCCESS)
		return false;

	return true;
}

/**************************************************************************
DOES:    Reads the Unique ID, index must be 0...3
RETURNS: selected UID int
**************************************************************************/
unsigned int read_uid(unsigned char index)
{
	unsigned int command[CMD_SIZE], result[CMD_SIZE];

	/* Prepare */
	command[0] = READ_UID;

	send_cmd(command, result);
	return result[index];
}
