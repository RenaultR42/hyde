#ifndef H_I2C
#define H_I2C

#include "types.h"

void I2C_Init(void);
byte WaitIICXferDone(void);
byte IICReceive(byte deviceaddr, byte rxbytecnt);
byte IICSend(byte deviceaddr, byte *txdata, int txbytecnt);
byte IICTransfer(byte deviceaddr, byte txbytecnt, byte *txbuf, byte rxbytecnt);
byte GetI2CError(void);

#define I2C_AA         0x04
#define I2C_SI         0x08
#define I2C_STO        0x10
#define I2C_STA        0x20
#define I2C_I2EN       0x40	/* I2C Control Set Register */
#define IIC_TXBUFSIZE	64	/* largest number of bytes to sent in 1 transfer */
#define IIC_RXBUFSIZE	64	/* largest number of bytes to read in 1 transfer */

typedef struct {
	byte DeviceAddress;

	byte SendBuffer[IIC_TXBUFSIZE];	// buffer that we fill and will be sent in the background
	byte *TxDataPtr;	// keeps track of the byte to sent.
	byte NumTxByte;		// number of bytes we would like to send

	byte ReceiveBuffer[IIC_RXBUFSIZE];
	byte *RxDataPtr;	// keeps track of the byte to receive.
	byte NumRxByte;		// number of bytes we woluld like to receive.
	byte CntReceived;	// the actual number received 

	byte Busy;		// the iic core is busy sending or receiving
	byte Error;		// this gets set when an error occurs during an i2c transfer.
} t_iic;

//#define READ_I2C_DATA  0x01 ///<Force read

//#define I2C_RX_RING_BUFFER_BIT 4   ///< Valid defines: 1-7
/// \brief The receive ring buffer size
//#define I2C_RX_RING_BUFFER_SIZE (1<<I2C_RX_RING_BUFFER_BIT) //-Must be 2^n

enum i2c_bus_status {
	I2C_OK,		/* Transaction completed successfully */
	I2C_ERROR,		/* Transaction failed */
	I2C_BUSY,		/* Transaction in progress */
	I2C_TIMEOUT	= 0x04, /* Transaction failed */
	I2C_LOST_ARB	= 0x08, /* Transaction failed */
	I2C_NO_RBUF	= 0x10, /* I2cRxBuff not initialized */
	I2C_CHKSUM_ERR	= 0x20, /* Transaction failed */
	I2C_RESTART	= 0x40
};

enum i2c_error {
	E_NONE		= 0,
	E_NOACK	= 1,
	E_BUS_LOST	= 2,
	E_BUS_ERROR	= 3,
	E_UNKNOWN	= 4,
	E_BUFFSIZE	= 5,
	E_NOBUFF	= 6,
};

enum i2c_status {
	I2C_STATUS_ERROR = 0,
	I2C_STATUS_START_CONDITION_TX = 0x08,
	I2C_STATUS_REPEATED_START_CONDITION_TX = 0x10,
	I2C_STATUS_TX_COMPLETED_ACK_RECEIVED = 0x18,
	I2C_STATUS_TX_COMPLETED_ACK_NOT_RECEIVED = 0x20,
	I2C_STATUS_ACK_RECEIVED = 0x28,
	I2C_STATUS_ACK_NOT_RECEIVED = 0x30,
	I2C_STATUS_ARBITRATION_LOST = 0x38,
	I2C_STATUS_READ_REQ_ACK_RECEIVED = 0x40,
	I2C_STATUS_READ_REQ_ACK_NOT_RECEIVED = 0x48,
	I2C_STATUS_ACK_SENT = 0x50,
	I2C_STATUS_ACK_NOT_SENT = 0x58,
	I2C_STATUS_SLAVE_RX_COMPLETED_ACK_SENT = 0x60,
	I2C_STATUS_SLAVE_ARBITRATION_LOST = 0x68,
	I2C_STATUS_SLAVE_GENERAL_CALL_RECEIVED_ACK_RECEIVED = 0x70,
	I2C_STATUS_SLAVE_ARBITRATION_LOST_ACK_RECEIVED = 0x78,
	I2C_STATUS_SLAVE_DATA_RECEIVED = 0x80,
	I2C_STATUS_SLAVE_DATA_RECEIVED_NACK_SENT = 0x88,
	I2C_STATUS_SLAVE_DATA_RECEIVED_ACK_RECEIVED = 0x90,
	I2C_STATUS_SLAVE_DATA_RECEIVED_NACK_RECEIVED = 0x98,
	I2C_STATUS_SLAVE_STOP_OR_RESTART = 0xA0,
	I2C_STATUS_SLAVE_READ_REQ_RECEIVED = 0xA8,
	I2C_STATUS_SLAVE_SENT_NACK = 0xB0,
	I2C_STATUS_SLAVE_REENABLE_SLAVE_RX = 0xC0,
	I2C_STATUS_SLAVE_SENT_ACK = 0xC8,
};

//macros
//#define i2c_start_tx()        LPC_I2C->CONSET = 0x00000020   ///< Sends the start command
//#define i2c_clr_intrpt_flag() LPC_I2C->CONCLR = 0x00000008   ///< Clear flag
//#define i2c_stop_tx()         LPC_I2C->CONSET = 0x00000010   ///< Sends a stop command
//#define i2c_ack()             LPC_I2C->CONSET = 0x00000004   ///< Sends an acknowledge
//#define i2c_nack()            LPC_I2C->CONCLR = 0x00000004   ///< Sends a Not acknowledged

#endif
