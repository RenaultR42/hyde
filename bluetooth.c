#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "bluetooth.h"
#include "LPC11xx.h"
#include "main.h"
#include "gpio.h"
#include "platform_hyde.h"
#include "audio.h"
#include "audio_routing.h"
#include "utils.h"
#include "timer.h"

extern config_data_t config_data;

#define LINK_SPP '5'
#define GATT_LINE_SIZE_MAX (SERIAL_BUFFER_SIZE_MAX - SERIAL_BUFFER_SIZE_MAX / 4 - 4)

enum reply_status {
	REPLY_NONE	= 0,
	REPLY_OK	= 1,
	REPLY_ERROR	= 2,
	REPLY_PENDING	= 3,
};

enum bc128_cmd {
	BC128_CMD_NOT_SUPPORTED = -1,
	BC128_CMD_READY = 0,
	BC128_CMD_OK,
	BC128_CMD_ERROR,
	BC128_CMD_PENDING,
	BC128_CMD_AVRCP_PLAY,
	BC128_CMD_AVRCP_PAUSE,
	BC128_CMD_OPEN_OK,
	BC128_CMD_CLOSE_OK,
	BC128_CMD_A2DP_STREAM_START,
	BC128_CMD_A2DP_STREAM_SUSPEND,
	BC128_CMD_A2DP_STREAM_STOP,
	BC128_CMD_PAIR_OK,
	BC128_CMD_PAIR_ERROR,
	BC128_CMD_ABS_VOL,
	BC128_CMD_RECV,
#ifndef DISABLE_TWS
	BC128_CMD_INQUIRY_TWS,
#endif
#ifndef DISABLE_BLE
	BC128_CMD_BLE_READ,
	BC128_CMD_BLE_WRITE,
#endif
};

enum bc128_cmd_flag {
	BC128_CMD_FLAG_NONE				= 0x0,
	BC128_CMD_FLAG_UPDATE_SPP			= 0x1,
	BC128_CMD_FLAG_UPDATE_DEVICE_ID		= 0x2,
	BC128_CMD_FLAG_TWS_CONNECT			= 0x4,
	BC128_CMD_FLAG_DISCOVERABLE			= 0x8,
};

static enum reply_status bc128_reply = REPLY_NONE;
static bool bc128_booted = false;
static enum pairing_status pairing_status = PAIR_NONE;
static enum bc128_cmd_flag cmd_flags = BC128_CMD_FLAG_NONE;

static byte spp = 0;
static char new_device_id = 0;
static char device_id_player = 0;
static char tws_mac_address[SERIAL_BUFFER_SIZE_MAX] = { '\0' };

static char ble_link_id = 0;

void init_bc128(config_data_t *config)
{
	char *bt_init_str[] = {
		"SET AUDIO=0 0", "SET MUSIC_META_DATA=OFF", "SET UART_CONFIG=9600 ON 0", "SET SSP_CAPS=3",
		"SET AUDIO_ANALOG=6 15 0 OFF", "SET BT_VOL_CONFIG=5 5 1F", "SET AUDIO_DIGITAL=2 48000 64 5",
		"SET CODEC=1 OFF", "CONNECTABLE ON",
#ifdef DEBUG_APP
		"SET PROFILES=0 0 3 0 3 1 2 1 0 0 0 0",
#else
		"SET PROFILES=0 0 3 0 3 1 0 0 0 0 0 0",
#endif
		"SET AUTOCONN=0", "SET DISCOVERABLE=1 0", "SET MM=ON ON 2 OFF OFF OFF OFF OFF", "SET COD=200428",
		"SET NAME_SHORT=ASHYDE",
#ifndef DISABLE_TWS
		"SET TWS_CONFIG=ON 1 2",
#endif
#ifndef DISABLE_BLE
		"BLE_CONFIG=0 OFF OFF 40 ON",
		"BLE_CONN_PARAM=128 12 6 40 0 400 50 400 400 64 400 400",
#endif
		NULL
	};
	int i = 0;
	char *pcmd = NULL;

	do {
		pcmd = bt_init_str[i++];
		send_bc128(pcmd);
		delay(2);
	} while (pcmd != NULL);

	save_bc128_config();
	delay(10);
	reset_bc128(config);
}

void save_bc128_config(void)
{
	send_bc128("WRITE");
	delay(1);
}

bool has_bluetooth_player(void)
{
	bool has_player = true;

	if (device_id_player == 0)
		has_player = false;

	return has_player;
}

enum pairing_status get_bt_pairing_status(void)
{
	return pairing_status;
}

void set_bt_pairing_status(enum pairing_status status)
{
	pairing_status = status;
}

static bool bc128_has_booted(void)
{
	return bc128_booted;
}

void send_gatt_db(char *buffer, int size)
{
	const int nb_lines = (buffer_size * 2) / GATT_LINE_SIZE_MAX + 1;
	int i, j;

	bc128_send("BLE_SET_DB %X", buffer_size / 2);

	for (i = 0; i < nb_lines; i++) {
		int size = buffer_size * 2 - i * GATT_LINE_SIZE_MAX;
		char line[GATT_LINE_SIZE_MAX + 1] = { '\0' };
		int current_char;

		for (j = 0, current_char = 0; j * 2 < size; j++) {
			snprintf(&line[current_char], 3, "%02X", buffer[i + j]);
			current_char += 2;

			if ((j + 1) % 2 == 0) {
				line[current_char++] = ' ';
			}
		}

		line[current_char] = '\0';
		send_bc128(line);
	}
}

void reset_bc128(config_data_t *config)
{
	const int MAX_TRIES = 5;
	int try = 0;
	struct timer timeout;
	char gatt_buffer[GATT_DB_SIZE_MAX] = { '\0' };

	bc128_booted = false;

	enable_bluetooth(false);
	rs232_switch(RS232_SWITCH_MODE_BLUETOOTH);
	delay(2);
	enable_bluetooth(true);
	delay(1);

	init_timer(&timeout);
	start_timer(&timeout, TIMEOUT_BC128_CMD);

	do {
		parse_cmd_bluetooth(get_uart_message(), config);

		if (timer_has_fired(&timeout)) {
			reset_timer(&timeout);
			try++;
		}
	} while (!bc128_has_booted() && try <= MAX_TRIES);

	if (bc128_has_booted) {
		buffer_size = init_gatt_buffer(gatt_buffer);

		send_gatt_db(gatt_buffer, buffer_size);
	}
}

void control_bt(enum audio_state mode)
{
	if (has_bluetooth_player()) {
		bool skip = false;
		char cmd[SERIAL_BUFFER_SIZE_MAX];
		sprintf(cmd, "MUSIC %c%i ", device_id_player, 0);

		switch (mode) {
		case PLAY:
			strcat(cmd, "PLAY");
			break;
		case PAUSE:
			strcat(cmd, "PAUSE");
			break;
		case STOP:
			strcat(cmd, "STOP");
			break;
		case NEXT:
			strcat(cmd, "FORWARD");
			break;
		case PREV:
			strcat(cmd, "BACKWARD");
			break;
		case FFWD_PRESS:
			strcat(cmd, "FF_PRESS");
			break;
		case FREW_PRESS:
			strcat(cmd, "REW_PRESS");
			break;
		case FFWD_RELEASE:
			strcat(cmd, "FF_RELEASE");
			break;
		case FREW_RELEASE:
			strcat(cmd, "REW_RELEASE");
			break;
		default:
			skip = true;
			break;
		}

		if (!skip) {
			send_bc128(cmd);
		}
	}

	switch (mode) {
	case PAIR_ON:
		send_bc128("SET SSP_CAPS=3");
		break;
	case PAIR_OFF:
#if 1
		send_bc128("SET SSP_CAPS=3");
#endif
		break;
#ifndef DISABLE_TWS
	case INQUIRY_TWS:
		send_bc128("INQUIRY 10 4 ON");
		break;
#endif
	default:
		break;
	}
}

void set_bluetooth_power(enum power_state mode)
{
	device_id_player = 0;

	switch (mode) {
	case POWER_OFF:
		send_bc128("POWER OFF");
		break;
	case POWER_ON:
	default:
		send_bc128("POWER ON");
		break;
	}
}

void update_bt_name(char *name, config_data_t *config)
{
	control_bt(PAIR_OFF);

	snprintf(config->btname, BLUETOOTH_NAME_CUSTOM_MAX_SIZE + 1, "%s", name);
	config->btname[BLUETOOTH_NAME_CUSTOM_MAX_SIZE] = '\0';

	send_bc128("SET NAME=ArtSound HYDE [%s]", config->btname);
	save_bc128_config();
	store_config(config);

	reset_bc128(config);

	delay(2);
}

void update_device_playback(char device_id_playback, enum audio_source source, config_data_t *config)
{
	if (device_id_playback != device_id_player) {
		/* Player was cancelled */
		if (device_id_playback == '0') {
			device_id_player = 0;

			if (source == SRC_BLUETOOTH) {
				send_bc128("TONE V %i N G7 L 8 N R7 L 16 N C7 L 4", get_volume_tone(config));
				delay(1);
			}

			set_audio_state(STOP);
		/* No player yet, new player is used */
		} else if (!has_bluetooth_player()) {
			device_id_player = device_id_playback;
		/* New player replaces current player */
		} else {
			if (source == SRC_BLUETOOTH) {
				send_bc128("MUSIC %c%i PAUSE", device_id_player, 0);
			}

			device_id_player = device_id_playback;
		}

		if (source == SRC_BLUETOOTH) {
			delay(50);
			if (has_bluetooth_player()) {
				set_route(SRC_BLUETOOTH);
				delay(50);
			}
		}
	}
}

static enum bc128_cmd get_bluetooth_command(const char *string)
{
	enum bc128_cmd cmd = BC128_CMD_NOT_SUPPORTED;

	if (find_substr(string, "Ready")) {
		cmd = BC128_CMD_READY;
	} else if (find_substr(string, "OK") == 0) {
		cmd = BC128_CMD_OK;
	} else if (find_substr(string, "ERROR 0x")) {
		cmd = BC128_CMD_ERROR;
	} else if (find_substr(string, "PENDING")) {
		cmd = BC128_CMD_PENDING;
	} else if (find_substr(string, "AVRCP_PLAY")) {
		cmd = BC128_CMD_AVRCP_PLAY;
	} else if (find_substr(string, "AVRCP_PAUSE")) {
		cmd = BC128_CMD_AVRCP_PAUSE;
	} else if (find_substr(string, "CLOSE_OK")) {
		cmd = BC128_CMD_CLOSE_OK;
	} else if (find_substr(string, "A2DP_STREAM_START")) {
		cmd = BC128_CMD_A2DP_STREAM_START;
	} else if (find_substr(string, "A2DP_STREAM_SUSPEND")) {
		cmd = BC128_CMD_A2DP_STREAM_SUSPEND;
	} else if (find_substr(string, "A2DP_STREAM_STOP")) {
		cmd = BC128_CMD_A2DP_STREAM_STOP;
	} else if (find_substr(string, "PAIR_OK")) {
		cmd = BC128_CMD_PAIR_OK;
	} else if (find_substr(string, "PAIR_ERROR")) {
		cmd = BC128_CMD_PAIR_ERROR;
	} else if (find_substr(string, "ABS_VOL")) {
		cmd = BC128_CMD_ABS_VOL;
	} else if (find_substr(string, "OPEN_OK")) {
		cmd = BC128_CMD_OPEN_OK;
	} else if (find_substr(string, "RECV")) {
		cmd = BC128_CMD_RECV;
	}
#ifndef DISABLE_TWS
	else if (find_substr(string, "INQUIRY")) {
		cmd = BC128_CMD_INQUIRY_TWS;
	}
#endif
#ifndef DISABLE_BLE
	else if (find_substr(string, "BLE_READ")) {
		cmd = BC128_CMD_BLE_READ;
	} else if (find_substr(string, "BLE_WRITE")) {
		cmd = BC128_CMD_BLE_WRITE;
	}
#endif

	return cmd;
}

static char get_device_id(enum bc128_cmd cmd, char *string)
{
	char arg[SERIAL_BUFFER_SIZE_MAX];
	char delim = ' ';
	char device_id = 0;

	switch (cmd) {
	case BC128_CMD_AVRCP_PAUSE:
	case BC128_CMD_AVRCP_PLAY:
	case BC128_CMD_OPEN_OK:
	case BC128_CMD_CLOSE_OK:
	case BC128_CMD_A2DP_STREAM_START:
	case BC128_CMD_A2DP_STREAM_STOP:
	case BC128_CMD_A2DP_STREAM_SUSPEND:
	case BC128_CMD_ABS_VOL:
		if (get_argument(string, arg, delim, 1)) {
			device_id = arg[0];
		}
		break;
	default:
		break;
	}

	return device_id;
}

static bool get_bluetooth_mac_address(enum bc128_cmd cmd, char *string, char *mac_address)
{
	bool ret = false;
	char delim = ' ';

	switch (cmd) {
#ifndef DISABLE_TWS
	case BC128_CMD_INQUIRY_TWS:
		if (get_argument(string, mac_address, delim, 1)) {
			ret  = true;
		}
		break;
#endif
	default:
		ret = false;
		break;
	}

	return ret;
}

void parse_cmd_bluetooth(serial_comm_t *msg, config_data_t *config)
{
	enum bc128_cmd received_cmd;
	char device_id_cmd;
	char device_id_playback = 0;
	char arg[SERIAL_BUFFER_SIZE_MAX];
	char *strp;

	if (!msg->cmd_read) {
		received_cmd = get_bluetooth_command((char *)msg->cmd_data_buf);

		/* TODO: after reset, apply default values like volume or balance */
		switch (received_cmd) {
		case BC128_CMD_READY:
			bc128_booted = true;
			break;
		case BC128_CMD_OK:
			bc128_reply = REPLY_OK;
			break;
		case BC128_CMD_ERROR:
			bc128_reply = REPLY_ERROR;
			break;
		case BC128_CMD_PENDING:
			bc128_reply = REPLY_PENDING;
			break;
		default:
			device_id_cmd = get_device_id(received_cmd, (char *)msg->cmd_data_buf);
			break;
		}

		if (has_bluetooth_player()) {
			switch (received_cmd) {
			case BC128_CMD_AVRCP_PLAY:
				if (device_id_cmd == device_id_player) {
					device_id_playback = device_id_cmd;
					set_audio_state(PLAY);
				}

				if (config->current_source != SRC_BLUETOOTH) {
					device_id_playback = device_id_cmd;
					delay(2);

					set_route(SRC_BLUETOOTH);
					config->current_source = SRC_BLUETOOTH;
					set_audio_state(PLAY);
					device_id_player = device_id_playback;
					delay(50);
				}
				break;
			case BC128_CMD_AVRCP_PAUSE:
				if (device_id_cmd == device_id_player) {
					set_audio_state(PAUSE);
				}
				break;
			/* {C,L,O,S,E,_,O,K, ,X,0, ,A,2,D,P} */
			/* {C,L,O,S,E,_,O,K, ,X,0, ,S,P,P} */
			case BC128_CMD_CLOSE_OK:
				/* Remove current player */
				if (get_argument((char *)msg->cmd_data_buf, arg, ' ', 2)) {
					if (find_substr(arg, "A2DP")) {
						if (device_id_cmd == device_id_player) {
							device_id_playback = '0';
						}
					}

#ifndef DISABLE_BLE
					else if (find_substr(arg, "BLE")) {
						if (device_id_cmd == ble_link_id) {
							ble_link_id = 0;
						}
					}
#endif

#ifdef DEBUG_APP
					else if (find_substr(arg, "SPP")) {
						if (device_id_cmd == ble_link_id) {
							ble_link_id = 0;
						}
					}
#endif
				}
				break;
			/* A2DP_STREAM_START <link id> */
			case BC128_CMD_A2DP_STREAM_START:
				/* Start stream from another link */
				if (device_id_cmd != device_id_player) {
					device_id_playback = device_id_cmd;
					set_audio_state(PLAY);
				}
				break;
			/* A2DP_STREAM_STOP <link id> */
			case BC128_CMD_A2DP_STREAM_STOP:
			/* A2DP_STREAM_SUSPEND <link id> */
			case BC128_CMD_A2DP_STREAM_SUSPEND:
				if (device_id_cmd == device_id_player) {
					set_audio_state(STOP);
				}
				break;
			/* {A,B,S,_,V,O,L, ,X,1, ,1,2,7} */
			case BC128_CMD_ABS_VOL:
				/* Sync volume from Bluetooth chip and internal volume setup */
				if (config->current_source == SRC_BLUETOOTH) {
					if (device_id_cmd == device_id_player && get_argument((char *)msg->cmd_data_buf, arg, ' ', 2)) {
						config->current_volume = (byte) (atoi(arg) / 4);	/* 127 -> 31 */
						configuration_changed();
					}
				}
				break;
			default:
				break;
			}
		} else {
			switch (received_cmd) {
			/* PAIR_OK B047BFE3ED3F */
			case BC128_CMD_PAIR_OK:
				set_bt_pairing_status(PAIR_OK);
				break;
			/* PAIR_ERROR B047BFE3ED3F */
			case BC128_CMD_PAIR_ERROR:
				set_bt_pairing_status(PAIR_ERROR);
				break;
			/* {O,P,E,N,_,O,K, ,X,0, ,A,2,D,P} */
			/* {O,P,E,N,_,O,K, ,X,0, ,S,P,P} */
			case BC128_CMD_OPEN_OK:
				/* If no player, new player becomes current player */
				if (get_argument((char *)msg->cmd_data_buf, arg, ' ', 2)) {
					if (find_substr(arg, "A2DP")) {
						if (device_id_player == 0) {
							if (config->current_source == SRC_BLUETOOTH) {
								device_id_playback = device_id_cmd;
							} else {
								device_id_player = device_id_cmd;
							}
						}
					}
#ifndef DISABLE_TWS
					else if (find_substr(arg, "TWS")) {
						cmd_flags |= BC128_CMD_FLAG_DISCOVERABLE;
					}
#endif

#ifndef DISABLE_BLE
					else if (find_substr(arg, "BLE")) {
						if (ble_link_id == 0) {
							ble_link_id = device_id_cmd;
						}
					}
#endif

#ifdef DEBUG_APP
					else if (find_substr(arg, "SPP")) {
						if (ble_link_id == 0) {
							ble_link_id = device_id_cmd;
						}
					}
#endif
				}
				break;

#ifndef DISABLE_TWS
			case BC128_CMD_INQUIRY_TWS:
				if (get_bluetooth_mac_address(received_cmd, (char *)msg->cmd_data_buf, tws_mac_address)) {
					cmd_flags |= BC128_CMD_FLAG_TWS_CONNECT;
				}
				break;
#endif

#ifndef DISABLE_BLE
			case BC128_CMD_BLE_READ:
				parse_ble_gatt_read_request((char *)msg->cmd_data_buf, config);
				break;
			case BC128_CMD_BLE_WRITE:
				parse_ble_gatt_write_request((char *)msg->cmd_data_buf, config);
				break;
#endif

			case BC128_CMD_RECV:
#ifdef DEBUG_APP
				if (get_argument((char *)msg->cmd_data_buf, arg, ' ', 1)) {
					if (arg[1] == LINK_SPP) {
						strp = strstr((char *) msg->cmd_data_buf, "*S");
						if (strp) {
							spp = get_audio_source((char) (strp + 2) - '0') | 0x10;
							cmd_flags |= BC128_CMD_FLAG_UPDATE_SPP;
						}
					}
				}
#endif
				break;
			default:
				break;
			}
		}

		if (device_id_playback) {
			cmd_flags |= BC128_CMD_FLAG_UPDATE_DEVICE_ID;
			new_device_id = device_id_playback;
		}

		msg->cmd_read = true;
	}
}

void process_bluetooth_queue(config_data_t *config)
{
	if (new_uart_message_is_available()) {
		parse_cmd_bluetooth(get_uart_message(), config);
	}

	if ((cmd_flags & BC128_CMD_FLAG_UPDATE_SPP)) {
		update_spp(spp, config);
	}

	if ((cmd_flags & BC128_CMD_FLAG_UPDATE_DEVICE_ID)) {
		update_device_playback(new_device_id, config->current_source, config);
	}

	if ((cmd_flags & BC128_CMD_FLAG_TWS_CONNECT)) {
		send_bc128("OPEN %s TWS", tws_mac_address);
	}

	if ((cmd_flags & BC128_CMD_FLAG_DISCOVERABLE)) {
		send_bc128("DISCOVERABLE ON");
	}

#ifndef DISABLE_BLE
	process_ble_gatt_read_requests(ble_link_id, config);
#endif

	cmd_flags = BC128_CMD_FLAG_NONE;
}

byte old_send_bc128(char *string)
{
	const int MAX_TRIES = 5;
	int try = 0;
	struct timer timeout;

	if (!bc128_has_booted()) {
		return 1;
	}

	if (rs232_switch_state() != RS232_SWITCH_MODE_BLUETOOTH) {
		rs232_switch(RS232_SWITCH_MODE_BLUETOOTH);
	}

	while (uart_is_busy())
		;

	use_uart(true);
	bc128_reply = REPLY_NONE;

	printf("%s\r", string);
	init_timer(&timeout);
	start_timer(&timeout, TIMEOUT_BC128_CMD);

	while (bc128_reply != REPLY_OK && bc128_reply != REPLY_PENDING && try < MAX_TRIES) {
		if (timer_has_fired(&timeout) || bc128_reply == REPLY_ERROR) {
			bc128_reply = REPLY_NONE;
			printf("%s\r", string);
			reset_timer(&timeout);
			try++;
		}

		parse_cmd_bluetooth(get_uart_message(), &config_data);
	}

	if (bc128_reply != REPLY_OK && bc128_reply != REPLY_PENDING) {
		delay(1);
		printf("TONE V %i N C7 L 8\r", (byte) get_volume_tone(&config_data));
		delay(TIMEOUT_BC128_CMD);
	}

	bc128_reply = REPLY_NONE;
	use_uart(false);
	return 0;
}

byte send_bc128(char *format, ...)
{
	char cmd[SERIAL_BUFFER_SIZE_MAX];
	const int MAX_TRIES = 5;
	int try = 0;
	struct timer timeout;
	va_list argptr;

#if 1
	if (!bc128_has_booted()) {
		return 1;
	}
#endif

	if (rs232_switch_state() != RS232_SWITCH_MODE_BLUETOOTH) {
		rs232_switch(RS232_SWITCH_MODE_BLUETOOTH);
	}

	while (uart_is_busy())
		;

	use_uart(true);
	bc128_reply = REPLY_NONE;

	va_start(argptr, format);
	vsprintf(cmd, format, argptr);
	va_end(argptr);
	printf("%s\r", cmd);

	init_timer(&timeout);
	start_timer(&timeout, TIMEOUT_BC128_CMD);

	while (bc128_reply != REPLY_OK && bc128_reply != REPLY_PENDING && try < MAX_TRIES) {
		if (timer_has_fired(&timeout) || bc128_reply == REPLY_ERROR) {
			bc128_reply = REPLY_NONE;

			printf("%s\r", cmd);

			reset_timer(&timeout);
			try++;
		}

		parse_cmd_bluetooth(get_uart_message(), &config_data);
	}

	if (bc128_reply != REPLY_OK && bc128_reply != REPLY_PENDING) {
		delay(1);
		printf("TONE V %i N C7 L 8\r", (byte) get_volume_tone(&config_data));
		delay(TIMEOUT_BC128_CMD);
	}

	bc128_reply = REPLY_NONE;
	use_uart(false);
	return 0;
}

void setup_dsp(void)
{
	art_debug("DSP Params");

	delay(1);
	send_bc128("MM_CFG 30 0");
	send_bc128("MM_CFG 31 0");
	send_bc128("MM_CFG 32 0");
}

void select_bluetooth_as_source(config_data_t *config)
{
	if (config->current_source != SRC_BLUETOOTH) {
		if (has_bluetooth_player()) {
			set_source(SRC_BLUETOOTH, config);
		} else {
			double_beep(NOTE_SIXTEENTH, TONE_G, TONE_E, config);
		}
	} else {
		if (has_bluetooth_player()) {
			send_bc128("ROUTE %c%i", device_id_player, 0);
			delay(30);
		} else {
			double_beep(NOTE_SIXTEENTH, TONE_G, TONE_E, config);
		}
	}
}

void set_bluetooth_volume(byte volume)
{
	send_bc128("VOLUME %c%i %x", device_id_player, 0, volume);
}
