#include <stdio.h>
#include <string.h>

#include "config.h"
#include "audio.h"
#include "bluetooth.h"
#include "utils.h"
#include "iap.h"
#include "platform_hyde.h"
#include "platform_power.h"
#include "main.h"
#include "timer.h"

static struct timer auto_config_timer;

byte get_volume_tone(config_data_t *config)
{
	const byte THRESHOLD = 0x1D;
	byte volume = THRESHOLD;

	if (config->current_volume < THRESHOLD)
		volume = config->current_volume * 8;

#if 1
	return volume;
#else
	return 0;
#endif
}

void apply_config(config_data_t *config)
{
	set_balance(config->balance);
	set_tone(config->boost, config);

	if (!config->enable_mono) {
		config->enable_mono = audio_switch(AUDIO_SWITCH_MODE_STEREO);
	} else {
		config->enable_mono = audio_switch(AUDIO_SWITCH_MODE_MONO);
	}

	set_power(config->power_state, config);
}

void copy_config(config_data_t *config, config_data_t *copy)
{
	byte i, j;

	copy->check_val = config->check_val;
	copy->power_state = config->power_state;
	copy->current_source = config->current_source;

	for (i = 0; i < RADIO_SOURCE_MAX; i++)
		copy->current_radio_index[i] = config->current_radio_index[i];

	copy->enable_mono = config->enable_mono;
	copy->boost = config->boost;

	copy->current_volume = config->current_volume;
	copy->balance = config->balance;

	for (i = 0; i < NUM_RF_REMOTES; i++)
		copy->rf_remote[i] = config->rf_remote[i];

	for (j = 0; j < RADIO_SOURCE_MAX; j++) {
		for (i = 0; i < NUM_PRESETS; i++) {
			copy->radio_presets[j][i] = config->radio_presets[j][i];
		}
	}

	copy->dab_scanned = config->dab_scanned;
	copy->max_volume = config->max_volume;

	copy->eco_mode = config->eco_mode;

	strncpy(copy->btname, config->btname, BLUETOOTH_NAME_CUSTOM_MAX_SIZE);
}

void auto_save_configuration(config_data_t *config)
{
	if (timer_has_fired(&auto_config_timer)) {
		store_config(config);
	}
}

void configuration_changed(void)
{
	init_timer(&auto_config_timer);
	start_timer(&auto_config_timer, TIME_SAVE_CHANGED_DATA);
}

void load_config(config_data_t *config)
{
#if 1
	read_block(sizeof(config_data_t), (unsigned char *) config);

	if (config->check_val != FLASH_CHECK_VAL_VALID) {
		/* Checked on next bootup to know if the flash data is valid. */
		config->check_val = FLASH_CHECK_VAL_VALID;

		art_debug("Empty CFG");
		do_full_reset(config);
	}
#else
	do_full_reset(config);
#endif
}

void store_config(config_data_t *config)
{
#if 1
	art_debug("Storing to flash");
	erase_sector();
	config->check_val = FLASH_CHECK_VAL_VALID;
	write_block(sizeof(config_data_t), (unsigned char *) config);
#endif
}

void set_boost(config_data_t *config, enum freq_boost boost)
{
	config->boost = boost;
}

bool remote_is_registered(config_data_t *config, word remote_addr)
{
	int i;

#if 0
	/* Check if it is already in the list */
	for (i = 0; i < NUM_RF_REMOTES; i++) {
		if (config->rf_remote[i] == remote_addr) {
			return true;
		}
	}

	return false;
#else
	return true;
#endif
}

void save_remote(config_data_t *config, word remote_addr)
{
	int i;

	/* Check if it is already in the list */
	for (i = 0; i < NUM_RF_REMOTES; i++) {
		if (config->rf_remote[i] == remote_addr) {
			return;
		}
	}

	for (i = 0; i < NUM_RF_REMOTES; i++) {
		if (config->rf_remote[i] == FREE) {
			config->rf_remote[i] = remote_addr;

			/* Immediate store so if the timer is running, it can be stopped */
			store_config(config);
			return;
		}
	}

	/* If we get here it means the memory is full.
	 * So exit the setup mode with a long beep(same as when having timeout)
	 * generate the trigger (get evaluated in the loop)
	 */
	setup_handler(config, SETUP_LINKMODE);
}

void do_full_reset(config_data_t *config)
{
	int i, j;

	mute_amplifier(true);
	config->eco_mode = ECOMODE_ON;
	config->power_state = POWER_ON;

	config->current_source = DEFAULT_SOURCE;
	config->current_volume = DEFAULT_VOLUME;
	config->enable_mono = 0;

	reset_tone(config);
	delay(2);

	config->balance = DEFAULT_BALANCE;
	art_debug("Setup DSP");

	delay(1);
	setup_dsp();
	config->max_volume = MIN_VOLUME_ATTN;
	send_bc128("SET NAME=ArtSound HYDE [%c%c]", 'A' + (read_uid(3) & 0xf), 'Z' - ((read_uid(3) & 0xf0) >> 4));
	strncpy(config->btname, "", BLUETOOTH_NAME_CUSTOM_MAX_SIZE);
	send_bc128("UNPAIR");

	for (i = 0; i < NUM_RF_REMOTES; i++) {
		config->rf_remote[i] = FREE;
	}

	/* Clear presets */
	for (j = 0; j < RADIO_SOURCE_MAX; j++) {
		for (i = 0; i < NUM_PRESETS; i++)
			config->radio_presets[j][i] = RADIO_FREE;
	}

	config->dab_scanned = 0;

	/* DONT CLEAR FIRST PRESET */
	config->radio_presets[RADIO_SOURCE_FM][PRESET_1] = DEFAULT_FM_FREQ;

	init_bc128(config);

	mute_amplifier(false);
	delay(5);
	send_bc128("TONE V 160 N E7 L 4 D 20");
	delay(50);
	mute_amplifier(true);

	apply_config(config);
	store_config(config);
	delay(100);

	art_debug("FULL RESET OK");
	configuration_changed();
}
