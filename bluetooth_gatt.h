#ifndef H_BLUETOOTH_GATT
#define H_BLUETOOTH_GATT

#include <stdbool.h>
#include <stdint.h>
#include <config.h>
#include <bluetooth.h>

#define GATT_UID_SIZE				32
#define GATT_DATA_SIZE_MAX			128
#define GATT_NB_SERVICE_MAX_PER_SERVICE	4
#define GATT_DB_SIZE_MAX			512


enum gatt_permissions {
	GATT_PERM_BOARDCAST			= 0x01,
	GATT_PERM_READ				= 0x02,
	GATT_PERM_WRITE_WITHOUT_RESPONSE	= 0x04,
	GATT_PERM_WRITE			= 0x08,
	GATT_PERM_NOTIFY			= 0x10,
	GATT_PERM_INDICATE			= 0x20,
};

enum gatt_data_type {
	GATT_DATA_TYPE_UID			= 0x00,
	GATT_DATA_TYPE_CHAR			= 0x30,
	GATT_DATA_TYPE_CLIENT_CHAR_CONFIG	= 0x6C,
	GATT_DATA_TYPE_CONSTANT_CHAR_VALUE	= 0xD0,
	GATT_DATA_TYPE_CHAR_VALUE		= 0xDC,
};

enum gatt_handler {
	/* Service tuner */
	GATT_HANDLER_POWER_MANAGEMENT		= 9,
	GATT_HANDLER_RADIO_MODE		= 11,

	/* Service radio */
	GATT_HANDLER_RADIO_STATION_NAME	= 15,
	GATT_HANDLER_RADIO_FREQ		= 17,
	GATT_HANDLER_RADIO_STORE_PRESET	= 19,
	GATT_HANDLER_RADIO_SELECT_PRESET	= 21,

	/* Service DAB */
	GATT_HANDLER_DAB_AUTO_SCAN		= 25,
	GATT_HANDLER_DAB_NB_SERVICE		= 27,
	GATT_HANDLER_DAB_SERVICE_INDEX		= 29,

	/* Service audio */
	GATT_HANDLER_INPUT_SOURCE		= 33,
	GATT_HANDLER_VOLUME			= 35,
};

struct gatt_uuid {
	enum gatt_data_type type;
	uint8_t size;
	char uuid[GATT_UID_SIZE + 1];
};

struct gatt_char_value {
	enum gatt_data_type type;
	bool has_value;
	char value[GATT_DATA_SIZE_MAX + 1];
};

struct gatt_char_config {
	enum gatt_data_type type;
	bool has_config;
};

struct gatt_char {
	struct gatt_uuid uuid;
	enum gatt_permissions perm;
	enum gatt_handle handle;
	struct gatt_char_value value;
	struct gatt_char_config config;
};

struct gatt_service {
	struct gatt_uuid uuid;
	int nb_charac;
	struct gatt_char charac[GATT_NB_SERVICE_MAX_PER_SERVICE];
};

int init_gatt_buffer(uint8_t *buffer);
void print_gatt_buffer(uint8_t *buffer, int buffer_size);
void parse_ble_gatt_read_request(char *request, config_data_t *config);
void parse_ble_gatt_write_request(char *request, config_data_t *config);
void process_ble_gatt_read_requests(enum bc128_cmd_flag cmd_flags, int ble_link_id, config_data_t *config);

#endif
