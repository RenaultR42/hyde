/*----------------------------------------------------------------------------
 * Name:    RFDet.c
 * Purpose: RF Decoding
 * Autthor:	Sagaert Johan
 * Note(s): Decoding Rev 1.0
 *----------------------------------------------------------------------------
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2013 Apex Systems International BVBA. All rights reserved.
 *----------------------------------------------------------------------------*/

#include <stdbool.h>

#include "LPC11xx.h"
#include "types.h"
#include "rfdet.h"
#include "main.h"
#include "utils.h"
#include "gpio.h"
#include "platform_hyde.h"

volatile static unsigned long long samplemask = 0;
volatile static unsigned long long sliceddata = 0;

static int M0_val = 0;
static int M1_val = 0;
static int resynccounter = 0;

rfkey_stat_t rfkey_status;

#define STARTMASK			0x800000000000ull
#define SAMPLEPERIOD 			2500	/* 1200 bds */
#define PULSEMIDDLE			(SAMPLEPERIOD / 2)
#define MR0SAMPLE			(SAMPLEPERIOD / 4)
#define MR1SAMPLE			(3 * SAMPLEPERIOD / 4)

#define RESYNCRELOADVALUE		5

#define RFKEY_RELEASED_DETECT_TIME	30	/* 300 ms */
#define CHECKSUM_POLY 			0x81005

void PIOINT1_IRQHandler(void)
{
	if (LPC_GPIO1->MIS & 0x1) {
		/* Clear irq */
		LPC_GPIO1->IC = 0x1;
		/* If not busy sampling */
		if (LPC_TMR32B0->TCR != 1) {
			resynccounter = RESYNCRELOADVALUE;
			samplemask = STARTMASK;
			sliceddata = 0;

			/* Disable */
			LPC_TMR32B0->TCR = 0;
			LPC_TMR32B0->TC = 0;

			LPC_TMR32B0->IR = 0x3;
			LPC_TMR32B0->TCR = 1;
		} else {
			if (resynccounter == 0) {
				if (M0_val == 0) {
					LPC_TMR32B0->TCR = 0;	/* STOP */
					LPC_TMR32B0->TC = PULSEMIDDLE - 1;	/* SET */
					LPC_TMR32B0->TCR = 1;	/* RUN */
					resynccounter = RESYNCRELOADVALUE;
				}
			}
		}
	}
}

void TIMER32_0_IRQHandler(void)
{
	/* MR0 match just before */
	if (LPC_TMR32B0->IR & 0x1) {
		M0_val = LPC_GPIO1->DATA & 0x1;
		LPC_TMR32B0->IR = 0x1;

		if (resynccounter) {
			resynccounter--;
		}
	} else if (LPC_TMR32B0->IR & 0x2) {
		/* MR1 match just after */
		M1_val = LPC_GPIO1->DATA & 0x1;
		LPC_TMR32B0->IR = 0x2;

		if (M0_val == 0 && M1_val == 1) {	/* LH -> 0 */
			samplemask >>= 1;
		} else if (M0_val == 1 && M1_val == 0)	/* HL -> 1 */
		{
			sliceddata |= samplemask;
			samplemask >>= 1;
		} else {
			/* All bits are moved, and the last 2 samples are 0 (no pulses anymore) */
			if (samplemask == 0 && M0_val == 0 && M1_val == 0) {
				unsigned long crc = (((sliceddata >> 20) & 0xfffff) ^ CHECKSUM_POLY) ^ sliceddata & 0xfffff;
				if (crc == 0) {
					rfkey_status.data_state = RF_KEY_DOWN;
					if (rfkey_status.time_down == 0)
						rfkey_status.time_down = 1;

					rfkey_status.addr = (sliceddata >> 28) & 0xfff;
					rfkey_status.key = (sliceddata >> 20) & 0x1f;
					start_timer(&rfkey_status.key_release_detection, RFKEY_RELEASED_DETECT_TIME);
				}
			}

			/* Hold timer in reset */
			LPC_TMR32B0->TCR = 2;
		}
	}
}

rfkey_stat_t *get_rf_key(void)
{
	return &rfkey_status;
}

/**************************************
* Configure timer T32B0 for RF Pulse sampling
***************************************/
void RF_Init(void)
{
	enable_sys_clock(SYS_CLOCK_CT32B0, true);

	/* Prescaler */
	LPC_TMR32B0->PR = 15;	/* 48000000 / 16= 3Mhz */

	/* RESET */
	LPC_TMR32B0->TCR = 2;

	/* Reset and int on MR0 MR1  reset on MR2 */
	LPC_TMR32B0->MCR = 0x89;

	/* Nothing on match */
	LPC_TMR32B0->EMR = 0;

	/* Timer mode */
	LPC_TMR32B0->CTCR = 0;

	/* Match mode */
	LPC_TMR32B0->PWMC = 0;

	LPC_TMR32B0->MR0 = MR0SAMPLE - 1;
	LPC_TMR32B0->MR1 = MR1SAMPLE - 1;
	LPC_TMR32B0->MR2 = SAMPLEPERIOD - 1;

	NVIC_EnableIRQ(TIMER_32_0_IRQn);

	rfkey_status.key = KEY_UNKWNON;
	rfkey_status.time_down = 0;
	rfkey_status.data_state = RF_KEY_HANDLED;
	init_timer(&rfkey_status.key_release_detection);

	/* SetupRF_IntPin */
	LPC_IOCON->R_PIO1_0 = 0xea;	/* PIN function when IO PIN 1_0 pull down in hysteresis */
	LPC_GPIO1->IEV = 1;	/* IRQ on Rising Edge */
	LPC_GPIO1->IE = 1;
	LPC_GPIO1->IC = 0x1;

	NVIC_EnableIRQ(EINT1_IRQn);
}

int keyboard_digit_choice(enum rf_keys key, unsigned int size)
{
	int digit = 0;

	art_debug("KEY: %d", key);

	switch (key) {
	case KEY_BT:
		digit++;
	case KEY_DAB:
		digit++;
	case KEY_FM:
		digit++;
	case KEY_6:
		digit++;
	case KEY_5:
		digit++;
	case KEY_4:
		digit++;
	case KEY_3:
		digit++;
	case KEY_2:
		digit++;
	case KEY_1:
		digit++;
	case KEY_RET:
	case KEY_AUX:
	default:
		break;
	}

	if (digit < 0 || digit > size)
		digit = 0;

	return digit;
}
