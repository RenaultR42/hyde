#include "platform_power.h"
#include "platform_hyde.h"
#include "bluetooth.h"
#include "i2c.h"
#include "config.h"
#include "utils.h"
#include "audio_routing.h"
#include "audio.h"
#include "tuner.h"

void check_external_trigger_power(config_data_t *config)
{
	static enum trig_power old_power_status = TRIG_NONE;
	enum trig_power power_status = TRIG_NONE;

	if (trigger_status_on()) {
		power_status = TRIG_POWERON;
	} else if (trigger_status_off()) {
		power_status = TRIG_POWEROFF;
	}

	if (old_power_status == power_status) {
		return;
	}

	old_power_status = power_status;

	switch (power_status) {
	case TRIG_POWERON:
		if (config->power_state != POWER_ON) {
			art_debug("Trig on");

			set_power(POWER_ON, config);
			config->power_state = POWER_ON;
			configuration_changed();
		}
		break;

	case TRIG_POWEROFF:
		art_debug("Trig off");
	case ADC_POWEROFF:
		if (power_status == ADC_POWEROFF)
			art_debug("ADC off");

		if (config->power_state != POWER_OFF) {
			set_power(POWER_OFF, config);
			config->power_state = POWER_OFF;
			configuration_changed();
		}
		break;
	default:
		break;
	}
}

void set_power_mode(enum power_state mode, enum eco_mode_status eco_mode, enum audio_source source)
{
	mute_amplifier(true);

	if (mode == POWER_OFF) {
		send_bc128("CONNECTABLE OFF");
		delay(1);

		art_debug("Power Off");

		if (source == SRC_BLUETOOTH) {
			control_bt(STOP);
		}

		delay(1);

		if (eco_mode == ECOMODE_ON) {
			set_tuner_power(false);
			delay(MODULES_RESET_DURATION);
			enable_ldo(false);
			delay(MODULES_RESET_DURATION);
		}

		set_bluetooth_power(mode);
	} else {
		/* NVIC_DisableIRQ(I2C_IRQn); */
		if (eco_mode == ECOMODE_ON) {
			enable_ldo(true);
			delay(MODULES_RESET_DURATION);
			set_tuner_power(true);
			delay(MODULES_RESET_DURATION);
			init_radio_source(source);
			delay(MODULES_RESET_DURATION);
		}

		set_bluetooth_power(mode);
	}
}

void set_power(enum power_state mode, config_data_t *config)
{
	set_power_mode(mode, config->eco_mode, config->current_source);

	if (mode == POWER_ON) {
		art_debug("Power ON. Source %d", config->current_source);

		set_source(config->current_source, config);
		mute_amplifier(false);
		send_bc128("CONNECTABLE ON");
		delay(1);
	}

	store_config(config);
}
