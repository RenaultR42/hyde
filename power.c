#include "power.h"
#include "LPC11xx.h"

void power_block_enable(enum power_block block, bool enable)
{
	if (enable) {
		LPC_SYSCON->PDRUNCFG &= ~(block);
	} else {
		LPC_SYSCON->PDRUNCFG |= block;
	}
}
