#include <string.h>
#include <stdio.h>

#include "LPC11xx.h"
#include "constants.h"
#include "types.h"
#include "i2c.h"
#include "platform_hyde.h"
#include "platform_gpio_hyde.h"
#include "gpio.h"
#include "timer.h"

volatile t_iic i2c;

void I2C_Init(void)
{
	LPC_SYSCON->PRESETCTRL |= (0x1 << 1);	/* Remove reset state */
	enable_sys_clock(SYS_CLOCK_I2C, true);

	/* Set pins for standard mode I2c */
	set_port_pinmux(PORT_IO_I2C_SDA, IO_PORT_FUNC(PORT_IO_I2C_SDA_FUNC));
	set_port_pinmux(PORT_IO_I2C_SCL, IO_PORT_FUNC(PORT_IO_I2C_SCL_FUNC));

	/* Clear flags */
	LPC_I2C->CONCLR = 0x0000006C;

	/* Set SCL rate: I2CPCLK/(SCLH+SCLL) */
	/* SCL = 48Mhz / (240+240) = 100Khz */
	LPC_I2C->SCLL = 240;
	LPC_I2C->SCLH = 240;

#ifdef IICSLAVE
	LPC_I2C->ADR0 = address;
	LPC_I2C->ADR1 = address;
	LPC_I2C->ADR2 = address;
	LPC_I2C->ADR3 = address;
	LPC_I2C->I2C0MASK0 = 0;
	LPC_I2C->I2C0MASK1 = 0;
	LPC_I2C->I2C0MASK2 = 0;
	LPC_I2C->I2C0MASK3 = 0;
#endif

	NVIC_EnableIRQ(I2C_IRQn);

	LPC_I2C->CONSET = 0x00000040;

	i2c.Busy = 0;
	i2c.Error = E_NONE;
}

byte GetI2CError(void)
{
	return i2c.Error;
}

void I2C_IRQHandler(void)
{
	byte stat = LPC_I2C->STAT;

	switch (stat) {
	case I2C_STATUS_ERROR:
		LPC_I2C->CONSET = I2C_STO | I2C_AA;
		LPC_I2C->CONCLR = I2C_SI;
		i2c.Busy = 0;
		i2c.Error = E_BUS_ERROR;
		break;

	/* Master Transmit */
	case I2C_STATUS_START_CONDITION_TX:
	case I2C_STATUS_REPEATED_START_CONDITION_TX:
		/* Transmit address */
		if (i2c.NumTxByte == 0)	/* We should receive now */
			LPC_I2C->DAT = i2c.DeviceAddress | 1;
		else
			LPC_I2C->DAT = i2c.DeviceAddress;
		LPC_I2C->CONCLR = I2C_SI | I2C_STA;
		break;

	case I2C_STATUS_TX_COMPLETED_ACK_RECEIVED:
		/* Device has acknowledged that the master is talking to it so... send first 'data' byte */
		if (i2c.NumTxByte == 0) {
			LPC_I2C->CONSET = I2C_STO;	/* Set Stop flag */
			i2c.Busy = 0;
			i2c.Error = E_NONE;
		} else {
			LPC_I2C->DAT = *i2c.TxDataPtr;
			i2c.TxDataPtr++;
			i2c.NumTxByte--;	/* Minus: 1st data */
		}
		LPC_I2C->CONCLR = I2C_SI;
		break;
	case I2C_STATUS_TX_COMPLETED_ACK_NOT_RECEIVED:
		/* Slave Address + Write has been transmitted, NOT ACK has been received. A STOP condition will be transmitted. */
		LPC_I2C->CONSET = I2C_STO | I2C_AA;
		LPC_I2C->CONCLR = I2C_SI;

		i2c.Busy = 0;
		i2c.Error = E_NOACK;
		break;
	case I2C_STATUS_ACK_RECEIVED:
		/* First data byte was received so send next.if more data has been transmitted, ACK has been received.
		 * If the transmitted data was the last data byte then transmit a STOP condition,
		 * otherwise transmit the next data byte
		 */
		if (i2c.NumTxByte > 0) {
			LPC_I2C->DAT = *i2c.TxDataPtr;	/* Send 'data byte' 2 through */
			i2c.TxDataPtr++;
			i2c.NumTxByte--;
			LPC_I2C->CONCLR = I2C_SI;
		} else { /* Everything was sent */
			if (i2c.NumRxByte) {	/* Should we receive something ? then send restart */
				i2c.CntReceived = 0;
				LPC_I2C->CONSET = I2C_STA;	/* Set Repeated-start flag */
				LPC_I2C->CONCLR = I2C_SI;
			} else {	/* Send stop command since there are no more bytes to send */
				LPC_I2C->CONSET = I2C_STO;
				LPC_I2C->CONCLR = I2C_SI;
				i2c.Busy = 0;
				i2c.Error = E_NONE;
			}
		}

		break;
	case I2C_STATUS_ACK_NOT_RECEIVED:
		LPC_I2C->CONSET = I2C_STO | I2C_AA;
		LPC_I2C->CONCLR = I2C_SI;
		i2c.Busy = 0;
		i2c.Error = E_NOACK;
		break;
	case I2C_STATUS_ARBITRATION_LOST:
		/* TODO Create auto-retry for ${x} number of times. */
		LPC_I2C->CONSET = I2C_STA | I2C_AA;
		LPC_I2C->CONCLR = I2C_SI;
		i2c.Busy = 0;
		i2c.Error = E_BUS_LOST;
		break;

	/* Master receives */
	case I2C_STATUS_READ_REQ_ACK_RECEIVED:
		if (i2c.NumRxByte > 1) {
			/* Will go to State 0x50 */
			/* Assert ACK after data is received */
			LPC_I2C->CONSET = I2C_AA;
		} else {
			/* Will go to State 0x58 */
			/* Assert NACK after data is received */
			LPC_I2C->CONCLR = I2C_AA;
		}
		LPC_I2C->CONCLR = I2C_SI;
		break;
	case I2C_STATUS_READ_REQ_ACK_NOT_RECEIVED:
		/* Device may be too busy to respond or is not there */
		LPC_I2C->CONSET = I2C_STO | I2C_AA;
		LPC_I2C->CONCLR = I2C_SI;
		i2c.Busy = 0;
		i2c.Error = E_NOACK;
		break;
	case I2C_STATUS_ACK_SENT:
		*i2c.RxDataPtr = LPC_I2C->DAT;
		i2c.RxDataPtr++;
		i2c.CntReceived++;
		i2c.NumRxByte--;

		/* All was received */
		if (i2c.NumRxByte == 0) {
			LPC_I2C->CONCLR = I2C_SI | I2C_AA;
		} else {
			LPC_I2C->CONSET = I2C_AA;
			LPC_I2C->CONCLR = I2C_SI;
		}
		break;
	case I2C_STATUS_ACK_NOT_SENT:
		/* End of transaction data has been received, NOT ACK has been returned. 
		 * Data will be read from DAT. A STOP condition will be transmitted. */
		*i2c.RxDataPtr = LPC_I2C->DAT;
		i2c.CntReceived++;
		i2c.NumRxByte--;
		LPC_I2C->CONSET = I2C_STO | I2C_AA;
		LPC_I2C->CONCLR = I2C_SI;
		i2c.Busy = 0;
		i2c.Error = E_NONE;
		break;

#ifdef IICSLAVE
		/* Slave Receive */
	case I2C_STATUS_SLAVE_RX_COMPLETED_ACK_SENT:
		/* FIXME Write code to handle lost Arbitration
		 * Arbitration lost in SLA+R || Own SLA+W received - ACK RECEIVED
		 */
	case I2C_STATUS_SLAVE_ARBITRATION_LOST:
		if (i2c.I2cRxBuff.dptr) {
			i2c_ack();
			i2c.I2cStatus = I2C_BUSY;
		} else {		/* Ring buffer not initialized */
			i2c_ack();
			i2c.I2cStatus = I2C_NO_RBUF;
		}
		break;
	case I2C_STATUS_SLAVE_GENERAL_CALL_RECEIVED_ACK_RECEIVED:
		i2c.I2cStatus = I2C_BUSY;
		/* Get ready to Rx data(case-90). Fall through to case 78 */
		/* FIXME Write code to handle lost Arbitration */
	case I2C_STATUS_SLAVE_ARBITRATION_LOST_ACK_RECEIVED:
		i2c.I2cNumRxByte = 0x00;
		if (i2c.I2cRxBuff.dptr) {
			i2c_ack();
		} else {		/* Ring buffer not initialized */
			i2c_ack();
		}
		break;
	case I2C_STATUS_SLAVE_DATA_RECEIVED:
		*i2c.I2cRxBuff.tail = LPC_I2C->DAT;	/* Load data buffer. */
		i2c_ack();
		if ((i2c.I2cRxBuff.tail - i2c.I2cRxBuff.dptr) & I2C_RX_RING_BUFFER_SIZE)
			i2c.I2cRxBuff.tail = i2c.I2cRxBuff.dptr;
		else
			++(i2c.I2cRxBuff.tail);
		break;
	case I2C_STATUS_SLAVE_DATA_RECEIVED_NACK_SENT:
		i2c_ack();	/* Re-enable reception  */
		break;
	case I2C_STATUS_SLAVE_DATA_RECEIVED_ACK_RECEIVED:
		/* First time entering */
		if (i2c.I2cNumRxByte == 0x00) {
			/* Hardware master (e.g keyboard) */
			if (LPC_I2C->DAT & 0x01) {
				*i2c.I2cRxBuff.tail = (LPC_I2C->DAT >> 1);	/* Get address */
				if ((i2c.I2cRxBuff.tail - i2c.I2cRxBuff.dptr) & I2C_RX_RING_BUFFER_SIZE)
					i2c.I2cRxBuff.tail = i2c.I2cRxBuff.dptr;
				else
					++(i2c.I2cRxBuff.tail);
				i2c_ack();
				i2c.I2cNumRxByte++;
			} else if (LPC_I2C->DAT == 0x06) {	/* Reset then re-read i2c-HW address. */
				/* TODO Cause a reset then read I2C hardware address pins. */
			}
		} else {	/* Two or more times through */
			*i2c.I2cRxBuff.tail = LPC_I2C->DAT;	/* Load data buffer. */
			if ((i2c.I2cRxBuff.tail - i2c.I2cRxBuff.dptr) & I2C_RX_RING_BUFFER_SIZE)
				i2c.I2cRxBuff.tail = i2c.I2cRxBuff.dptr;
			else
				++i2c.I2cRxBuff;
			i2c_ack();
			i2c.I2cNumRxByte++;
		}
		break;
	case I2C_STATUS_SLAVE_DATA_RECEIVED_NACK_RECEIVED:
		i2c_ack();
		/* Received General call but didn't ack because ring buffer wasn't initialized  */
		i2c.I2cStatus = I2C_NO_RBUF;
		break;
	case I2C_STATUS_SLAVE_STOP_OR_RESTART:
		i2c_ack();	/* (re)enable slave reception */
		i2c.I2cStatus = I2C_OK;
		break;
	/* Slave Transmit */
	/*
	 * TODO: Add code to transmit packets. Could use callback/function pointers
	 */
	case I2C_STATUS_SLAVE_READ_REQ_RECEIVED:
	case I2C_STATUS_SLAVE_SENT_NACK:
		LPC_I2C->DAT = 0xFF;
		i2c_nack();	/* Ignore master till it gets the hint. 0xFF are read. */
		break;
	case I2C_STATUS_SLAVE_REENABLE_SLAVE_RX:
	case I2C_STATUS_SLAVE_SENT_ACK:
		i2c_ack();
		break;
#endif
	default:
		LPC_I2C->CONSET = I2C_STO;
		LPC_I2C->CONCLR = I2C_SI | I2C_STA;
		i2c.Busy = 0;
		i2c.Error = E_UNKNOWN;
	}

	LPC_I2C->CONCLR = I2C_SI;	/* Clear the I2C interrupt flag. */
}

byte IICSend(byte deviceaddr, byte *txdata, int bytecnt)
{
	/* Look until i2c hardware is.ready */
	WaitIICXferDone();
#ifdef DEBUGIIC
	if (i2c.Error)
		art_debug("*IICSend Previous I2C Error from device %02X\r", i2c.DeviceAddress);
#endif

	i2c.Error = 0;

	if (bytecnt > IIC_TXBUFSIZE)
		return E_BUFFSIZE;
	if (bytecnt == 0)
		return E_NONE;

	i2c.DeviceAddress = deviceaddr;
	i2c.NumTxByte = bytecnt;
	i2c.NumRxByte = 0;
	i2c.RxDataPtr = NULL;
	i2c.CntReceived = 0;

	memcpy((void *) i2c.SendBuffer, txdata, bytecnt);
	i2c.TxDataPtr = i2c.SendBuffer;

	/* Start the transfer */
	i2c.Busy = 1;
	i2c.Error = E_NONE;
	LPC_I2C->CONSET = I2C_STA;
	return E_NONE;
}


byte IICReceive(byte deviceaddr, byte rxbytecnt)
{
	if (rxbytecnt > IIC_RXBUFSIZE)
		return E_BUFFSIZE;
	if (rxbytecnt == 0)
		return E_NONE;

	/* Look until i2c hardware is.ready */
	WaitIICXferDone();
#ifdef DEBUCIIC
	if (i2c.Error)
		art_debug("*IICReceive Error from device %02X\r", i2c.DeviceAddress);
#endif

	i2c.Error = 0;
	i2c.DeviceAddress = deviceaddr;
	i2c.TxDataPtr = NULL;
	i2c.NumTxByte = 0;
	i2c.RxDataPtr = i2c.ReceiveBuffer;
	i2c.NumRxByte = rxbytecnt;
	i2c.CntReceived = 0;

	/* Start the transfer */
	i2c.Busy = 1;
	i2c.Error = E_NONE;
	LPC_I2C->CONSET = I2C_STA;

	return WaitIICXferDone();
}

byte IICTransfer(byte deviceaddr, byte txbytecnt, byte *txdata, byte rxbytecnt)
{
	if (rxbytecnt > IIC_RXBUFSIZE)
		return E_BUFFSIZE;

	if (txbytecnt > IIC_TXBUFSIZE)
		return E_BUFFSIZE;

	if (txdata == NULL)
		return E_NOBUFF;

	/* Look until i2c hardware is.ready */
	WaitIICXferDone();
#ifdef DEBUCIIC
	if (i2c.Error)
		art_debug("*IICReceive Error from device %02X\r", i2c.DeviceAddress);
#endif
	i2c.Error = 0;

	memcpy((void *) i2c.SendBuffer, txdata, txbytecnt);
	i2c.DeviceAddress = deviceaddr;
	i2c.TxDataPtr = i2c.SendBuffer;
	i2c.NumTxByte = txbytecnt;
	i2c.RxDataPtr = i2c.ReceiveBuffer;
	i2c.NumRxByte = rxbytecnt;
	i2c.CntReceived = 0;

	/* Start the transfer */
	i2c.Busy = 1;
	i2c.Error = E_NONE;
	LPC_I2C->CONSET = I2C_STA;

	return WaitIICXferDone();
}

byte WaitIICXferDone(void)
{
	while (i2c.Busy)
		;

	return i2c.Error;
}
