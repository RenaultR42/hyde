#ifndef H_IAP
#define H_IAP

#include <stdbool.h>

bool read_block(int readcnt, unsigned char *buffer);
bool write_block(int writecnt, unsigned char *buffer);
bool erase_sector(void);
unsigned int read_uid(unsigned char index);

#endif
