#ifndef H_POWER
#define H_POWER

#include <stdbool.h>

enum power_block {
	POWER_BLOCK_IRCOUT_PD = 0x1,
	POWER_BLOCK_IRC_PD = 0x2,
	POWER_BLOCK_FLASH_PD = 0x4,
	POWER_BLOCK_BOD_PD = 0x8,
	POWER_BLOCK_ADC_PD = 0x10,
	POWER_BLOCK_SYSOSC_PD = 0x20,
	POWER_BLOCK_WDTOSC_PD = 0x40,
	POWER_BLOCK_SYSPLL_PD = 0x80,
};

void power_block_enable(enum power_block block, bool enable);

#endif
