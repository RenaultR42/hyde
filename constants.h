#ifndef H_CONSTANTS
#define H_CONSTANTS

/* RF */
#define NUM_RF_REMOTES		10
#define NUM_PRESETS		6

/* SERIAL */
#define SERIAL_RX_BUFFERSIZE	128
#define SERIAL_BUFFER_SIZE_MAX	128

/* GENERAL */
#define ALT_FUNC_NUM_1_4_TIME	500	/* 5 sec */
#define TIME_PAIRING_ACTIVE	150	/* 1,5 sec */
#define TIME_TONE_TO_DEFAULT	200	/* 2 sec */
#define STORE_PRESET_TIME	200	/* 2 sec */
#define TIME_DAB_FULL_SCAN	200	/* 2 sec */
#define TIME_LONGPRESS		200	/* 2 sec */
#define TIME_SHORTPRESS	60
#define TIME_SCANNING_ACTIVE	100	/* 1 sec */
#define SETUP_MODE_TIME	500	/* 5 sec */
#define ALL_POWER_OFF_TIME	500	/* 5 sec */

/* To reduce the impact of long press button */
#define TONE_RESPONSE_DIVIDER		4
#define VOLUME_RESPONSE_DIVIDER	6
#define BALANCE_RESPONSE_DIVIDER	10

#define TIMEOUT_PAIRING_MODE	6000	/* 1 min */
#define TIMEOUT_SETUPMODE	6000	/* 1 min */
#define TIMEOUT_KEYCODE	500	/* 5 sec */
#define TIMEOUT_SETTINGS_MENU	500
#define TIMEOUT_BC128_CMD	5	/* 20 ms */

#define TIMEOUT_BC128_POWER		10
#define TIME_SAVE_CONFIG		100

enum volume_status {
	VOL_NOT_MINMAX		= 0,
	VOL_LOWER_LIMIT	= 1,
	VOL_UPPER_LIMIT	= 2,
};

enum volume_limit_status {
	VOL_LIMIT_OFF		= 0,
	VOL_LIMIT_ENTER	= 1,
	VOL_LIMIT_ACTIVE	= 2,
};

enum bt_name_status {
	BTNAME_OFF	= 0,
	BTNAME_ENTER	= 1,
	BTNAME_ACTIVE	= 2,
};

#define TIME_SAVE_CHANGED_DATA	800	/* 8 sec */

enum power_state {
	POWER_OFF = 0,
	POWER_ON = 1,
};

enum trig_power {
	TRIG_NONE	= 0,
	TRIG_POWERON	= 1,
	TRIG_POWEROFF	= 2,
	ADC_POWEROFF	= 4,
};

enum setup_status {
	SETUP_NONE		= 0,
	SETUP_FULLRESET	= 1,
	SETUP_LINKMODE		= 2,
};

enum eco_mode_status {
	ECOMODE_ON	= 0,
	ECOMODE_OFF	= 1,
	ECOMODE_SETUP	= 2,
};

enum plus_minus_button_mode {
	VOLUME_ACTIVE		= 0,
	BALANCE_ACTIVE		= 1,
};

/* After this time the function of plus/min defaults to volume functionality. */
#define BALANCE_ACTIVE_TIME	0x20	/* 5 sec */

enum direction_button {
	DIR_UP		= 1,
	DIR_DOWN	= 2,
};

enum balance_button {
	BAL_LEFT	= 1,
	BAL_RIGHT	= 2,
};

#define MIN_VOLUME_ATTN		0x1f	/* 0x63//=0-99   //0x1f//=0-31 */
#define MAX_VOLUME_ATTN		0
#define MUTED_ATTN			0

#define DEFAULT_VOLUME			0x05
#define DEFAULT_MAX_VOLUME		0x1f

#define DEFAULT_BALANCE		100
#define DEFAULT_SOURCE			SRC_FMTUNER
#define DEFAULT_FREQ_KHZ		88500

#define INTERVAL_AUTOSCAN_BEEP		100	/* 500ms */

#define DEFAULT_FM_FREQ		((DEFAULT_FREQ_KHZ - 87500) / 50)	/* Default 88.5 Mhz Value = (frequency 87.5Mhz) / 50 KHz */

#define MODULES_RESET_DURATION		10	/* 100 ms */
#define BLINKING_INTERVAL		25	/* 250 ms */
#define LED_TIME_VALID_RF		2	/* 20 ms */
#define RADIO_POLL_INTERVAL		100	/* 1 s */
#define PAIRING_BEEP_INTERVAL		100	/* 1 s */
#define PAIRING_OK_BEEP_COUNT		3

#define BUFFER_KEYCODE_REMOTE_ADDR_SIZE	3
#define KEYBOARD_REMOTE_ADDR_SIZE		6
#define BUFFER_KEYCODE_BT_NAME_SIZE		3
#define KEYBOARD_BT_NAME_SIZE			9

#endif
