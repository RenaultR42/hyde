#ifndef H_GPIO
#define H_GPIO

#include <stdint.h>
#include <stdbool.h>

#include "LPC11xx.h"

typedef LPC_GPIO_TypeDef GPIO;
typedef volatile uint32_t IO_PORT;

enum gpio_direction {
	GPIO_INPUT,
	GPIO_OUTPUT,
};

enum gpio_base_addr {
	GPIO_BASE_0 = LPC_GPIO0_BASE,
	GPIO_BASE_1 = LPC_GPIO1_BASE,
	GPIO_BASE_2 = LPC_GPIO2_BASE,
	GPIO_BASE_3 = LPC_GPIO3_BASE,
};

#define IO_PORT_PSEUDO_OPEN_DRAIN_MODE	(1 << 10)

#define IO_PORT_DIGITAL		(1 << 7)
#define IO_PORT_ANALOG			(0 << 7)

#define IO_PORT_HYSTERISIS		(1 << 5)
#define IO_PORT_NO_HYSTERISIS		(0 << 5)

#define IO_PORT_MODE_INACTIVE		(0 << 3)
#define IO_PORT_MODE_PULL_DOWN		(1 << 3)
#define IO_PORT_MODE_PULL_UP		(2 << 3)
#define IO_PORT_MODE_REPEATER		(3 << 3)

#define IO_PORT_FUNC(x)		(x & 0x7)

#define IO_PORT_0_1_PIO0_1	0
#define IO_PORT_0_2_PIO0_2	0
#define IO_PORT_0_3_PIO0_3	0
#define IO_PORT_0_4_I2C_SCL	1
#define IO_PORT_0_5_I2C_SDA	1
#define IO_PORT_0_6_PIO0_6	0
#define IO_PORT_0_7_UART_CTS	1
#define IO_PORT_0_8_PIO0_8	0
#define IO_PORT_0_9_PIO0_9	0
#define IO_PORT_0_10_PIO0_10	1
#define IO_PORT_0_11_ADC	2
#define IO_PORT_1_1_PIO1_1	1
#define IO_PORT_1_2_PIO1_2	1
#define IO_PORT_1_3_PIO1_3	1
#define IO_PORT_1_4_PIO1_4	0
#define IO_PORT_1_5_UART_RTS	1
#define IO_PORT_1_6_UART_RX	1
#define IO_PORT_1_7_UART_TX	1
#define IO_PORT_1_9_PIO1_9	0
#define IO_PORT_1_11_PIO1_11	0
#define IO_PORT_2_0_PIO2_0	0
#define IO_PORT_3_2_PIO3_2	0
#define IO_PORT_3_4_PIO3_4	0

bool get_gpio(GPIO *gpio, int num);
void set_gpio(GPIO *gpio, int num, bool value);
void toggle_gpio(GPIO *gpio, int num);

void set_gpio_direction(GPIO *gpio, int num, enum gpio_direction direction);
void gpio_clear_irq(GPIO *gpio, int num);

void set_port_pinmux(IO_PORT *port, uint32_t mode);

#endif
