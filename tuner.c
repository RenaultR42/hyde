/*----------------------------------------------------------------------------
 * Name:    Q8Tuner.c
 * Purpose: Controls Q8+ module
 * Author : Sagaert Johan
 * Note(s):
 *----------------------------------------------------------------------------
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2013 Apex Systems International BVBA. All rights reserved.
 *----------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>

#include "LPC11xx.h"		/* LPC11xx definitions        */
#include "tuner.h"
#include "types.h"
#include "i2c.h"
#include "constants.h"
#include "main.h"
#include "gpio.h"
#include "utils.h"
#include "platform_hyde.h"

extern volatile t_iic i2c;

static byte key_press_hold = 0;
static bool has_booted = false;

static enum tuner_mode radio_mode = UNKNOWN_MODE;
static int timecounter = 0;

bool tuner_has_booted(void)
{
	return has_booted;
}

void InitRadio(enum tuner_mode mode)
{
	const int MAX_TRIES = 5;
	int i;
	enum tuner_status status;

	has_booted = false;

	/* Quantek
	 * Q8 DAB module has two power on modes, power on to DAB or power to FM.
	 *
	 * A. The default radio status sequence after power on is: 
	 *	(1) hardware initialization completed -> (2) standby -> (3) DAB auto scan or DAB scan list (power on to DAB mode).
	 * B. If the host sends FM band press command to Q8 DAB module before the module into standby status, the module will change to FM manual.
	 * The radio status sequence is changed as below:
	 * (1)hardware initialization completed -> (2)standby -> (3)FM manual.
	 */

	timecounter = 0;

	for (i = 0; i < MAX_TRIES; i++) {
		delay(15);
		art_debug("Waiting for Vtun %d", i);
	}

	art_debug("Initializing Radio");
	delay(10);

	i = 0;

	do {
		timecounter += 20;
		delay(2);
		status = GetRadioStatus();
		i++;
	} while (status != STATUS_HW_INITIALIZATION_COMPLETED
		&& status != STATUS_FM_MANUAL
		&& status != STATUS_DAB_MANUAL_TUNE); // && i < MAX_TRIES);

	has_booted = true;

	art_debug("Tuner Power ON");
	timecounter += 20;
	delay(2);

	art_debug("Requested mode %d", mode);

	if (mode == STANDBY) {
		SetTunerMode(FM_MODE);
		SetTunerMode(STANDBY);
	} else {
		SetTunerMode(mode);
	}

	i = 0;

	while (i < MAX_TRIES) { /* Don't try more then 5 times */
		/* Execute a paired command, get FM scan threshold to ensure the setting is correct.  */
		SetFMScanThreshold(FMAUTOSCAN_LEVEL);
		if (GetFMScanThreshold() == FMAUTOSCAN_LEVEL)
			break;
		delay(5);
		i++;

		art_debug("Attempt %d", i);
	}

	key_press_hold = 0;
	art_debug("Radio init done: status %d", GetRadioStatus());
}

void ResetTunerModule(void)
{
	byte iictxdata[4];

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = RESET_PRESS;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);		/* Quantek, it shoud be 10 ms. */
}

void SetMenu(void)
{
	byte iictxdata[4];

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = MENU_PRESS;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);		/* Quantek, it shoud be 10 ms. */
}

enum tuner_mode get_radio_mode(void)
{
	return radio_mode;
}

enum tuner_status GetRadioStatus(void)
{
	byte iictxdata[4];
	int err = 0;

	iictxdata[0] = 0;
	iictxdata[1] = READ_RADIO_STATUS;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 2);
	err = IICReceive(Q8TUNER_IIC_HW_ADDR, 1);

	if (err) {
		art_debug("ERROR %d", err);
		return STATUS_ERROR;
	}

	art_debug("STATUS INIT %d", (byte) i2c.ReceiveBuffer[0]);
	return i2c.ReceiveBuffer[0];
}

/* This command will toggle the module between standby and radio on mode (DAB or FM).
 * The module still comsumes power in standby mode.
 * Cutting off the power of the module is the best way for power saving.
 * If the host just wants to toggle between standby and radio on mode, the radio status check should be ignored, and send standby command once.
 */
void SetTunerStandby(void)
{
	byte iictxdata[4];

	iictxdata[0]= 0; 
	iictxdata[1]= SEND_KEY_ID;
	iictxdata[2]= STANDBY_PRESS;

	if (GetRadioStatus() != STATUS_STANDBY) {
		IICSend(Q8TUNER_IIC_HW_ADDR,iictxdata,3);
		delay(30); /* Quantek, it shoud be 100 ~ 500 ms. */
	}
}

void set_tuner_power(bool enable)
{
	enable_tuner(enable);

	if (!enable)
		has_booted = false;
}

/*************************
 Set the tuner in FM/DAB/STANDBY Mode
*************************/
void SetTunerMode(enum tuner_mode mode)
{
	byte iictxdata[4];
	enum tuner_status status;
	int maxtries = 200;

	if (!tuner_has_booted()) {
		art_debug("start init radio, mode %d", mode);
		InitRadio(mode);
		return;
	}

	if (mode != radio_mode) {
		radio_mode = mode;

		switch (mode) {
		case DAB_MODE:
			do {
				status = GetRadioStatus();
				art_debug("Selecting DAB Mode stat %d", status);

				iictxdata[0] = 0;
				iictxdata[1] = SEND_KEY_ID;
				iictxdata[2] = DAB_BAND_PRESS;
				IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);
				delay(10);	/* Not sure if this is needed or it shoud be 100 ~ 500 ms. */
			} while (status != STATUS_DAB_SERVICE_LIST 
				 && status != STATUS_ALARM_TO_DAB_BUT_PLAY_FAIL
				 && status != STATUS_DAB_AUTO_SCAN && maxtries-- > 0);

			art_debug("Tuner Mode set %d", mode);
			break;
		case FM_MODE:
			do {
				status = GetRadioStatus();
				art_debug("Selecting FM Mode. Status %d", status);

				iictxdata[0] = 0;
				iictxdata[1] = SEND_KEY_ID;
				iictxdata[2] = FM_BAND_PRESS;
				IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);
				delay(10);	/* MANDATORY ,can miss sometimes mode change. It shoud be 100 ~ 500 ms. */
				timecounter += 400;
			} while (status != STATUS_FM_MANUAL && status != STATUS_FM_SEEKING && maxtries-- > 0);

			art_debug("Tuner Mode set to %d", mode);
			break;
		case STANDBY:
			status = GetRadioStatus();
			delay(50);
			iictxdata[0] = 0;
			iictxdata[1] = SEND_KEY_ID;
			iictxdata[2] = STANDBY_PRESS;
			IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);
			delay(50);	/* It shoud be 100 ~ 500 ms. orig 100, 500 works */
			status = GetRadioStatus();
			break;
		default:
			break;
		}
	}
}

/***************************
Get the firmware revision
****************************/
word GetFirmwareVersion(void)
{
	byte iictxdata[4];

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = KEY_GET_FIRMWARE_VERSION;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(25);

	iictxdata[0] = 0;
	iictxdata[1] = GET_FIRMWARE_VERSION;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 2);

	delay(25);

	IICReceive(Q8TUNER_IIC_HW_ADDR, 2);

	return (i2c.ReceiveBuffer[0] << 8) | i2c.ReceiveBuffer[1];
}

/***********************************
Set the FM scanning sensitivity
***********************************/
void SetFMScanThreshold(byte scanthreshold)
{
	byte iictxdata[4];

	iictxdata[0] = 0;
	iictxdata[1] = SET_GET_FM_DETECTION_THRESHOLD;
	iictxdata[2] = scanthreshold;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);		/* Quantek, it shoud be 10 ms. */

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = SET_FM_DET_THRESHOLD;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);		/* Quantek, it shoud be 10 ms. */
}

byte GetFMScanThreshold(void)
{
	byte iictxdata[4];
	byte scanthreshold;

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = GET_FM_DET_THRESHOLD;

	art_debug("Get FM scan treshold");

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);		/* Quantek, it shoud be 10 ms. */

	iictxdata[0] = 0;
	iictxdata[1] = SET_GET_FM_DETECTION_THRESHOLD;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 2);

	delay(2);		/* Quantek, it shoud be 10 ms. */

	IICReceive(Q8TUNER_IIC_HW_ADDR, 1);
	scanthreshold = i2c.ReceiveBuffer[0];
	return scanthreshold;
}

/**************************
Tune to previous or next DAB station in the index list
***************************/
void DABTune(byte direction)
{
	byte iictxdata[4];
	byte button = UP_PRESS;

	if (direction < 0) {
		button = DOWN_PRESS;
	}

	art_debug("DAB tune direction: %D\n\r", button);

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = button;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);
	delay(2);
}

/******************************************
* Returns the current channel for FM
* fmchannel= (Freq[Khz] - 87500 ) / 500
*******************************************/
word GetFMChannel(void)
{
	word fmchannel;	/* Offset from 87.5Mhz in 50 Khz steps */
	byte iictxdata[4];

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = GET_FM_FREQ;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	iictxdata[0] = 0;
	iictxdata[1] = GET_SET_FM_FREQ;

	delay(2);	/* MANDATORY to get the correct data. It shoud be 10 ms. */

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 2);
	IICReceive(Q8TUNER_IIC_HW_ADDR, 2);

	fmchannel = (i2c.ReceiveBuffer[0] << 8) | i2c.ReceiveBuffer[1];
	return fmchannel;
}

/*********************************
* actually sets the channel
* freq[Khz]=(channel * 50) + 87500
**********************************/
void SetFMChannel(word fmchannel)
{
	byte iictxdata[4];

	iictxdata[0] = 0;
	iictxdata[1] = GET_SET_FM_FREQ;
	iictxdata[2] = (fmchannel >> 8) & 0xff;
	iictxdata[3] = fmchannel & 0xff;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 4);

	delay(2);	/* MANDATORY or else sometimes it does not accept the frequency. */

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = SET_FM_FREQ;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);
}

/***********************************
Tune DAB to selected index
***********************************/
void TuneDABService(byte serviceindex)
{
	byte iictxdata[4];

	art_debug("TuneDABService %d", serviceindex);

	/* Write service index to address (0x0006) first */
	iictxdata[0] = 0;			/* Address high byte */
	iictxdata[1] = DAB_NUMBER_OF_SERVICE;	/* Address low byte */
	iictxdata[2] = serviceindex;		/* Service index */
	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);	/* MANDATORY to get it working, delay to let the module get the service index. It shoud be 10 ms. */

	/* Tune to a service according to the index */
	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = KEY_TUNE_DAB_SERVICE;
	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);
}

void SetRadioChannel(enum radio_source source, word channel)
{
	if (source == RADIO_SOURCE_DAB)
		TuneDABService(channel);
	else
		SetFMChannel(channel);
}

/************************************************
*	Forward/Reverse scan on the FM Band
*************************************************/
void ScanFM(byte direction)
{
	byte iictxdata[4];

	if (direction == DIR_UP) {
		art_debug("SCAN FM FORWARD");

		iictxdata[0] = 0;
		iictxdata[1] = SEND_KEY_ID;
		iictxdata[2] = AUTO_SCAN_PRESS;

		IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);
	} else if (direction == DIR_DOWN) {
		art_debug("SCAN FM BACKWARD");

		iictxdata[0] = 0;
		iictxdata[1] = SEND_KEY_ID;
		iictxdata[2] = AUTO_SCAN_PRESHOLD;

		IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

		/* Add a long key release command */
		delay(10);
		iictxdata[0] = 0;
		iictxdata[1] = SEND_KEY_ID;
		iictxdata[2] = RELEASE_PRESS_HOLD_KEY;

		IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);
		delay(10);
	}

	delay(2);
}

/*****************************
Release key affteer a press hold key
******************************/
void TunerReleaseKey(void)
{
	byte iictxdata[4];

	if (key_press_hold) {
		iictxdata[0] = 0;
		iictxdata[1] = SEND_KEY_ID;
		iictxdata[2] = RELEASE_PRESS_HOLD_KEY;

		IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);
		delay(2);

		key_press_hold = 0;
	}
}

/****************************
Store current Radio channel
*****************************/
void store_radio_preset(enum radio_source source, byte presetnr, config_data_t *config)
{
	byte index = GetCurrentChannel(source);

	art_debug("Store radio Preset %i=[%i]\n\r", presetnr, index);

	config->radio_presets[source][presetnr] = index;
	config->current_radio_index[source] = index;
	configuration_changed();
}

/*****************************
Start a full DAB scan
******************************/
void FullDabScan(void)
{
	byte iictxdata[4];

	art_debug("FULL DAB SCAN");

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = AUTO_SCAN_PRESS;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	delay(2);
}

/****************************************
* Returns the number of registered DAB services in the module.
*****************************************/
word GetNumberOfServices(void)
{
	byte iictxdata[4];

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = GET_DAB_NUMBER_OF_SERVICE;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	iictxdata[0] = 0;
	iictxdata[1] = DAB_NUMBER_OF_SERVICE;

	delay(2);	/* MANDATORY to get the correct data. It shoud be 10 ms. */

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 2);

	IICReceive(Q8TUNER_IIC_HW_ADDR, 1);

	art_debug("Number of services=%i", i2c.ReceiveBuffer[0]);
	return i2c.ReceiveBuffer[0];
}

/*****************************************
* Get the name of the service
* NOTE destinationbuf must be at least 17 bytes in size !
******************************************/
void GetServiceName(char *destinationbuf)
{
	byte iictxdata[4];

	/* Make it an empty string */
	if (destinationbuf)
		destinationbuf[0] = 0;

	iictxdata[0] = 0;
	iictxdata[1] = SEND_KEY_ID;
	iictxdata[2] = GET_DAB_SERVICE_NAME;

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

	iictxdata[0] = 0;
	iictxdata[1] = DAB_SERVICE_NAME;

	delay(2);	/* MANDATORY to get the correct data. It shoud be 10 ms. */

	IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 2);
	IICReceive(Q8TUNER_IIC_HW_ADDR, 17);

	/* Add null terminator */
	i2c.ReceiveBuffer[i2c.ReceiveBuffer[0] + 1] = 0;

	if (destinationbuf) {
		strcpy(destinationbuf, (char *) &i2c.ReceiveBuffer[1]);
		art_debug("Service Name=[%s]\n\r", destinationbuf);
	}
}

void get_dab_service_info(struct dab_service_info *info)
{
	const int MAX_TRIES = 10;
	char station_name[TUNER_SERVICE_NAME_MAX + 1];
	byte iictxdata[4];
	int i;
	int retries = 0;

	/* Prevent endless loops */
	while (retries++ < MAX_TRIES) {
		iictxdata[0] = 0;
		iictxdata[1] = DAB_SERVICE_INFORMATION;
		iictxdata[2] = info->index;
		iictxdata[3] = info->index ^ 0xaa; /* Invert the data in this check byte */
		IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 4);

		delay(2);

		iictxdata[0] = 0;
		iictxdata[1] = SEND_KEY_ID;
		iictxdata[2] = GET_DAB_SERVICE_INFORMATION;

		IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 3);

		delay(2);	/* MANDATORY to get the correct data */

		iictxdata[0] = 0;
		iictxdata[1] = DAB_SERVICE_INFORMATION;
		IICSend(Q8TUNER_IIC_HW_ADDR, iictxdata, 2);

		delay(2);	/* MANDATORY to get the correct data */
		IICReceive(Q8TUNER_IIC_HW_ADDR, 2);

		/* These 2 bytes will be equal when the data is ready */
		if (i2c.ReceiveBuffer[0] == i2c.ReceiveBuffer[1])
			break;
	}

	IICReceive(Q8TUNER_IIC_HW_ADDR, TUNER_SERVICE_NAME_MAX + 1);

	/* Add a null terminator so it is a valid C string */
	i2c.ReceiveBuffer[1 + i2c.ReceiveBuffer[0]] = '\0';
	strcpy(info->name, &i2c.ReceiveBuffer[1]);

	art_debug("Genre=%02", i2c.ReceiveBuffer[0]);
	art_debug("Name %02i=[%s]", info->index, info->name);
}

/**************************************************************
* Find the service index of the current playing DAB station
***************************************************************/

/* If the host has enough memory to store DAB scan list information,
 * in order to reduce I2C transaction, the host should collect the scan list information after each DAB auto scan.
 */
void get_current_dab_info(struct dab_service_info *info)
{
	byte iictxdata[4];
	int i;
	int servicecnt;

	info->index = 0;
	info->name[0] = '\0';
	info->freq_ensemble = FREE;

	/* Get the number of DAB services, needed to iterate through the full list. */
	servicecnt = GetNumberOfServices();

	/* First get the name of the current selected DAB station */
	GetServiceName(currenstation);

	for (i = 0; i < servicecnt; i++) {
		struct dab_service_info tmp_info;
		info.index = i;

		get_dab_service_info(&tmp_info)

		if (strcmp(currenstation, info.name) == 0) {
			info->index = tmp_info.index;
			info->freq_ensemble = tmp_info.freq_ensemble;
			strcpy(info->name, tmp_info.name);
			break;
		}
	}
}

word GetCurrentChannel(enum radio_source source)
{
	struct dab_service_info dab_info;
	word index;

	switch (source) {
	case RADIO_SOURCE_DAB:
		get_current_dab_info(&dab_info);
		index = dab_info.index;
		break;
	case RADIO_SOURCE_FM:
		index = GetFMChannel();
		break;
	default:
		index = 0;
		break;
	}

	return index;
}
