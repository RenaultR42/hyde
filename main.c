/*
 *		ArtSound HYDE Firmware  - v1.4d
 *		<Thomas.Brijs@houseofmusic.be>
 *		<charles-antoine.couret@mind.be>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LPC11xx.h"
#include "main.h"
#include "constants.h"
#include "wdt.h"
#include "gpio.h"
#include "adc.h"
#include "i2c.h"
#include "rfdet.h"
#include "iap.h"
#include "bluetooth.h"
#include "tuner.h"
#include "platform_hyde.h"
#include "audio.h"
#include "audio_routing.h"
#include "config.h"
#include "utils.h"
#include "platform_power.h"
#include "timer.h"
#include "preset.h"

config_data_t config_data;

enum active_mode {
	ACTIVEMODE_BOOTUP	= 0,
	ACTIVEMODE_NORMAL,
	ACTIVEMODE_PAIRING,
	ACTIVEMODE_LINKING,
	ACTIVEMODE_VOLLIMIT,
	ACTIVEMODE_SETTINGS,
	ACTIVEMODE_BTNAME,
	ACTIVEMODE_APPCONTROL,
};

enum vol_limit_mode {
	VOLLIMIT_ENTER = 0,
	VOLLIMIT_OFF,
};

static bool key_ignore_repeats = false;
static bool key_ignore_release = false;
static int key_nb_pressed_action = 1;

static byte modeswitch = 0;
static byte vollimit = 0;

static enum plus_minus_button_mode key_up_down_function = VOLUME_ACTIVE;
static enum active_mode activemode = ACTIVEMODE_NORMAL;

static config_data_t config_snapshot;

static char BTPaired[] = "TONE N E5 L 16 D 20 TN R5 L 16 D 1 TN R5 L 16 N G5 L 32 D 20 TN A5 L 32 D 20 TN B5 L 32 D 20 TN C6 L 32 D 20 TN E6 L 8 D 20";

void set_active_mode(enum active_mode mode, enum audio_source source, config_data_t *config)
{
	switch (mode) {
	case ACTIVEMODE_NORMAL:
		do_beep(NOTE_EIGHTH, TONE_F, config);
		key_up_down_function = VOLUME_ACTIVE;

		set_source(source, config);
		break;
	case ACTIVEMODE_LINKING:
	default:
		break;
	}

	activemode = mode;
}

void change_channel(enum direction_button direction, enum audio_source audio_src, config_data_t *config)
{
	if (key_up_down_function == VOLUME_ACTIVE) {
		if (audio_src == SRC_FMTUNER || audio_src == SRC_DABTUNER) {
			enum radio_source source = audio_src == SRC_DABTUNER ? RADIO_SOURCE_DAB : RADIO_SOURCE_FM;
			byte round_cnt = 0;
			byte dir_inc = direction == DIR_DOWN ? -1 : 1;

			do {
				current_radio_preset[source] += dir_inc;
				if (current_radio_preset[source] > PRESET_6) {
					current_radio_preset[source] = PRESET_1;
				}
			} while (config->radio_presets[source][current_radio_preset[source]] == RADIO_FREE && ++round_cnt < NUM_PRESETS);

			select_radio_preset(current_radio_preset[source], source, config);
		} else if (audio_src == SRC_BLUETOOTH) {
			if (get_audio_state() == FFWD_PRESS) {
				control_bt(FFWD_RELEASE);
				set_audio_state(FFWD_RELEASE);
			}
		}
	}
}

void store_preset_from_remote(rfkey_stat_t *key_info, enum preset_keys key, enum audio_source audio_src, config_data_t *config)
{
	if (key_info->time_down >= STORE_PRESET_TIME) {
		key_ignore_repeats = true;
		key_ignore_release = true;
		store_preset(key, audio_src, config);
	}
}

void change_music(rfkey_stat_t *key, enum direction_button direction, config_data_t *config)
{
	enum audio_state state = NEXT;
	int dab_dir = 1;

	if (direction == DIR_DOWN) {
		state = PREV;
		dab_dir = -1;
	}

	switch (key_up_down_function) {
	case VOLUME_ACTIVE:
	default:
		if (key->time_down >= TIME_SCANNING_ACTIVE) {
			key_ignore_repeats = true;
			key_ignore_release = true;

			if (config->current_source == SRC_FMTUNER) {
				do_beep(NOTE_SIXTEENTH, TONE_C, config);
				delay(1);
				ScanFM(direction);
			} else if (config->current_source == SRC_DABTUNER) {
				if (!config->dab_scanned) {
					FullDabScan();
					config->dab_scanned = 1;
					store_config(config);
				} else {
					do_beep(NOTE_EIGHTH, TONE_E, config);
					delay(1);
					DABTune(dab_dir);
				}
			}
		} else {
			if (config->current_source == SRC_BLUETOOTH) {
				key_ignore_repeats = true;
				control_bt(state);
			}
		}
		break;
	}
}

void change_volume(rfkey_stat_t *key, enum direction_button direction, config_data_t *config)
{
	key_nb_pressed_action = VOLUME_RESPONSE_DIVIDER;

	if (key->time_down >= TIME_SHORTPRESS) {
		key_ignore_release = true;
	} else {
		key_nb_pressed_action = 1;
	}

	volume_up_down(direction, config);
}

void mode_normal_handler(config_data_t *config, rfkey_stat_t *key)
{
	if (remote_is_registered(config, key->addr)) {
		if (key->data_state == RF_KEY_DOWN) {
			if (config->power_state != POWER_ON) {
				switch (key->key) {
				case KEY_POWERON:
					if (config->eco_mode != ECOMODE_SETUP) {
						set_power(POWER_ON, config);
						config->power_state = POWER_ON;
						configuration_changed();
					} else {
						enable_amplifier(true);
						mute_amplifier(false);
						send_bc128("TONE V 48 N E7 L 16 D 20 N R7 L 16 N G7 L 16");
						config->eco_mode = ECOMODE_ON;	/* Ecomode on and setup off */
						store_config(config);

						art_debug("ECO On. From %d", config->eco_mode);

						set_tuner_power(false);
						enable_ldo(false);
						delay(TIME_SAVE_CONFIG);
						mute_amplifier(true);

						restore_gain(config->current_source);
						key_ignore_repeats = true;
					}
					break;
				case KEY_POWEROFF:
					if (config->eco_mode == ECOMODE_SETUP) {
						key_ignore_repeats = true;

						art_debug("ECO Off. From %d", config->eco_mode);

						enable_amplifier(true);
						mute_amplifier(false);
						send_bc128("TONE V 48 N G7 L 16 D 20 N R7 L 16 N E7 L 16");
						config->eco_mode = ECOMODE_OFF;
						store_config(config);
						/* NVIC_SystemReset(); */
						enable_ldo(true);
						delay(MODULES_RESET_DURATION);
						set_tuner_power(true);
						delay(MODULES_RESET_DURATION);
						init_radio_source(config->current_source);

						delay(TIME_SAVE_CONFIG);
						mute_amplifier(true);
						restore_gain(config->current_source);
					}
					break;
				case KEY_CFG:
					if ((key->time_down >= TIME_LONGPRESS) && (config->eco_mode != ECOMODE_SETUP)) {
						key_ignore_repeats = true;

						send_bc128("SET AUDIO_ANALOG=0 13 0 OFF");
						art_debug("ECO Setup - From %d", config->eco_mode);
						enable_amplifier(true);
						mute_amplifier(false);
						config->eco_mode = ECOMODE_SETUP;
						send_bc128("TONE V %i N F7 L 8", get_volume_tone(config));
						delay(40);
						mute_amplifier(true);
					}
					break;
				}
			} else if (config->power_state == POWER_ON) {
				switch (key->key) {
				case KEY_POWEROFF:
					if (config->power_state != POWER_OFF) {
						set_power(POWER_OFF, config);
						config->power_state = POWER_OFF;
						configuration_changed();
					}
					break;
				case KEY_1:
					store_preset_from_remote(key, PRESET_1, config->current_source, config);
					break;
				case KEY_2:
					store_preset_from_remote(key, PRESET_2, config->current_source, config);
					break;
				case KEY_3:
					store_preset_from_remote(key, PRESET_3, config->current_source, config);
					break;
				case KEY_4:
					store_preset_from_remote(key, PRESET_4, config->current_source, config);
					break;
				case KEY_5:
					store_preset_from_remote(key, PRESET_5, config->current_source, config);
					break;
				case KEY_6:
					store_preset_from_remote(key, PRESET_6, config->current_source, config);
					break;
				case KEY_FM:
					select_source(SRC_FMTUNER, config);
					break;
				case KEY_DAB:
					if (key->time_down < TIME_DAB_FULL_SCAN) {
						select_source(SRC_DABTUNER, config);
					} else if (key->time_down >= TIME_DAB_FULL_SCAN && config->current_source == SRC_DABTUNER) {
						key_ignore_repeats = true;
						key_ignore_release = true;
						FullDabScan();
						config->dab_scanned = 1;
					}
					break;
				case KEY_BT:
					if (key->time_down >= TIME_PAIRING_ACTIVE) {
						key_ignore_repeats = true;
						key_ignore_release = true;

						art_debug("Activate Pairing mode");

#ifdef DISABLE_TWS
						control_bt(PAIR_ON);
#else
						control_bt(INQUIRY_TWS);
#endif
						activemode = ACTIVEMODE_PAIRING;
					}
					break;
				case KEY_AUX:
					if (key->time_down >= TIME_SCANNING_ACTIVE) {
						select_source(SRC_DIGITAL, config);

						key_ignore_repeats = true;
						key_ignore_release = true;
					}
					break;
				case KEY_CFG:
					if (key->time_down >= TIME_LONGPRESS) {
						key_ignore_repeats = true;
						key_ignore_release = true;

						art_debug("Settings Mode");

						copy_config(config, &config_snapshot);
						activemode = ACTIVEMODE_SETTINGS;
					}
					break;
				case KEY_PLUS:
					change_volume(key, DIR_UP, config);
					break;
				case KEY_MIN:
					change_volume(key, DIR_DOWN, config);
					break;
				case KEY_NEXT:
					change_music(key, DIR_UP, config);
					break;
				case KEY_PREV:
					change_music(key ,DIR_DOWN, config);
					break;
				}
			}

			art_debug("Adr=0x%02X Data=0x%02X Key=0x%01X V=%i T=%i", key->addr, key->key, key->key, key->data_state, key->time_down);

			key->data_state = RF_KEY_HANDLED;
		} else if (key->data_state == RF_KEY_RELEASED && config->power_state == POWER_ON) {
			TunerReleaseKey();

			switch (key->key) {
			case KEY_1:
				select_preset(PRESET_1, config->current_source, config);
				break;
			case KEY_2:
				select_preset(PRESET_2, config->current_source, config);
				break;
			case KEY_3:
				select_preset(PRESET_3, config->current_source, config);
				break;
			case KEY_4:
				select_preset(PRESET_4, config->current_source, config);
				break;
			case KEY_5:
				select_preset(PRESET_5, config->current_source, config);
				break;
			case KEY_6:
				select_preset(PRESET_6, config->current_source, config);
				break;
			case KEY_AUX:
				if (key->time_down < TIME_SCANNING_ACTIVE) {
					select_source(SRC_ANALOG, config);
				}
				break;
			case KEY_BT:
				select_bluetooth_as_source(config);
				break;
			case KEY_NEXT:
				change_channel(DIR_UP, config->current_source, config);
				break;
			case KEY_PREV:
				change_channel(DIR_DOWN, config->current_source, config);
				break;
			case KEY_OK:
				if ((config->current_source == SRC_BLUETOOTH) && has_bluetooth_player()) {
					if (get_audio_state() != PLAY) {
						control_bt(PLAY);
					} else {
						control_bt(PAUSE);
					}
				}
				break;
			default:
				break;
			}

			key->data_state = RF_KEY_HANDLED;
		}
	}
}

void mode_pairing_handler(config_data_t *config, rfkey_stat_t *key, bool changed_menu)
{
	static struct timer timer_beep, timeout_pairing;

	if (changed_menu) {
		set_bt_pairing_status(PAIR_NONE);
		init_timer(&timer_beep);
		init_timer(&timeout_pairing);

		start_timer(&timer_beep, PAIRING_BEEP_INTERVAL);
		start_timer(&timeout_pairing, TIMEOUT_PAIRING_MODE);
	}

	/* Timeout pairing / setup mode
	 * When not paired within 1 min,then time out with Long Beep (2 sec).
	 */
	if (timer_has_fired(&timeout_pairing)) {
		set_bt_pairing_status(PAIR_ERROR);
	}

	if (timer_has_fired(&timer_beep)) {
		reset_timer(&timer_beep);
		send_bc128("TONE V %i N F7 L 8", get_volume_tone(config));
	}

	if (get_bt_pairing_status() > PAIR_NONE) {
		if (get_bt_pairing_status() == PAIR_OK) {
			send_bc128(BTPaired);
		} else if (get_bt_pairing_status() == PAIR_ERROR) {
			send_bc128("TONE V %i N E7 L 8", get_volume_tone(config));
		}

		control_bt(PAIR_OFF);
		set_bt_pairing_status(PAIR_NONE);
		activemode = ACTIVEMODE_NORMAL;
		return;
	}

	if (remote_is_registered(config, key->addr)) {
		if (key->data_state == RF_KEY_DOWN) {
			switch (key->key) {
			case KEY_RET:
				if (key->time_down < TIME_SCANNING_ACTIVE) {
					control_bt(PAIR_OFF);
					double_beep(NOTE_SIXTEENTH, TONE_G, TONE_E, config);
					set_active_mode(ACTIVEMODE_NORMAL, config->current_source, config);
				}
				break;
			default:
				break;
			}

			key->data_state = RF_KEY_HANDLED;
		} else if (key->data_state == RF_KEY_RELEASED) {
			key->data_state = RF_KEY_HANDLED;
		}
	}
}

void mode_linking_handler(config_data_t *config, rfkey_stat_t *key)
{
	switch (key->key) {
	case KEY_POWEROFF:
		switch (key->data_state) {
		case RF_KEY_DOWN:
			save_remote(config, key->addr);
			activemode = ACTIVEMODE_NORMAL;
			key->data_state = RF_KEY_HANDLED;
			key_ignore_repeats = true;
			break;
		case RF_KEY_RELEASED:
			key->data_state = RF_KEY_HANDLED;
			key_ignore_repeats = false;
			break;
		}
		break;
	default:
		break;
	}
}

void mode_volume_limit_handler(config_data_t *config, rfkey_stat_t *key, bool mode_changed)
{
	static int key_exp = BUFFER_KEYCODE_REMOTE_ADDR_SIZE - 1;
	static word keycode = 0;
	word code_digit = 0;

	if (mode_changed) {
		key_exp = BUFFER_KEYCODE_REMOTE_ADDR_SIZE - 1;
		keycode = 0;
	}

	if (remote_is_registered(config, key->addr) && config->power_state == POWER_ON) {
		if (key->data_state == RF_KEY_DOWN) {
			key->data_state = RF_KEY_HANDLED;
		} else if (key->data_state == RF_KEY_RELEASED) {
			if (key->key == KEY_RET) {
				vollimit = VOLLIMIT_OFF;
				double_beep(NOTE_SIXTEENTH, TONE_G, TONE_E, config);
			}

			code_digit = keyboard_digit_choice(key->key, KEYBOARD_REMOTE_ADDR_SIZE);

			if (vollimit && code_digit) {
				do_beep(NOTE_EIGHTH, TONE_E, config);
			}

			if (code_digit != 0) {
				/* Begins with 100, then 10 and unit like: (3*100+5+10+9*1) = 359 */
				keycode += (word) powerfunc(10, (int) key_exp) * code_digit;
				key_exp--;

				art_debug("Chars left: %d", key_exp);

				/* Input complete, can be stored */
				if (key_exp < 0) {
					if ((int) keycode == ((int) key->addr) * 10) {
						art_debug("Code OK: %d", keycode);

						if (config->max_volume >= MIN_VOLUME_ATTN) {
							config->max_volume = config->current_volume;
						} else {
							config->max_volume = MIN_VOLUME_ATTN;
						}

						do_beep(NOTE_EIGHTH, TONE_G, config);
						store_config(config);
						vollimit = VOLLIMIT_OFF;
					} else {
						do_beep(NOTE_EIGHTH, TONE_C, config);	/* Wrong code */
						art_debug("Wrong code: %d", keycode);
					}

					key_exp = BUFFER_KEYCODE_REMOTE_ADDR_SIZE - 1;
					keycode = 0;
				}
			} else {
				do_beep(NOTE_SIXTEENTH, TONE_C, config);	/* Invalid key */
				art_debug("Key != [1-6]", 0);
			}

			key->data_state = RF_KEY_HANDLED;
		}

		if (vollimit == VOLLIMIT_OFF) {
			activemode = ACTIVEMODE_NORMAL;
			key_up_down_function = VOLUME_ACTIVE;
		}
	}
}

void change_balance(enum balance_button button, config_data_t *config)
{
	if (key_up_down_function == BALANCE_ACTIVE) {
#ifndef DEBUG_GAIN
		balance_left_right(button, config);
#else
		enum direction_button dir = button == BAL_RIGHT ? DIR_UP : DIR_DOWN;

		in_out_gain(dir);
#endif

		key_nb_pressed_action = BALANCE_RESPONSE_DIVIDER;
	}
}

void select_freq_boost(enum freq_boost boost, config_data_t *config)
{
	key_ignore_repeats = true;
	key_up_down_function = VOLUME_ACTIVE;

	if (config->boost != boost) {
		set_tone(boost, config);

		art_debug("Boost ON");
	}

	do_beep(NOTE_EIGHTH, TONE_F, config);
}

void mode_settings_handler(config_data_t *config, rfkey_stat_t *key, bool changed_menu)
{
	static struct timer timeout_menu;

	if (remote_is_registered(config, key->addr) && config->power_state == POWER_ON) {
		if (changed_menu) {
			init_timer(&timeout_menu);
			start_timer(&timeout_menu, TIMEOUT_SETTINGS_MENU);

			do_beep(NOTE_EIGHTH, TONE_F, config);
			delay(10);

			art_debug("Entered settings");

			key->data_state = RF_KEY_HANDLED;
		}

#ifndef DEBUG_GAIN
		if (timer_has_fired(&timeout_menu)) {
			double_beep(NOTE_SIXTEENTH, TONE_E, TONE_G, config);
			activemode = ACTIVEMODE_NORMAL;
			key_up_down_function = VOLUME_ACTIVE;
			store_config(config);
			return;
		}
#endif

		if (key->data_state == RF_KEY_DOWN) {
			reset_timer(&timeout_menu);

			switch (key->key) {
			case KEY_1:
				select_freq_boost(FREQ_BOOST_BASS, config);
				break;
			case KEY_2:
				select_freq_boost(FREQ_BOOST_MID, config);
				break;
			case KEY_3:
				select_freq_boost(FREQ_BOOST_TREB, config);
				break;
			case KEY_4:
				if (key_up_down_function != BALANCE_ACTIVE)
					do_beep(NOTE_EIGHTH, TONE_F, config);

				key_up_down_function = BALANCE_ACTIVE;

				art_debug("Balance control active");

				if (key->time_down >= TIME_LONGPRESS) {
					key_ignore_repeats = true;
					if (config->balance != DEFAULT_BALANCE) {
						do_beep(NOTE_SIXTEENTH, TONE_D, config);
						set_balance(DEFAULT_BALANCE);
						config->balance = DEFAULT_BALANCE;
						key_up_down_function = VOLUME_ACTIVE;

						art_debug("Balance reset");
					}
				}
				break;
			case KEY_6:
				key_ignore_repeats = true;
				key_up_down_function = VOLUME_ACTIVE;

				reset_tone(config);
				do_beep(NOTE_EIGHTH, TONE_F, config);
				break;
			case KEY_NEXT:
				change_balance(BAL_RIGHT, config);
				break;
			case KEY_PREV:
				change_balance(BAL_LEFT, config);
				break;
			case KEY_OK:
				if (key->time_down < TIME_SCANNING_ACTIVE) {
					key_ignore_repeats = true;
					key_ignore_release = true;

					art_debug("Save and exit");

					store_config(config);
					double_beep(NOTE_SIXTEENTH, TONE_E, TONE_G, config);
					activemode = ACTIVEMODE_NORMAL;
					key_up_down_function = VOLUME_ACTIVE;
					reset_timer(&timeout_menu);
				}
				break;
			case KEY_BT: /* To RET */
				modeswitch = BTNAME_ENTER;
				key->data_state = RF_KEY_HANDLED;
			case KEY_AUX: /* To RET */
				if (modeswitch == BTNAME_OFF)
					vollimit = VOLLIMIT_ENTER;
			case KEY_RET:
				key_ignore_repeats = true;

				art_debug("Exit and restore");

				copy_config(&config_snapshot, config);
				apply_config(config);

				reset_timer(&timeout_menu);

				if (modeswitch) {
					double_beep(NOTE_SIXTEENTH, TONE_E, TONE_G, config);
					activemode = ACTIVEMODE_BTNAME;
				} else if (vollimit == VOLLIMIT_ENTER) {
					double_beep(NOTE_SIXTEENTH, TONE_E, TONE_G, config);
					key->data_state = RF_KEY_HANDLED;
					activemode = ACTIVEMODE_VOLLIMIT;
				} else {
					double_beep(NOTE_SIXTEENTH, TONE_G, TONE_E, config);
					activemode = ACTIVEMODE_NORMAL;
				}

				key_up_down_function = VOLUME_ACTIVE;
				break;
			default:
				break;
			}

			key->data_state = RF_KEY_HANDLED;
		} else if (key->data_state == RF_KEY_RELEASED) {
			TunerReleaseKey();

			switch (key->key) {
			case KEY_5:
				if (key->time_down < TIME_LONGPRESS) {
					key_ignore_repeats = true;

					if (config->enable_mono) {
						config->enable_mono = audio_switch(AUDIO_SWITCH_MODE_STEREO);

						double_beep(NOTE_SIXTEENTH, TONE_F, TONE_F, config);
						art_debug("Stereo Mode");
					} else {
						config->enable_mono = audio_switch(AUDIO_SWITCH_MODE_MONO);

						if (config->balance != DEFAULT_BALANCE) {
							config->balance = DEFAULT_BALANCE;
							set_balance(config->balance);
						}

						do_beep(NOTE_EIGHTH, TONE_F, config);
						art_debug("Mono Mode");
					}
				}
				break;

			default:
				break;
			}

			key->data_state = RF_KEY_HANDLED;
		}
	}
}

void init_values(config_data_t *config)
{
	art_debug("Loading Startup Values");

	set_audio_state(STOP);
	activemode = ACTIVEMODE_NORMAL;
	key_up_down_function = VOLUME_ACTIVE;

	load_config(config);

	if (config->eco_mode == ECOMODE_SETUP)
		config->eco_mode = ECOMODE_ON;

	reset_bc128(config);

#if 1
	set_power_mode(config->power_state, config->eco_mode, config->current_source);
#else
	set_power_mode(config->power_state, ECOMODE_ON, config->current_source);
#endif

	apply_config(config);

	art_debug("Volume %d", config->current_volume);
	art_debug("Powerstate %d", config->power_state);

	configuration_changed();
}

void mode_btname_handler(config_data_t *config, rfkey_stat_t *key, bool mode_changed)
{
	static int key_exp = BUFFER_KEYCODE_BT_NAME_SIZE - 1;
	static word keycode = 0;
	char name[SERIAL_BUFFER_SIZE_MAX];
	word code_digit = 0;

	if (mode_changed) {
		key_exp = BUFFER_KEYCODE_BT_NAME_SIZE - 1;
		keycode = 0;
	}

	if (remote_is_registered(config, key->addr) && config->power_state == POWER_ON) {
		key_ignore_repeats = true;

		if (modeswitch == BTNAME_ENTER) {
			delay(5);
			key->key = KEY_OK;
			modeswitch = BTNAME_ACTIVE;
		}

		if (key->data_state == RF_KEY_DOWN) {
			key->data_state = RF_KEY_HANDLED;
		} else if (key->data_state == RF_KEY_RELEASED) {
			if (key->key == KEY_RET) {
				modeswitch = BTNAME_OFF;
			}

			code_digit = keyboard_digit_choice(key->key, KEYBOARD_BT_NAME_SIZE);

			art_debug("Char: %d", code_digit);

			if (modeswitch)
				do_beep(NOTE_EIGHTH, TONE_E, config);

			if ((code_digit >= 0) && (code_digit <= 9)) {
				/* Begins by 100, then 10 and unit */
				keycode += (word) powerfunc(10, (int) key_exp) * code_digit;
				key_exp--;

				art_debug("Chars left: %d", key_exp);

				if (key_exp < 0) {
					art_debug("Code OK: %d", keycode);

					key_exp = BUFFER_KEYCODE_BT_NAME_SIZE - 1;
					double_beep(NOTE_SIXTEENTH, TONE_E, TONE_G, config);
					sprintf(name, "%d", keycode);
					update_bt_name(name, config);
					delay(40);

					modeswitch = BTNAME_OFF;
					init_values(config);
				}
			} else {
				do_beep(NOTE_EIGHTH, TONE_C, config);	/* Invalid key */
				art_debug("0 <= KEY <= 9", 0);
			}

			key->data_state = RF_KEY_HANDLED;

		}

		if (modeswitch == BTNAME_OFF) {
			activemode = ACTIVEMODE_NORMAL;
			key_up_down_function = VOLUME_ACTIVE;
		}
	}
}

void setup_handler(config_data_t *config, enum setup_status status)
{
	switch (status) {
	case SETUP_FULLRESET:
		do_full_reset(config);
		break;
	case SETUP_LINKMODE:
		switch (activemode) {
		case ACTIVEMODE_LINKING:
			set_active_mode(ACTIVEMODE_NORMAL, config->current_source, config);
			break;
		default:
			set_active_mode(ACTIVEMODE_LINKING, config->current_source, config);
			break;
		}
		break;
	default:
		break;
	}
}

void check_full_reset(config_data_t *config)
{
	static struct timer timer_setup;
	static bool init = false;
	static bool always_pressed = true;
	enum setup_status status = SETUP_NONE;

	/* Check setup input button */
	if (setup_button_status()) {
		if (!init || !timer_is_enabled(&timer_setup)) {
			init_timer(&timer_setup);
			start_timer(&timer_setup, SETUP_MODE_TIME);
			init = true;
		} else if (timer_has_fired(&timer_setup)) {
			if (always_pressed) {
				status = SETUP_FULLRESET;
			} else {
				status = SETUP_LINKMODE;
			}
		}
	} else {
		disable_timer(&timer_setup);
		always_pressed = false;
	}

	setup_handler(config, status);
}

void led_rf_setup(enum rf_key_status key_status, bool enable)
{
	static struct timer time;
	static bool init = false;

	if (key_status == RF_KEY_DOWN) {
		if (!init || !timer_is_enabled(&time)) {
			init = true;
			init_timer(&time);
			start_timer(&time, LED_TIME_VALID_RF);
		} else if (timer_has_fired(&time)) {
			enable_led(!enable);
			reset_timer(&time);
		}
	} else {
		disable_timer(&time);
		enable_led(enable);
	}
}

void powerstate_led(enum rf_key_status key_status, enum power_state state)
{
	if (state == POWER_ON) {
		led_rf_setup(key_status, true);
	} else {
		led_rf_setup(key_status, false);
	}
}

void lookup_active_mode(config_data_t *config, rfkey_stat_t *key)
{
	static enum rf_keys old_key = KEY_UNKWNON;
	static enum active_mode old_mode = ACTIVEMODE_BOOTUP;
	static int key_cnt = 0;
	bool mode_changed = false;

#if 0
	if (key->data_state == RF_KEY_HANDLED) {
		return;
	}
#endif

	if (old_mode != activemode)
		mode_changed = true;

	old_mode = activemode;

	if (!mode_changed) {
		if (old_key == key->key && key->data_state == RF_KEY_DOWN) {
			key_cnt++;
		} else {
			key_cnt = 0;
		}

		if (old_key == key->key && key->data_state == RF_KEY_DOWN && key_ignore_repeats) {
			return;
		} else if (old_key == key->key && key->data_state == RF_KEY_RELEASED && key_ignore_release) {
			key->data_state = RF_KEY_HANDLED;
			key_nb_pressed_action = 1;
			return;
		} else if (old_key == key->key && key->data_state == RF_KEY_DOWN && key_nb_pressed_action > 1) {
			if (key_cnt != key_nb_pressed_action)
				return;
			else if (key_cnt == key_nb_pressed_action)
				key_cnt = 0;
		} else if (old_key != key->key) {
			key_ignore_repeats = false;
			key_ignore_release = false;
			key_nb_pressed_action = 1;
			old_key = key->key;
		}
	} else {
		key_ignore_repeats = false;
		key_ignore_release = false;
		key_nb_pressed_action = 1;
		old_key = key->key;
		key_cnt = 0;
	}

	switch (activemode) {
	case ACTIVEMODE_LINKING:
		mode_linking_handler(config, key);
		break;
	case ACTIVEMODE_PAIRING:
		mode_pairing_handler(config, key, mode_changed);
		break;
	case ACTIVEMODE_SETTINGS:
		mode_settings_handler(config, key, mode_changed);
		break;
	case ACTIVEMODE_VOLLIMIT:
		mode_volume_limit_handler(config, key, mode_changed);
		break;
	case ACTIVEMODE_BTNAME:
		mode_btname_handler(config, key, mode_changed);
		break;
	case ACTIVEMODE_NORMAL:
	default:
		mode_normal_handler(config, key);
		break;
	}
}

enum tuner_status get_radio_status(bool radio_power, enum audio_source source)
{
	static struct timer time_get_status;
	static bool init = false;
	enum tuner_status new_status = STATUS_ERROR;

	if (radio_power && (source == SRC_DABTUNER || source == SRC_FMTUNER)) {
		if (!init|| !timer_is_enabled(&time_get_status)) {
			init = true;
			init_timer(&time_get_status);
			start_timer(&time_get_status, RADIO_POLL_INTERVAL);
		} else if (timer_has_fired(&time_get_status)) {
			reset_timer(&time_get_status);
			new_status = GetRadioStatus();
		}
	} else {
		disable_timer(&time_get_status);
	}

	return new_status;
}

void auto_scan_dab_timer(config_data_t *config, enum tuner_status status, enum audio_source source)
{
	static struct timer timer_scan;
	static bool init = false;

	if (status == STATUS_DAB_AUTO_SCAN && source == SRC_DABTUNER) {
		if (!init || !timer_is_enabled(&timer_scan)) {
			init = true;
			init_timer(&timer_scan);
			start_timer(&timer_scan, INTERVAL_AUTOSCAN_BEEP);
		} else if (timer_has_fired(&timer_scan)) {
			send_bc128("TONE V %i N A7 L 8", get_volume_tone(config));

			reset_timer(&timer_scan);
		}
	} else {
		disable_timer(&timer_scan);
	}
}

void pressed_key_time(rfkey_stat_t *key, unsigned int delta)
{
	/* Timing RFKEY */
	if (timer_is_enabled(&key->key_release_detection)) {
		if (key->time_down < MAX_KEYDOWNTIME)
			key->time_down += delta;

		if (timer_has_fired(&key->key_release_detection)) {
			key->data_state = RF_KEY_RELEASED;
			key->time_down = 0;
		}
	}
}

int main(void)
{
	struct timer blink_timer;
	unsigned int old_cnt = get_monotonic_clock();
	unsigned int cnt = get_monotonic_clock();
	rfkey_stat_t *key = NULL;
	int delta;

	enum tuner_status old_radio_state = STATUS_ERROR;
	enum tuner_status new_radio_state;

	hardware_init();
	init_preset();
	init_values(&config_data);

	art_debug("ECO Mode: %d", config_data.eco_mode);
	art_debug("Main loop");

	init_timer(&blink_timer);
	start_timer(&blink_timer, BLINKING_INTERVAL);

	while (true) {
		cnt = get_monotonic_clock();
		delta = compute_delta(old_cnt, cnt);
		old_cnt = cnt;

#ifndef NO_WDT
		WDTFeed();
#endif

		process_bluetooth_queue(&config_data);
		key = get_rf_key();
		pressed_key_time(key, delta);

		new_radio_state = get_radio_status(tuner_is_enabled(), config_data.current_source);
		if (new_radio_state != old_radio_state && new_radio_state != STATUS_ERROR) {
			art_debug("New Radio State %d", new_radio_state);
			old_radio_state = new_radio_state;
		}

		if (activemode != ACTIVEMODE_LINKING) {
			powerstate_led(key->data_state, config_data.power_state);
		} else {
			blinking_led(&blink_timer);
		}

		auto_scan_dab_timer(&config_data, new_radio_state, config_data.current_source);
		check_external_trigger_power(&config_data);
		check_full_reset(&config_data);
		lookup_active_mode(&config_data, key);

		auto_save_configuration(&config_data);
	}
}
