#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "utils.h"
#include "platform_hyde.h"
#include "uart.h"
#include "timer.h"

uint8_t char_to_digit(char c)
{
	uint8_t ret = 0;

	if (c >= '0' && c <= '9') {
		ret = c - '0';
	} else if (c >= 'a' && c <= 'f') {
		ret = (c - 'a') + 10;
	} else if (c >= 'A' && c <= 'F') {
		ret = (c - 'A') + 10;
	}

	return ret;
}

bool find_substr(const char *string, const char *substr)
{
	bool found = false;

	if (strncmp(string, substr, strlen(substr)) == 0)
		found = true;

	return found;
}

bool get_argument(char *string, char *arg, char delim, int num_arg)
{
	char *substr = string;
	char *start_arg = NULL;
	int arg_len = 0;
	bool arg_found = false;

	while (num_arg-- > 0 && substr != NULL) {
		substr = strchr(substr, delim);

		/* To skip the delimiter for the next loop */
		if (substr && strlen(substr)) {
			substr++;
		}
	}

	if (substr && num_arg <= 0) {
		start_arg = substr;

		substr = strchr(start_arg, delim);
		if (substr) {
			arg_len = substr - start_arg;
		} else {
			arg_len = strlen(start_arg);
		}

		strncpy(arg, start_arg, arg_len);
		arg[arg_len] = '\0';
		arg_found = true;
	}

	return arg_found;
}

void delay(int delay_10ms)
{
	struct timer timeout;

	init_timer(&timeout);
	start_timer(&timeout, delay_10ms);

	do {
		;
	} while (!timer_has_fired(&timeout));
}

long int powerfunc(int x, int n)
{
	int i;
	int number = 1;

	for (i = 0; i < n; ++i)
		number *= x;

	return number;
}

int compute_delta(unsigned int old_cnt, unsigned int cnt)
{
	int delta;

	if (old_cnt > cnt) {
		delta = cnt + (0xFFFFFFFF - old_cnt);
	} else {
		delta = cnt - old_cnt;
	}

	return delta;
}

int old_art_debug(char *output, int value)
{
#ifdef ARTDEBUG
	while (uart_is_busy())
		;

	use_uart(true);

	if (rs232_switch_state() != RS232_SWITCH_MODE_CONTROLLER) {
		rs232_switch(RS232_SWITCH_MODE_CONTROLLER);
		delay(2);
	}

	if (value) {
		value = printf("* %s: %i\r", output, value);
	} else {
		value = printf("* %s\r", output);
	}

	delay(3);
	rs232_switch(RS232_SWITCH_MODE_BLUETOOTH);
	use_uart(false);
#else
	if (rs232_switch_state() != RS232_SWITCH_MODE_BLUETOOTH) {
		rs232_switch(RS232_SWITCH_MODE_BLUETOOTH);
	}

	value = 0;
#endif

	return value;
}

int _art_debug(char *output, ...)
{
#ifdef ARTDEBUG
	char cmd[SERIAL_BUFFER_SIZE_MAX];
	va_list argptr;

	while (uart_is_busy())
		;

	use_uart(true);

	if (rs232_switch_state() != RS232_SWITCH_MODE_CONTROLLER) {
		rs232_switch(RS232_SWITCH_MODE_CONTROLLER);
		delay(2);
	}

	va_start(argptr, output);
	vsprintf(cmd, output, argptr);
	va_end(argptr);
	printf("%s\r", cmd);

	delay(3);
	rs232_switch(RS232_SWITCH_MODE_BLUETOOTH);
	use_uart(false);
#else
	if (rs232_switch_state() != RS232_SWITCH_MODE_BLUETOOTH) {
		rs232_switch(RS232_SWITCH_MODE_BLUETOOTH);
	}
#endif

	return 0;
}
