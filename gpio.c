#include "LPC11xx.h"
#include "gpio.h"

bool get_gpio(GPIO *gpio, int num)
{
	return ((gpio->DATA >> num) & 0x1) ? true : false;
}

void set_gpio(GPIO *gpio, int num, bool value)
{
	if (value) {
		gpio->DATA |= (1UL << num);
	} else {
		gpio->DATA &= ~(1UL << num);
	}
}

void toggle_gpio(GPIO *gpio, int num)
{
	set_gpio(gpio, num, !get_gpio(gpio, num));
}

void set_gpio_direction(GPIO *gpio, int num, enum gpio_direction direction)
{
	if (direction == GPIO_OUTPUT) {
		gpio->DIR |= (1UL << num);
	} else {
		gpio->DIR &= ~(1UL << num);
	}
}

void gpio_clear_irq(GPIO *gpio, int num)
{
	gpio->IC |= (1UL << num);
}

void set_port_pinmux(IO_PORT *port, uint32_t mode)
{
	const uint32_t MASK = 0x4BF;
	uint32_t old = *port & ~MASK;

	*port = old | (mode & MASK);
}
