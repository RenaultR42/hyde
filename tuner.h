#ifndef H_TUNER
#define H_TUNER

#include <stdbool.h>

#include "types.h"

enum tuner_mode {
	UNKNOWN_MODE = 0,
	DAB_MODE = 1,
	FM_MODE = 2,
	STANDBY = 4,
};

#define TUNER_SERVICE_NAME_MAX 17
#define TUNER_NO_FREQ_ENSEMBLE 0xFF

struct dab_service_info {
	char name[TUNER_SERVICE_NAME_MAX + 1];
	int freq_ensemble;
	int index;
};

#define FMAUTOSCAN_LEVEL	5
#define Q8TUNER_IIC_HW_ADDR	0x50

#define LCM_UPDATE_COUNTER	135	/* DLR=1 */
#define LCM_DATA_BUFFER	136	/* DLR=32 */
/* Read LCM content : The Default LCM is a 16x2 type,
 *  there are total 32 bytes presenting the character content.
 * External host read the LCM update counter to know if the content is changed or not.
 * The counter will increase by 1 every time the content is updated
 * and the number wraps to 0 after 255.
 */

#define SEND_KEY_ID		168	/* DLS=1 */
#define READ_RADIO_STATUS	169	/* DLR=1 */

/* Status codes responses on READ_RADIO_STATUS */
enum tuner_status {
	/* After  HW_INITIALIZATION_COMPLETED the module will accept commands from the I2C interface. */
	STATUS_HW_INITIALIZATION_COMPLETED	= 0x20,
	STATUS_STANDBY				= 0x40,

	/* Status DAB_AUTO_SCAN is active while DAB full scan is busy */
	STATUS_DAB_AUTO_SCAN			= 0x60,

	/* DAB service list contains valid data  */
	STATUS_DAB_SERVICE_LIST		= 0x61,
	STATUS_DAB_MANUAL_TUNE			= 0x62,
	DAB_SERVICE_WITHIN_SPECIFIC_ENSEMBLE	= 0x63,

	/* DAB full scan resulted in nothing found */
	STATUS_ALARM_TO_DAB_BUT_PLAY_FAIL	= 0x64,

	/* Manual tuned */
	STATUS_FM_MANUAL			= 0x80,

	/* Status while seeking is active */
	STATUS_FM_SEEKING			= 0x81,
	STATUS_FM_SENSITIVITY_ADJUSTMENT	= 0x82,
	STATUS_RESET_EXECUTING			= 0xF0,

	/* The RESET_DONE status will occur after receiving a reset key command (0x62)
	 * from the host (your CPU), the preceding status of this is "RESET_EXECUTING".
	 * If Q8 is in RESET_DONE status, it will halt and does not response to any commands from the host.
	 */
	STATUS_RESET_DONE			= 0xF1,
	STATUS_ERROR				= 0xFF,
};

#define READ_CURRENT_DATE	170	/* DLR=4 */
/* (a) Data Length : 4 byte
 * (b) Initial Address, 170 (Decimal)
 * (c) Number:
 *      1) Year: 1 byte, (Offset from Year 2000) (address = 170)
 *      2) Month: 1 byte (address = 171)
 *      3) Day: 1 byte (address = 172)
 *      4) Week: 1 byte, 0: Sunday ... 6: Saturday (address = 173)
 */

#define READ_CURRENT_TIME	174	/* DLR=3 */
/* (a) Data Length: 3 byte
 * (b) Initial Address, 174 (Decimal)
 * (c) Number:
 *       1) Hour: 1 byte, (address = 174)
 *       2) Minute: 1 byte (address = 175)
 *       3) Second: 1 byte (address = 176)
 */

#define DATETIME_SYNCHRONIZATION_STATUS	177	/* DLR=1 */
/* (a) Data Length: 1 byte
 * (b) Start Address, 177 (Decimal)
 * (c) Number: 0, NOT Synchronized; 1, Synchronized
 */

#define GET_SET_FM_FREQ			178	/* DL=2 */
/* (a) Data Length: 2 byte
 * (b) Start Address, MSB,178 (Decimal); LSB, 179 (Decimal)
 * (c) MSB first then LSB, value form 0 to 410.
 *       Frequency to value:
 *       Value = (frequency 87.5Mhz) / 50 KHz
 * (d) Set frequency:
 *       Write frequency value to start address first, then writing 0xA0 to key id address.
 * (e) Get frequency:
 *      Write 0xA1 to key id address, then reading the frequency value.
 */

#define FM_SIGNAL_STRENGTH			180	/* DLR=1 */
/* (a) Data Length: 1 byte
 * (b) Start Address, 180 (Decimal)
 * (c) Value form 0 (no signal) to16 (strong signal).
 * (d) Get signal strength:
 *      Write 0xA2 to key id address, then reading the signal strength.
 */

#define DAB_SERVICE_NAME			6
/* (a) Data Length: 1 byte + n bytes; the maximum value of n is 16.
 * (b) Start Address, 6 (Decimal)
 * (c) Service name length: byte at address 6 indicates the length of service name string.
 * (d) Service name string: string starts from address 7, and has n characters.
 */

#define DAB_ENSEMBLE_FREQUENCY			6
/* (a) Data Length: 1 byte.
 * (b) Start Address, 6 (Decimal)
 * (c) Ensemble frequency index: from 0 to 40. 0xFF indicates no ensemble
 */

#define DAB_NUMBER_OF_SERVICE			6
/* (a) Data Length: 1 byte.
 * (b) Start Address, 6 (Decimal)
 * (c) Number of service: the maximum number is 100.
 */

#define DAB_SERVICE_INFORMATION		6
/* (a) Data Length: index (1+1 bytes), ensemble frequency index (1byte),
 *       program type (1byte), service name length (1byte), service name (n bytes)
 * (b) Start Address, 6 (Decimal)
 * (c) Index: the index in the service list, from 0 to (number of service - 1).
 *               The first byte is send by host, and the second byte is slave responses to host.
 *       Ensemble frequency index: same as DAB ensemble frequency.
 *       Program type: the value is from 0 to 31.
 *       Service name length: the maximum is 16.
 *       Service name: the string of service name.
 * (d) Get service information sequence:
 * (1) Write two bytes to index address:
 *       The first byte is the required service index, and the second byte is for checking.
 *       The first byte and second one should have different.
 *               For example:
 *               First byte is 0x00, and the second is 0xFF. Service information that
 *               index is 0 in the service list will be retrieved. Information is ready
 *               after the value of second byte is 0x00.
 * (2) Write 0xB4 to key ID address.
 * (3) Read two bytes from index address.
 * (4) Read all information of the service, when these two index bytes have the same value.
 */

#define TUNE_DAB_SERVICE		6
/* (a) Data Length: index (1 byte).
 * (b) Start Address, 6 (Decimal)
 * (c) Index: the index in the service list, from 0 to (number of service - 1).
 */

#define PAD_DETECTION_GPIO		6
/* (a) GPIO 26: Slave mode Module Pin 31.
 * (b) Signal Level: level high represents PAD stream ready
 */

#define PAD_ADDRESS_AND_SIZE		181
/* (a) PAD Address: start address, 181 (Decimal); Length, 2 bytes; High byte first.
 * (b) PAD Size: start address, 183 (Decimal); Length, 2 bytes; High byte first.
 */

#define GET_FIRMWARE_VERSION		6
/* (a) Data Length: major number (1 byte), and minor number (1 byte).
 * (b) Start Address, 6 (Decimal); first byte is major version number and the second is minor version number.
 * (c) Get Firmware Version sequence:
 *       (1) Write 0xC0 to key ID address.
 *       (2) Read two bytes from Start address (0x06).
 */

#define SET_GET_FM_DETECTION_THRESHOLD	6	/* This address is 0x06. Quantek */
/* (a) Data Length: 1 byte
 * (b) Start Address: 6 (Decimal)
 * (c) Value: 1 ~ 15
 *       The default value is 8. Increasing the value will cause that low RF
 *       power stations will not be detected during FM seeking.
 * (d) Set threshold:
 *       Write threshold value to start address first, then writing 0xA3 to key id address.
 * (e) Get threshold:
 *       Write 0xA4 to key id address, then reading the threshold value.
 */

/* Key Codes */
#define M1_PRESS		0x40
#define M2_PRESS		0x41
#define M3_PRESS		0x42
#define M4_PRESS		0x43
#define M5_PRESS		0x44
#define PRESET_PRESS		0x45
#define MUTE_PRESS		0x46
#define STANDBY_PRESS		0x47
#define ALARM_PRESS		0x48
#define AUTO_SCAN_PRESS	0x49
#define UP_PRESS		0x4A
#define SLEEP_PRESS		0x4B
#define ENTER_PRESS		0x4C
#define DOWN_PRESS		0x4D
#define MENU_PRESS		0x4E
#define BAND_PRESS		0x4F

#define DAB_BAND_PRESS		0x60
#define FM_BAND_PRESS		0x61
#define RESET_PRESS		0x62

#define M1_PRESS_HOLD		0x80
#define M2_PRESS_HOLD		0x81
#define M3_PRESS_HOLD		0x82
#define M4_PRESS_HOLD		0x83
#define M5_PRESS_HOLD		0x84
#define PRESET_PRESS_HOLD	0x85
#define MUTE_PRESS_HOLD	0x86
#define STANDBY_PRESS_HOLD	0x87
#define NULL_KEY		0x88

#define AUTO_SCAN_PRESHOLD	0x89
#define UP_PRESS_HOLD		0x8A
#define SLEEP_PRESS_HOLD	0x8B
#define ENTER_PRESS_HOLD	0x8C
#define DOWN_PRESS_HOLD	0x8D
#define MENU_PRESS_HOLD	0x8E
#define BAND_PRESS_HOLD	0x8F

#define SET_FM_FREQ		0xA0
#define GET_FM_FREQ		0xA1
#define GET_FM_SIGNAL		0xA2
#define SET_FM_DET_THRESHOLD	0xA3
#define GET_FM_DET_THRESHOLD	0xA4

#define GET_DAB_DLS			0xB0
#define GET_DAB_SERVICE_NAME		0xB1
#define GET_DAB_ENSEMBLE_FREQ		0xB2
#define GET_DAB_NUMBER_OF_SERVICE	0xB3
#define GET_DAB_SERVICE_INFORMATION	0xB4
#define KEY_TUNE_DAB_SERVICE		0xB5

#define TURN_ON_DAB_SLIDE_SHOW		0xB6
#define TURN_OFF_DAB_SLIDE_SHOW	0xB7

#define KEY_GET_FIRMWARE_VERSION	0xC0
#define RELEASE_PRESS_HOLD_KEY		0xFF

#include "config.h"

bool tuner_has_booted(void);
void set_tuner_power(bool enable);
void InitRadio(enum tuner_mode mode);
enum tuner_status GetRadioStatus(void);
enum tuner_mode get_radio_mode(void);
void SetTunerMode(enum tuner_mode mode);
void SetTunerPowerOff(void);
void SetFMScanThreshold(byte scanthreshold);
byte GetFMScanThreshold(void);
void DABTune(byte direction);
void SetRadioChannel(enum radio_source source, word fmchannel);
word GetNumberOfServices(void);
void get_dab_service_info(struct dab_service_info *info);
void GetServiceName(char *destinationbuf);
word GetCurrentChannel(enum radio_source source);
void ScanFM(byte direction);
void TunerReleaseKey(void);
void store_radio_preset(enum radio_source source, byte presetnr, config_data_t *config);
void SetMenu(void);
void ResetTunerModule(void);
void FullDabScan(void);

#endif
