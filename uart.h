#ifndef H_UART
#define H_UART

#include <stdbool.h>
#include <stdint.h>

#include "types.h"

typedef struct {
	word rx_byte_cnt;
	bool cmd_read;
	byte cmd_data_buf[SERIAL_RX_BUFFERSIZE];
} serial_comm_t;

#define IER_RBR         (0x01<<0)
#define IER_THRE        (0x01<<1)
#define IER_RLS         (0x01<<2)
#define IER_ABEO        (0x01<<8)
#define IER_ABTO        (0x01<<9)
#define IIR_PEND        0x01
#define IIR_RLS         0x03
#define IIR_RDA         0x02
#define IIR_CTI         0x06
#define IIR_THRE        0x01
#define IIR_ABEO        (0x01<<8)
#define IIR_ABTO        (0x01<<9)

#define LSR_RDR         (0x01<<0)
#define LSR_OE          (0x01<<1)
#define LSR_PE          (0x01<<2)
#define LSR_FE          (0x01<<3)
#define LSR_BI          (0x01<<4)
#define LSR_THRE        (0x01<<5)
#define LSR_TEMT        (0x01<<6)
#define LSR_RXFE        (0x01<<7)

bool uart_is_busy(void);
void use_uart(bool is_used);
void uart_init(uint32_t baudrate);
serial_comm_t *get_uart_message(void);
bool new_uart_message_is_available(void);

#endif
