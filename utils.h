#ifndef H_UTILS
#define H_UTILS

#include <stdbool.h>
#include <stdarg.h>
#include <stdint.h>

#if 0
#define ARTDEBUG	1 /* Enable printf to RS-282 output over RJ-45 */
#define DEBUG_APP	1
#define DEBUG_GAIN	1 /* Gain definition (dev) */
#endif

#if 0
#define NO_ADC		1
#define NO_WDT		1
#endif

#ifdef ARTDEBUG
#define art_debug(s, ...) _art_debug(s, ##__VA_ARGS__)
#else
#define art_debug(s, ...) ;
#endif

uint8_t char_to_digit(char c);

bool find_substr(const char *string, const char *substr);
bool get_argument(char *string, char *arg, char delim, int num_arg);

void delay(int delay_10ms);
unsigned int get_monotonic_clock(void);
int compute_delta(unsigned int old_cnt, unsigned int cnt);
long int powerfunc(int x, int n);
int _art_debug(char *output, ...);

#endif
