/*----------------------------------------------------------------------------
 * Name:    ADC.c
 * Purpose: low level ADC functions
 
 * Note(s):
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *----------------------------------------------------------------------------*/

#include "LPC11xx.h"		/* LPC11xx definitions        */
#include "adc.h"
#include "main.h"
#include "tuner.h"
#include "gpio.h"
#include "bluetooth.h"
#include "power.h"
#include "platform_gpio_hyde.h"
#include "platform_hyde.h"
#include "utils.h"

extern config_data_t config_data;

static word power_monitor_threshold = 0;

static unsigned int adc_get_value(void)
{
	return (LPC_ADC->DR[0] >> 6) & ADC_VALUE_MAX;
}

static void start_adc(void)
{
	/* Start burst */
	LPC_ADC->CR |= (1UL << 16);
	LPC_ADC->INTEN = 0x01;	/* Generate irq when convertion from AD0is done */
}

/*----------------------------------------------------------------------------
  Initialize ADC Pins
 *----------------------------------------------------------------------------*/
void adc_init(enum adc_channels channels)
{
	/* GPIO0.11 -> AD0 : Power monitor function */
	enable_sys_clock(SYS_CLOCK_GPIO, true);
	enable_sys_clock(SYS_CLOCK_ADC, true);
	enable_sys_clock(SYS_CLOCK_IOCON, true);

	set_port_pinmux(PORT_IO_ADC, IO_PORT_ANALOG | IO_PORT_MODE_PULL_UP | IO_PORT_FUNC(PORT_IO_ADC_FUNC));
	set_gpio_direction(GPIO_BANK_ADC, GPIO_PIN_ADC, GPIO_INPUT);
	gpio_clear_irq(GPIO_BANK_ADC, GPIO_PIN_ADC);
	power_block_enable(POWER_BLOCK_ADC_PD, true);
	LPC_ADC->CR = (channels << 0) |	/* Select AD0 pin             */
	    (255UL << 8);	/* ADC clock is 48MHz/256     */

	start_adc();
	NVIC_EnableIRQ(ADC_IRQn);
}

/*----------------------------------------------------------------------------
  A/D IRQ: Executed when A/D Conversion is done 
 *----------------------------------------------------------------------------*/
void ADC_IRQHandler(void)
{
	/* Spanning - ADC Val
	 *      Max: 21,..V = 1024
	 *      18V = 848
	 *      VCC: gemeten: 738
	 *      12V = 566
	 */
#ifndef NO_ADC
	word adc_value = adc_get_value();

	if (!power_monitor_threshold) {
		power_monitor_threshold = (adc_value * 85) / 100;
		return;
	}

	if (adc_value < power_monitor_threshold) {
		store_config(&config_data);
		enable_amplifier(false);

		art_debug("ADC: AMP Off %d", adc_value);

		if ((adc_value >= (power_monitor_threshold * 80 / 100))
		    && (adc_value < power_monitor_threshold)) {
			art_debug("ADC: LDO On %d", adc_value);

			enable_amplifier(false);
			configuration_changed();
			enable_ldo(true);
			delay(MODULES_RESET_DURATION);
			set_tuner_power(true);
			delay(MODULES_RESET_DURATION);
			init_radio_source(&config_data.current_source);
		} else if (adc_value < (power_monitor_threshold * 80 / 100)) {
			set_tuner_power(false);
			enable_ldo(false);
		}
	} else {
		/* Software ON  start amplifier, other wise eco mode*/
		if (config_data.power_state == POWER_ON) {
			enable_amplifier(true);
		} else {
			if (config_data.eco_mode != ECOMODE_SETUP) {
				enable_amplifier(false);
			}
		}
	}
#endif
}
