#include "timer.h"
#include "utils.h"

volatile unsigned int sys_cnt = 0;

void SysTick_Handler(void)
{
	sys_cnt++;
}

unsigned int get_monotonic_clock(void)
{
	return sys_cnt;
}

void init_timer(struct timer *t)
{
	t->start_time = 0;
	t->timeout = 0;

	disable_timer(t);
}

void start_timer(struct timer *t, unsigned int delay)
{
	reset_timer(t);

	t->timeout = delay;
}

void reset_timer(struct timer *t)
{
	t->start_time = get_monotonic_clock();
	t->state = TIMER_STATE_ENABLED;
}

void disable_timer(struct timer *t)
{
	t->state = TIMER_STATE_DISABLED;
}

bool timer_has_fired(struct timer *t)
{
	bool fired = false;

	if (timer_is_enabled(t) && compute_delta(t->start_time, get_monotonic_clock()) >= t->timeout) {
		fired = true;
		disable_timer(t);
	}

	return fired;
}

bool timer_is_enabled(struct timer *t)
{
	bool enabled;

	switch (t->state) {
	case TIMER_STATE_DISABLED:
		enabled = false;
		break;
	default:
		enabled = true;
		break;
	}

	return enabled;
}
