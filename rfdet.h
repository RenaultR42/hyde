#ifndef H_RFDET
#define H_RFDET

#include "types.h"
#include "timer.h"

#define MAX_KEYDOWNTIME 	1000

enum rf_key_status {
	RF_KEY_HANDLED		= 0,
	RF_KEY_DOWN		= 1,
	RF_KEY_RELEASED	= 2,
};

enum radio_source {
	RADIO_SOURCE_DAB = 0,
	RADIO_SOURCE_FM,
	RADIO_SOURCE_MAX,
};

/*******************************
* Keys Wireless Remote Control
*******************************/
enum rf_keys {
	KEY_UNKWNON = -1,
	KEY_POWERON = 0x00,	/* On */
	KEY_POWEROFF = 0x01,	/* Off */
	KEY_PLUS = 0x02,	/* Up */
	KEY_OK = 0x03,		/* Confirm */
	KEY_PREV = 0x04,	/* Previous */
	KEY_NEXT = 0x05,	/* Next */
	KEY_MIN = 0x06,	/* Down */
	KEY_1 = 0x07,		/* 1 / BASS */
	KEY_2 = 0x08,		/* 2 / MID */
	KEY_3 = 0x09,		/* 3 / HIGH */
	KEY_4 = 0xa,		/* 4 / BALANCE */
	KEY_5 = 0x0b,		/* 5 / MONO-STEREO */
	KEY_6 = 0x0c,		/* 6 / FLAT */
	KEY_FM = 0x0d,		/* FM */
	KEY_DAB = 0x0e,	/* DAB */
	KEY_BT = 0x0f,		/* BT */
	KEY_AUX = 0x10,	/* AUX */
	KEY_CFG = 0x11,	/* Config */
	KEY_RET = 0x12,	/* Return */
};

typedef struct {
	word addr;
	enum rf_keys key;
	enum rf_key_status data_state;
	struct timer key_release_detection;
	int time_down;
} rfkey_stat_t;

int keyboard_digit_choice(enum rf_keys key, unsigned int size);
void RF_Init(void);
rfkey_stat_t *get_rf_key(void);

#endif
