#ifndef H_AUDIO
#define H_AUDIO

enum audio_state {
	PLAY		= 0x01,
	PAUSE		= 0x02,
	STOP		= 0x03,
	NEXT		= 0x04,
	PREV		= 0x05,
	FFWD		= 0x06,
	FREW		= 0x07,
	PAIR_ON	= 0x08,
	PAIR_OFF 	= 0x09,
	FFWD_PRESS	= 0x0a,
	FREW_PRESS	= 0x0b,
	FFWD_RELEASE	= 0x0c,
	FREW_RELEASE	= 0x0d,
	BT_SEEK_FF	= 0x0e,
	BT_SEEK_REW	= 0x0f,
	BT_SEEK_NONE	= 0x10,
	INQUIRY_TWS	= 0x11,
};

/* Length of notes */
enum length_notes {
	NOTE_WHOLE	= 1,	/* Complete note */
	NOTE_HALF	= 2,
	NOTE_FOURTH	= 4,
	NOTE_EIGHTH	= 8,
	NOTE_SIXTEENTH	= 16,
};

/* Music notes */
enum music_notes {
	TONE_A	= 'A',
	TONE_B	= 'B',
	TONE_C	= 'C',
	TONE_D	= 'D',
	TONE_E	= 'E',
	TONE_F	= 'F',
	TONE_G	= 'G',
};

#include "config.h"

void set_audio_state(enum audio_state new_state);
enum audio_state get_audio_state(void);

void reset_tone(config_data_t *config);
void set_tone(enum freq_boost tone, config_data_t *config);
enum audio_source get_audio_source(int route);

void set_balance(byte balance);
void balance_left_right(enum balance_button direction, config_data_t *config);
void restore_gain(enum audio_source source);
void in_out_gain(enum direction_button dir);

void set_volume(byte val, byte vol_max, enum audio_source source);
void volume_up_down(enum direction_button dir, config_data_t *config);

void do_beep(int duration, char tone, config_data_t *config);
void double_beep(int duration, char tone_1, char tone_2, config_data_t *config);

#endif
