#include <stdio.h>
#include <string.h>

#define LINE_SIZE_MAX		150
#define BUFFER_SIZE_MAX	1024

#include "bluetooth_gatt.h"
#include "utils.h"

enum bc128_ble_gatt_flag {
	BC128_CMD_FLAG_BLE_NONE			= 0x0,
	BC128_CMD_FLAG_BLE_READ_GET_POWER_MAN		= 0x1,
	BC128_CMD_FLAG_BLE_READ_GET_RADIO_MODE		= 0x2,
	BC128_CMD_FLAG_BLE_READ_GET_FREQ		= 0x4,
	BC128_CMD_FLAG_BLE_READ_GET_VOLUME		= 0x8,
	BC128_CMD_FLAG_BLE_READ_GET_INPUT_SRC		= 0x10,
	BC128_CMD_FLAG_BLE_READ_GET_PRESET		= 0x20,
	BC128_CMD_FLAG_BLE_READ_GET_STATION_NAME	= 0x40,
	BC128_CMD_FLAG_BLE_READ_GET_NB_SERVICE		= 0x80,
	BC128_CMD_FLAG_BLE_READ_GET_SERVICE_INDEX	= 0x100,
};

static enum bc128_cmd_flag cmd_flags = BC128_CMD_FLAG_NONE;
static word current_dab_index = 0;

void convert_string_to_hex(char *src, char *dst)
{
	int len = strlen(src);
	int i;

	for (i = 0; i < len; i++) {
		sprintf(&dst[i * 2], "%X", src[i]);
	}
}

int gatt_char_config_serialize(struct gatt_char_config *config, uint8_t *buffer)
{
	int pos = 0;

	if (config->has_config) {
		buffer[pos++] = (uint8_t)config->type;
		buffer[pos++] = 0;
	}

	return pos;
}

int gatt_char_value_serialize(struct gatt_char_value *value, uint8_t *buffer)
{
	int pos = 0;

	buffer[pos++] = (uint8_t)value->type;

	if (value->has_value) {
		uint8_t length = strlen(value->value);
		int i;

		buffer[pos++] = length;

		for (i = 0; i < length; ) {
			/* End of string when number of char is odd */
			if (i == length - 1 && length % 2 != 0) {
				buffer[pos++] = (uint8_t)value->value[i];
				buffer[pos++] = 0;
				i++;
			} else {
				buffer[pos++] = (uint8_t)value->value[i];
				buffer[pos++] = (uint8_t)value->value[i + 1];
				i += 2;
			}
		}
	} else {
		buffer[pos++] = 0;
	}

	return pos;
}

int gatt_char_serialize(struct gatt_char *charac, uint8_t *buffer)
{
	const uint8_t header_size = charac->uuid.size == 4 ? 0x5 : 0x13;
	int pos = 0;
	int i;

	/* Header */
	buffer[pos++] = (uint8_t)charac->uuid.type;
	buffer[pos++] = (uint8_t)header_size;
	buffer[pos++] = (uint8_t)charac->perm;
	buffer[pos++] = (uint8_t)charac->handle;

	/* Begin UID */
	buffer[pos++] = 0;

	for (i = charac->uuid.size - 1; i >= 0; i -= 2) {
		uint8_t num = (char_to_digit(charac->uuid.uuid[i - 1]) << 4) + char_to_digit(charac->uuid.uuid[i]);
		buffer[pos++] = num;
	}

	/* End of UID */
	buffer[pos++] = 0;

	/* Characteristic value */
	pos += gatt_char_value_serialize(&charac->value, &buffer[pos]);

	/* Characteristic configuration */
	pos += gatt_char_config_serialize(&charac->config, &buffer[pos]);

	return pos;
}

int gatt_service_serialize(struct gatt_service *service, uint8_t *buffer)
{
	int pos = 0;
	int i;

	/* Header */
	buffer[pos++] = (uint8_t)service->uuid.type;
	buffer[pos++] = (uint8_t)service->uuid.size / 2;

	for (i = service->uuid.size - 1; i >= 0; i -= 2) {
		uint8_t num = (char_to_digit(service->uuid.uuid[i - 1]) << 4) + char_to_digit(service->uuid.uuid[i]);
		buffer[pos++] = num;
	}

	for (i = 0; i < service->nb_charac; i++) {
		pos += gatt_char_serialize(&service->charac[i], &buffer[pos]);
	}

	return pos;
}

void add_primary_service(struct gatt_service *service, const char *uuid)
{
	service->uuid.type = GATT_DATA_TYPE_UID;
	strncpy(service->uuid.uuid, uuid, GATT_UID_SIZE + 1);
	service->uuid.size = strlen(uuid);
}

void add_characteristic(struct gatt_char *charac, const char *uuid, enum gatt_handle handle, enum gatt_permissions perm, char *value, bool has_config)
{
	charac->uuid.type = GATT_DATA_TYPE_CHAR;
	strncpy(charac->uuid.uuid, uuid, GATT_UID_SIZE + 1);
	charac->uuid.size = strlen(uuid);

	charac->handle = handle;
	charac->perm = perm;

	if (value) {
		charac->value.has_value = true;
		charac->value.type = GATT_DATA_TYPE_CONSTANT_CHAR_VALUE;
		strncpy(charac->value.value, value, GATT_DATA_SIZE_MAX + 1);
	} else {
		charac->value.has_value = false;
		charac->value.type = GATT_DATA_TYPE_CHAR_VALUE;
	}

	charac->config.type = GATT_DATA_TYPE_CLIENT_CHAR_CONFIG;
	charac->config.has_config = has_config;
}

void print_gatt_buffer(uint8_t *buffer, int buffer_size)
{
	const int nb_lines = (buffer_size * 2) / LINE_SIZE_MAX + 1;
	int i, j;

	for (i = 0; i < nb_lines; i++) {
		int size = buffer_size * 2 - i * LINE_SIZE_MAX;
		char line[LINE_SIZE_MAX + 1] = { '\0' };
		int current_char;

		for (j = 0, current_char = 0; j * 2 < size; j++) {
			snprintf(&line[current_char], 3, "%02X", buffer[i + j]);
			current_char += 2;

			if ((j + 1) % 2 == 0) {
				line[current_char++] = ' ';
			}
		}

		line[current_char] = '\0';
		printf("%s\r", line);
	}
}

/* Base UUID for official Bluetooth services: XXXXXXXX-0000-1000-8000-00805F9B34FB */
int init_gatt_buffer(uint8_t *buffer)
{
	int pos = 0;
	struct gatt_service service;

	/* Official GATT service to get info about device */
	add_primary_service(&service, "1800");
	add_characteristic(&service.charac[0], "2A00", 3, GATT_PERM_READ, "ArtCore Hyde", false);
	add_characteristic(&service.charac[1], "2A01", 5, GATT_PERM_READ, NULL, false);
	service.nb_charac = 2;

	pos += gatt_service_serialize(&service, &buffer[pos]);

	/* Service tuner */
	add_primary_service(&service, "17A0D16D9E5C438095E8EB9315ABAE01");
	add_characteristic(&service.charac[0], "17A0D16E9E5C438095E8EB9315ABAE01", GATT_HANDLER_POWER_MANAGEMENT, GATT_PERM_READ | GATT_PERM_WRITE | GATT_PERM_NOTIFY, NULL, true);
	add_characteristic(&service.charac[1], "17A0D16F9E5C438095E8EB9315ABAE01", GATT_HANDLER_RADIO_MODE, GATT_PERM_READ | GATT_PERM_NOTIFY, NULL, true);
	service.nb_charac = 2;

	pos += gatt_service_serialize(&service, &buffer[pos]);

	/* Radio info from DAB or FM */
	add_primary_service(&service, "75371D8BB5D348C48B7A24859DEABA32");
	/* Frequence */
	add_characteristic(&service.charac[0], "75371D8CB5D348C48B7A24859DEABA32", GATT_HANDLER_RADIO_STATION_NAME, GATT_PERM_READ | GATT_PERM_NOTIFY, NULL, true);
	/* Station name */
	add_characteristic(&service.charac[1], "75371D8DB5D348C48B7A24859DEABA32", GATT_HANDLER_RADIO_FREQ, GATT_PERM_READ | GATT_PERM_WRITE | GATT_PERM_NOTIFY, NULL, true);
	add_characteristic(&service.charac[2], "75371D8EB5D348C48B7A24859DEABA32", GATT_HANDLER_RADIO_STORE_PRESET, GATT_PERM_READ | GATT_PERM_WRITE | GATT_PERM_NOTIFY, NULL, true);
	add_characteristic(&service.charac[3], "75371D8FB5D348C48B7A24859DEABA32", GATT_HANDLER_RADIO_SELECT_PRESET, GATT_PERM_WRITE, NULL, false);
	service.nb_charac = 4;

	pos += gatt_service_serialize(&service, &buffer[pos]);

	/* DAB service */
	add_primary_service(&service, "6996BD4906A34DF5A021ACA68AE457ED");
	/* Frequence */
	add_characteristic(&service.charac[0], "6996BD4A06A34DF5A021ACA68AE457ED", GATT_HANDLER_DAB_AUTO_SCAN, GATT_PERM_WRITE, NULL, false);
	/* Station name */
	add_characteristic(&service.charac[1], "6996BD4B06A34DF5A021ACA68AE457ED", GATT_HANDLER_DAB_NB_SERVICE, GATT_PERM_READ | GATT_PERM_NOTIFY, NULL, true);
	add_characteristic(&service.charac[2], "6996BD4C06A34DF5A021ACA68AE457ED", GATT_HANDLER_DAB_SERVICE_INDEX, GATT_PERM_READ | GATT_PERM_WRITE | GATT_PERM_NOTIFY, NULL, true);
	service.nb_charac = 3;

	pos += gatt_service_serialize(&service, &buffer[pos]);

	/* Service audio */
	add_primary_service(&service, "2CB1FAD1AC5F4E6C9E456156DA47E4F4");
	/* Input selection */
	add_characteristic(&service.charac[0], "2CB1FAD2AC5F4E6C9E456156DA47E4F4", GATT_HANDLER_INPUT_SOURCE, GATT_PERM_READ | GATT_PERM_WRITE | GATT_PERM_NOTIFY, NULL, true);
	/* Volume */
	add_characteristic(&service.charac[1], "2CB1FAD3AC5F4E6C9E456156DA47E4F4", GATT_HANDLER_VOLUME, GATT_PERM_READ | GATT_PERM_WRITE | GATT_PERM_NOTIFY, NULL, true);
	service.nb_charac = 2;

	pos += gatt_service_serialize(&service, &buffer[pos]);

	return pos;
}

void parse_ble_gatt_read_request(char *request, config_data_t *config)
{
	char arg[SERIAL_BUFFER_SIZE_MAX];

	if (get_argument(request, arg, ' ', 2)) {
		int handler = strtol(arg, NULL, 16);

		switch (handler) {
		case GATT_HANDLER_POWER_MANAGEMENT:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_POWER_MAN;
			break;
		case GATT_HANDLER_RADIO_MODE:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_RADIO_MODE;
			break;
		case GATT_HANDLER_FREQ:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_FREQ;
			break;
		case GATT_HANDLER_RADIO_STATION_NAME:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_STATION_NAME;
			break;
		case GATT_HANDLER_RADIO_SELECT_PRESET:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_PRESET;
			break;
		case GATT_HANDLER_DAB_NB_SERVICE:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_NB_SERVICE;
			break;
		case GATT_HANDLER_DAB_SERVICE_INDEX:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_SERVICE_INDEX;
			break;
		case GATT_HANDLER_INPUT_SOURCE:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_VOLUME;
			break;
		case GATT_HANDLER_VOLUME:
			cmd_flags |= BC128_CMD_FLAG_BLE_READ_GET_INPUT_SRC;
			break;
		default:
			break;
		}
	}
}

void parse_ble_gatt_write_request(char *request, config_data_t *config)
{
	char arg[SERIAL_BUFFER_SIZE_MAX];
	enum tuner_mode mode = get_radio_mode();

	if (get_argument(request, arg, ' ', 2)) {
		int handler = strtol(arg, NULL, 16);

		if (get_argument((char *)msg->cmd_data_buf, arg, ' ', 3)) {
			word data = strtol(arg, NULL, 16);

			switch (handler) {
			case GATT_HANDLER_POWER_MANAGEMENT:
				set_power(data, config);
				break;
			case GATT_HANDLER_RADIO_MODE:
				SetTunerMode(data);
				break;
			case GATT_HANDLER_FREQ:
				if (mode == DAB_MODE) {
					config->current_radio_index[RADIO_SOURCE_DAB] = data;
					TuneDABService(data);
				} else if (mode == FM_MODE) {
					config->current_radio_index[RADIO_SOURCE_FM] = data;
					SetFMChannel(data);
				}
				break;
			case GATT_HANDLER_RADIO_STORE_PRESET:
				if (get_argument((char *)msg->cmd_data_buf, arg, ' ',4)) {
					word preset = strtol(arg, NULL, 16);
					if (get_argument((char *)msg->cmd_data_buf, arg, ' ', 5)) {
						word freq = strtol(arg, NULL, 16);

						store_preset(preset, config->current_source, config);
					}
				}
				break;
			case GATT_HANDLER_RADIO_SELECT_PRESET:
				if (get_argument((char *)msg->cmd_data_buf, arg, ' ',4)) {
					word preset = strtol(arg, NULL, 16);
					select_radio_preset(preset, config->current_source, config);
				}
				break;
			case GATT_HANDLER_DAB_AUTO_SCAN:
				FullDabScan();
				config->dab_scanned = 1;
				break;
			case GATT_HANDLER_DAB_SERVICE_INDEX:
				current_dab_index = data;
				break;
			case GATT_HANDLER_INPUT_SOURCE:
				set_route(data);
				break;
			case GATT_HANDLER_VOLUME:
				set_volume(data, config->max_volume, config->current_source);
				break;
			default:
				break;
			}
		}
	}
}

void process_ble_gatt_read_requests(int ble_link_id, config_data_t *config)
{
	enum tuner_mode mode = get_radio_mode();
	enum radio_source source = mode == DAB_MODE ? RADIO_SOURCE_DAB : RADIO_SOURCE_FM;

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_POWER_MAN)) {
		send_bc128("BLE_READ_RES %d %X 1 %X", ble_link_id, GATT_HANDLER_POWER_MANAGEMENT, config->power_state);
	}

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_RADIO_MODE)) {
		send_bc128("BLE_READ_RES %d %X 1 %X", ble_link_id, BC128_CMD_FLAG_BLE_READ_GET_RADIO_MODE, mode);
	}

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_FREQ)) {
		send_bc128("BLE_READ_RES %d %X 2 %X %X", ble_link_id, BC128_CMD_FLAG_BLE_READ_GET_FREQ, source, config->current_radio_index[source]);
	}

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_PRESET)) {
		send_bc128("BLE_READ_RES %d %X 7 %X %X %X %X %X %X %X", ble_link_id, BC128_CMD_FLAG_BLE_READ_GET_PRESET, source,
			config->radio_presets[source][PRESET_1],
			config->radio_presets[source][PRESET_2],
			config->radio_presets[source][PRESET_3],
			config->radio_presets[source][PRESET_4],
			config->radio_presets[source][PRESET_5],
			config->radio_presets[source][PRESET_6]);
	}

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_STATION_NAME)) {
		struct dab_service_info info;
		char name[TUNER_SERVICE_NAME_MAX * 2 + 1];
		int name_len;
		int msg_len;

		info.index = config->current_radio_index[RADIO_SOURCE_DAB];
		info.name[0] = '\0';
		info.freq_ensemble = TUNER_NO_FREQ_ENSEMBLE;

		get_dab_service_info(&info);
		convert_string_to_hex(name, info.name);

		name_len = strlen(name);
		msg_len = name_len + 1;

		send_bc128("BLE_READ_RES %d %X %X %X %s", ble_link_id, BC128_CMD_FLAG_BLE_READ_GET_STATION_NAME, msg_len, name_len, name);
	}

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_NB_SERVICE)) {
		send_bc128("BLE_READ_RES %d %X 1 %X", ble_link_id, BC128_CMD_FLAG_BLE_READ_GET_NB_SERVICE, GetNumberOfServices());
	}

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_SERVICE_INDEX)) {
		struct dab_service_info info;
		char name[TUNER_SERVICE_NAME_MAX * 2 + 1];
		int name_len;
		int msg_len;

		info.index = current_dab_index;
		info.name[0] = '\0';
		info.freq_ensemble = TUNER_NO_FREQ_ENSEMBLE;

		get_dab_service_info(&info);

		name_len = strlen(info.name);
		msg_len = name_len + 3;
		convert_string_to_hex(name, info.name);

		send_bc128("BLE_READ_RES %d %X %X %X %X %X %s", ble_link_id, BC128_CMD_FLAG_BLE_READ_GET_SERVICE_INDEX, msg_len, info.index, info.freq_ensemble, name_len, name);
	}

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_VOLUME)) {
		send_bc128("BLE_READ_RES %d %X 1 %X", ble_link_id, GATT_HANDLER_INPUT_SOURCE, config->current_volume);
	}

	if ((cmd_flags & BC128_CMD_FLAG_BLE_READ_GET_INPUT_SRC)) {
		send_bc128("BLE_READ_RES %d %X 1 %X", ble_link_id, GATT_HANDLER_VOLUME, config->current_source);
	}

	cmd_flags = BC128_CMD_FLAG_BLE_NONE;
}

