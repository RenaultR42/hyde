#include <stdbool.h>
#include <stdio.h>

#include "audio_routing.h"
#include "audio.h"
#include "bluetooth.h"
#include "tuner.h"
#include "platform_hyde.h"
#include "utils.h"

void update_spp(byte spp, config_data_t *config)
{
	art_debug("SPP command %d", spp);

	set_source(spp & 0x0f, config);

	send_bc128("SEND 15 REPLY:%i", spp);
}

void select_source(enum audio_source source, config_data_t *config)
{
	if (config->current_source != source) {
		set_source(source, config);
	}
}

void set_route(enum audio_source source)
{
	switch (source) {
	case SRC_ANALOG:
		analog_switch(ANALOG_SWITCH_MODE_AUX);
		send_bc128("ROUTE 1");
		break;
	case SRC_DIGITAL:
		send_bc128("ROUTE 4");
		break;
	case SRC_BLUETOOTH:
		send_bc128("ROUTE 0");
		break;
	case SRC_DABTUNER:
	case SRC_FMTUNER:
		analog_switch(ANALOG_SWITCH_MODE_Q8);
	default:
		send_bc128("ROUTE 1");
		break;
	}
}

void set_source(enum audio_source sourceid, config_data_t *config)
{
	art_debug("Select Source %d", sourceid);

	mute_amplifier(true);

	if (has_bluetooth_player() && (sourceid != SRC_BLUETOOTH)) {
		control_bt(PAUSE);
	}

	switch (sourceid) {
	case SRC_FMTUNER:
		change_radio_mode(RADIO_SOURCE_FM, config);
		break;
	case SRC_DABTUNER:
		change_radio_mode(RADIO_SOURCE_DAB, config);
		break;
	case SRC_ANALOG:
	default:
		restore_gain(sourceid);
		analog_switch(ANALOG_SWITCH_MODE_AUX);
		delay(5);
		SetTunerMode(STANDBY);
		break;
	}

	delay(10);

	if (sourceid != config->current_source) {
		set_route(sourceid);
		config->current_source = sourceid;
	}

	delay(30);

	set_volume(config->current_volume, config->max_volume, config->current_source);
	mute_amplifier(false);

	switch (sourceid) {
	case SRC_DIGITAL:
		delay(50);
		double_beep(NOTE_SIXTEENTH, TONE_D, TONE_D, config);
		break;
	case SRC_ANALOG:
		analog_switch(ANALOG_SWITCH_MODE_AUX);
		delay(50);
		do_beep(NOTE_SIXTEENTH, TONE_D, config);
		break;
	}

	configuration_changed();
}

void init_radio_source(enum audio_source source)
{
	switch (source) {
	case SRC_FMTUNER:
		InitRadio(FM_MODE);
		break;
	case SRC_DABTUNER:
		InitRadio(DAB_MODE);
		break;
	default:
		InitRadio(STANDBY);
		break;
	}
}

void change_radio_mode(enum radio_source source, config_data_t *config)
{
	enum tuner_mode opposite_mode;
	enum tuner_mode new_mode;
	enum audio_source audio_src;
	enum radio_source radio_src_opposite;

	switch (source) {
	case RADIO_SOURCE_DAB:
		audio_src = SRC_DABTUNER;
		new_mode = DAB_MODE;
		opposite_mode = FM_MODE;
		radio_src_opposite = RADIO_SOURCE_FM;
		break;
	case RADIO_SOURCE_FM:
		audio_src = SRC_FMTUNER;
		new_mode = FM_MODE;
		opposite_mode = DAB_MODE;
		radio_src_opposite = RADIO_SOURCE_DAB;
		break;
	default:
		return;
		break;
	}

	restore_gain(audio_src);

	if (get_radio_mode() == opposite_mode)
		config->current_radio_index[radio_src_opposite] = GetCurrentChannel(radio_src_opposite);

	SetTunerMode(new_mode);
	delay(5);
	analog_switch(ANALOG_SWITCH_MODE_Q8);
	delay(5);
	SetRadioChannel(source, config->current_radio_index[source]);
}
